import axios from 'axios';
import {
  ARCHIVE_OBJECTS,
  ARCHIVE_OBJECTS_PUT_LINK,
  headerConfig,
  IMAGE_OBJECTS, IMAGE_OBJECTS_PUT_LINK, VIDEO_OBJECTS, VIDEO_OBJECTS_PUT_LINK,
} from '../Constants/Constants';

export const submitQuestion = (payload) => ({
  type: 'SUBMIT_QUESTION',
  payload
});

export const saveImageData = (payload) => ({
  type: 'SAVE_IMAGE_DATA',
  payload
});

export const saveAnswerData = (payload) => ({
  type: 'SAVE_ANSWER_DATA',
  payload
});

export const errorImageUpload = (payload) => ({
  type: 'ERROR_IMAGE_UPLOAD',
  payload
});

export const uploadImageToS3 = (fileType, file) => {
  return async (dispatch) => {
    const configHeader = {
      method: 'PUT',
      headers: {
        'Content-Type': file.type
      },
      body: file
    };
    try {
      if (fileType.fileExtension === 'jpeg' || fileType.fileExtension === 'jpg' || fileType.fileExtension === 'png') {
        const { data: { url } } = await axios.post(IMAGE_OBJECTS_PUT_LINK, fileType, headerConfig);
        await fetch(url, configHeader);
        const urlToBucket = {
          name: file.name,
          url
        };
        const { data } = await axios.post(IMAGE_OBJECTS, urlToBucket, headerConfig);
        return data;
      }
      return null;
    } catch (error) {
      return dispatch(errorImageUpload(true));
    }
  };
};

export const uploadToS3 = (fileType, file) => {
  return async (dispatch) => {
    const configHeader = {
      method: 'PUT',
      headers: {
        'Content-Type': file.type
      },
      body: file
    };
    try {
      if (fileType.fileExtension === 'zip') {
        const { data: { url } } = await axios.post(ARCHIVE_OBJECTS_PUT_LINK, fileType);
        await fetch(url, configHeader);
        const urlToBucket = {
          name: file.name,
          url
        };
        const { data } = await axios.post(ARCHIVE_OBJECTS, urlToBucket);
        return data;
      }
      if (fileType.fileExtension === 'mp4' || fileType.fileExtension === 'mov') {
        const { data: { url } } = await axios.post(VIDEO_OBJECTS_PUT_LINK, fileType);
        await fetch(url, configHeader);
        const urlToBucket = {
          name: file.name,
          url
        };
        const { data } = await axios.post(VIDEO_OBJECTS, urlToBucket);
        return data;
      }
      return null;
    } catch (error) {
      return dispatch(errorImageUpload(true));
    }
  };
};
