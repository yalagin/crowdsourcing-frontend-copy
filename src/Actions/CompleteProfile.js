import axios from 'axios';
import { API_BASE_URL, PERSON_URL } from '../Constants/Constants';

export const isErrorCompleteProfile = (value) => ({
  type: 'IS_ERROR_COMPLETE_PROFILE',
  payload: value
});

export const completeProfile = (userId, roleAndTag, personData, history) => {
  return async (dispatch) => {
    try {
      await axios.put(`${API_BASE_URL}${userId}`, roleAndTag,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`,
            'Content-Type': 'application/ld+json'
          }
        });
      await axios.post(PERSON_URL, personData,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`,
            'Content-Type': 'application/ld+json'
          }
        });
      history.push('/papan-pintar');
    } catch (error) {
      dispatch(isErrorCompleteProfile(true));
    }
  };
};
