import { refreshToken } from './Auth';

export function Jwt({ dispatch, getState }) {
  return (next) => (action) => {
    // only worry about expiring token for async actions
    if (typeof action === 'function') {
      if (localStorage.getItem('user')) {
        // decode jwt so that we know if and when it expires
        const tokenExpiration = localStorage.getItem('user') && JSON.parse(localStorage.getItem('user')).exp;
        if (tokenExpiration && Date.now() >= tokenExpiration * 1000) {
          // make sure we are not already refreshing the token
          if (!getState().auth.freshTokenPromise) {
            return refreshToken(dispatch).then(() => next(action));
          }
          return getState().auth.freshTokenPromise.then(() => next(action));
        }
      }
    }
    return next(action);
  };
}
