/* eslint-disable */
import axios from 'axios';
import * as jwtDecode from 'jwt-decode';
import { isEmpty } from 'lodash';
import {
  API_BASE_URL, FETCH_USERS,
  headerLDJsonConfig, LOGIN_URL
} from '../Constants/Constants';
import { fetch } from '../Generated/utils/dataAccess';

export const isError = (value) => ({
  type: 'IS_ERROR',
  payload: value
});

export const isSuccessRegister = (value) => ({
  type: 'IS_SUCCESS_REGISTER',
  payload: value
});

//todo make error handle like you will be logged out
export function error(error) {
  return { type: 'AUTH_REFRESH_ERROR', error };
}

export function loading(loading) {
  return { type: 'AUTH_REFRESH_LOADING', loading };
}

export function success(created) {
  return { type: 'AUTH_REFRESH_SUCCESS', created };
}

// todo change to more secure way of storing data
function saveAppToken(token, refreshingToken) {
  const decodedToken = jwtDecode(token);
  localStorage.setItem('token', token);
  localStorage.setItem('refresh_token', refreshingToken);
  localStorage.setItem('id', decodedToken.uuid);
  localStorage.setItem('user', JSON.stringify(decodedToken));
}

export const signIn = (emailAndPassword, history) => {
  return async (dispatch) => {
    try {
      const { data: { token, refresh_token } } = await axios.post(LOGIN_URL, emailAndPassword);
      if (token) {
        saveAppToken(token, refresh_token);
        const userId = localStorage.getItem('id');
        const { data } = await axios.get(`${API_BASE_URL}${userId}`);
        if (data && !isEmpty(data.tags)) {
          return history.push('/papan-pintar');
        }
        return history.push('/complete-profile');
      }
      return new Error();
    } catch (error) {
      return dispatch(isError(true));
    }
  };
};

export const signUp = (users, history, setStatus) => {
  return async (dispatch) => {
    try {
      const { data } = await axios.post(FETCH_USERS, users, headerLDJsonConfig);
      if (data) {
        dispatch(isSuccessRegister(true));
        history.push('/signin');
      }
    } catch (error) {
      if (error.response.data) {
        setStatus(error.response.data.violations);
      }
    }
  };
};

export const signOut = () => {
  return () => {
    localStorage.clear();
    window.location.reload();
  };
};

export function refreshToken(dispatch) {
  dispatch(loading(true));
  const value = { refresh_token: localStorage.getItem('refresh_token') };
  const freshTokenPromise = fetch('/token/refresh', { method: 'POST', body: JSON.stringify(value) })
    .then((response) => {
      dispatch(loading(false));

      return response.json();
    })
    .then((t) => {
      dispatch(success(t));
      saveAppToken(t.token, t.refresh_token);
      return t.token ? Promise.resolve(t.token) : Promise.reject({
        message: 'could not refresh token'
      });
    })
    .catch((e) => {
      dispatch(loading(false));
      dispatch(error(e.message));
      dispatch(signOut());
      return Promise.resolve();
    });

  dispatch({
    type: 'REFRESHING_TOKEN',
    // we want to keep track of token promise in the state so that we don't try to refresh
    // the token again while refreshing is in process
    freshTokenPromise
  });

  return freshTokenPromise;
}

/* eslint-enable */
