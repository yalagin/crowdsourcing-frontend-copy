import * as Yup from 'yup';

export const completeProfileSchema = Yup.object().shape({
  role: Yup.string().required('Please enter your role!'),
  tags: Yup.array().required('Please enter your area of expertise!'),
  worksFor: Yup.string().required('Please enter your current company!'),
  addressRegion: Yup.string()
});
