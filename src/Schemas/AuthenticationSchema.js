import * as Yup from 'yup';

export const signInSchema = Yup.object().shape({
  email: Yup.string().email('Your email is invalid!').required('Please enter your email address!'),
  password: Yup.string().min(5, 'Your password is too short!').required('Please enter your password!')
});

export const signUpSchema = Yup.object().shape({
  username: Yup.string().required('Please enter your full name!'),
  email: Yup.string().email('Your email is invalid!').required('Please enter your email address!'),
  password: Yup
    .string()
    .min(7, 'Your password is too short!')
    .matches(
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{7,})/,
      'Must Contain 7 Characters, One Uppercase, One Lowercase, and One Number'
    )
    .required('Please enter your password!'),
  acceptTerms: Yup.bool().oneOf([true], 'Accept Terms & Conditions is required')
});
