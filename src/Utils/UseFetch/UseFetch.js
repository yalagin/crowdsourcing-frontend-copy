const doReFetch = (setLoading, setReFetch) => {
  setLoading(true);
  setReFetch(true);
};

export default {
  doReFetch
};
