import UseFetch from './UseFetch';

const {
  doReFetch
} = UseFetch;

describe('UseFetch', () => {
  describe('#doReFetch', () => {
    it('should call setLoading with true and setReFetch with true when invoked', () => {
      const mockSetLoading = jest.fn();
      const mockSetReFetch = jest.fn();

      doReFetch(mockSetLoading, mockSetReFetch);

      expect(mockSetLoading).toHaveBeenCalledWith(true);
      expect(mockSetReFetch).toHaveBeenCalledWith(true);
    });
  });
});
