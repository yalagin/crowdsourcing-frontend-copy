export const API_BASE_URL = 'https://localhost:8443';
export const LOGIN_URL = '/authentication_token';
export const FETCH_USERS = '/users';
export const CREATIVE_WORKS_URL = '/creative_works';
export const OPTION_TAGS_URL = '/tags';
export const ADDRESS_REGION = '/address_regions';
export const MEDIA_OBJECTS = '/media_objects';
export const IMAGE_OBJECTS = '/image_objects';
export const ARCHIVE_OBJECTS = '/archive_objects';
export const VIDEO_OBJECTS = '/video_objects';
export const MEDIA_OBJECTS_PUT_LINK = '/media_objects/get-put-link';
export const IMAGE_OBJECTS_PUT_LINK = '/image_objects/give-me-put-link';
export const ARCHIVE_OBJECTS_PUT_LINK = '/archive_objects/give-me-put-link';
export const VIDEO_OBJECTS_PUT_LINK = '/video_objects/give-me-put-link';
export const PERSON_URL = '/people';
export const ORGANIZATION_UNCONFIRMED = '/organization_unconfirmed_members';
export const QUESTIONS_URL = '/questions';
export const ANSWERS_URL = '/answers';
export const ORGANIZATION_URL = '/organizations';
export const PROJECT_URL = '/projects';
export const AWARDS_URL = '/awards';

export const noop = () => {};

export const headerConfig = {
  headers: { Authorization: `Bearer ${localStorage.getItem('token')}` }
};

export const headerAuthorizedLDJsonConfig = {
  headers: {
    Authorization: `Bearer ${sessionStorage.getItem('token')}`,
    'Content-Type': 'application/ld+json'
  }
};

export const headerLDJsonConfig = {
  headers: {
    'Content-Type': 'application/ld+json'
  }
};

export const headerImageConfig = (files) => ({
  headers: {
    Authorization: '',
    'Content-Type': files.type
  }
});

export const headerBlob = {
  responseType: 'blob'
};
