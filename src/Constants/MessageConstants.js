const MessageConstants = {
  ERROR_LOGIN: 'The email/password combination you entered is incorrect',
  SUCCESS_REGISTER: 'User successfully registered! \nPlease check your email for confirmation.',
  ERROR_COMPLETE_PROFILE: 'Error occurs when trying to complete profile'
};

export default MessageConstants;
