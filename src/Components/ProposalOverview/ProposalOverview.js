import React, { useEffect } from 'react';
import {
  Col, Container, Row
} from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import ModalProposalMember from '../ModalProposalMember/ModalProposalMember';
import {
  list as educationalLevelList
} from '../../Generated/actions/educationallevel/list';
import ProjectBriefTab from '../ProjectBriefTab/ProjectBriefTab';

const ProposalOverview = ({
  projectDetail, isShowModal, mappedImages,
  mappedMembers, setIsShowModal
}) => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(educationalLevelList());
  }, []);

  const handleShowModal = () => {
    setIsShowModal(!isShowModal);
  };

  return (
    <Container>
      <div className='question-answer-overview'>
        <Row>
          < ProjectBriefTab projectDetail={projectDetail} />
          <Col xs={4}>
            <div
              className='question-answer-overview-title'
              onClick={handleShowModal}
            >
              <Row>
                <Col xs={12}>
                  <h1>Anggota <br /> Kelompok</h1>
                </Col>
                <Col xs={12}>
                  <div className='question-answer-overview-member'>
                    {mappedImages && mappedImages['hydra:member'].map((images) => (
                      <div className='question-answer-overview-member-avatar'>
                        <img src={images.contentUrl} alt='' />
                      </div>
                    ))}
                  </div>
                </Col>
              </Row>
            </div>
            <ModalProposalMember
              isShowModal={isShowModal}
              setIsShowModal={setIsShowModal}
              mappedMembers={mappedMembers}
            />
          </Col>
        </Row>
      </div>
    </Container>
  );
};

ProposalOverview.propTypes = {

};

export default ProposalOverview;
