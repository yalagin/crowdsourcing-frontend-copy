import React, { useEffect, useState } from 'react';
import { Col, Image, Row } from 'react-bootstrap';
import { Scrollbars } from 'react-custom-scrollbars';
import { isEmpty } from 'lodash';
import questionPreview from '../../Assets/Images/question-preview.png';
import cancelPreview from '../../Assets/Images/cancel-preview.png';
import correctIcon from '../../Assets/Images/correct-icon.png';
import Input from '../Input/Input';
import submitPreview from '../../Assets/Images/submit-preview.png';
import { useFetch } from '../../Hooks/UseFetch/UseFetch';
import { IMAGE_OBJECTS } from '../../Constants/Constants';

const ProposalQuestionPreview = ({ questionData, previewIndex, questionsCount }) => {
  const { fetchedData: { data: imageObjects } } = useFetch(IMAGE_OBJECTS);
  const [previewImage, setPreviewImage] = useState(null);

  useEffect(() => {
    if (!isEmpty(imageObjects) && questionData.image !== null) {
      const imageObjectArr = imageObjects && imageObjects['hydra:member'].filter((image) => {
        return image['@id'] === questionData.image;
      });
      setPreviewImage(imageObjectArr[0].contentUrl);
    }
  }, [imageObjects]);

  const renderAnswerField = (question) => ({
    trueOrFalse: (
      <div className='footer__answer margin-top--80'>
        <div className='footer__answer-button'>
          <h1>True</h1>
        </div>
        <div className='footer__answer-button'>
          <h1>False</h1>
        </div>
      </div>
    ),
    multipleChoice: (
      <>
        <div className='footer__answer'>
          <div className='footer__answer-button'>
            <h1>{question.type === 'TYPE_MULTIPLE_CHOICE' && question.answerPreview[0].name}</h1>
          </div>
          <div className='footer__answer-button'>
            <h1>{question.type === 'TYPE_MULTIPLE_CHOICE' && question.answerPreview[1].name}</h1>
          </div>
        </div>
        <div className='footer__answer'>
          <div className='footer__answer-button'>
            <h1>{question.type === 'TYPE_MULTIPLE_CHOICE' && question.answerPreview[2].name}</h1>
          </div>
          <div className='footer__answer-button'>
            <h1>{question.type === 'TYPE_MULTIPLE_CHOICE' && question.answerPreview[3].name}</h1>
          </div>
        </div>
      </>
    ),
    fillTheBlank: (
      <div className='footer__answer margin-top--80'>
        <Input placeholder='TYPE ANSWER HERE' />
        <Image src={submitPreview} />
      </div>
    )
  });

  const renderCorrectAnswerPreview = (answerKey) => {
    return (
      <div className='mask'>
        <div className='mask__text'>
          <h1>Yay</h1>
        </div>
        <div className='mask__icon'>
          <Image src={correctIcon} />
        </div>
        <div className='mask__description'>
          <h1>Correct</h1>
          <div className='mask__key'>
            <Scrollbars
              autoHeight
              autoHeightMin={0}
              autoHeightMax={70}
              autoHide={false}
            >
              <p>{answerKey}</p>
            </Scrollbars>
          </div>
          <div className='mask__button'>
            <div className='mask__button image'>
              Close
            </div>
          </div>
          <span>Report to Us</span>
        </div>
      </div>
    );
  };

  const renderQuestionPreviewWithAnswer = (question, index) => {
    return (
      <div className='proposal-summary__preview-wrapper'>
        <div
          className='proposal-summary__preview'
          style={{ backgroundImage: `url(${previewImage})` }}
        >
          <div className='wrapper'>
            <div className='header'>
              <div className='header__ask'>
                <Image src={questionPreview} />
              </div>
              <div className='header__progress'>
                <span className='question-answered--style'>Questions Answered</span>
                <span className='question-answered--count'>{index + 1}/{questionsCount}</span>
              </div>
              <div className='header__cancel'>
                <Image src={cancelPreview} />
              </div>
            </div>
            <div className='content'>
              <h1>
                {question.description}
              </h1>
            </div>
            <div className='footer'>
              {question.type === 'TYPE_TRUE_OR_FALSE' && renderAnswerField(question).trueOrFalse}
              {question.type === 'TYPE_MULTIPLE_CHOICE' && renderAnswerField(question).multipleChoice}
              {question.type === 'TYPE_FILL_THE_BLANK' && renderAnswerField(question).fillTheBlank}
            </div>
          </div>
        </div>
        {renderCorrectAnswerPreview(question.answerKey)}
      </div>
    );
  };

  const renderQuestionPreviewWithoutAnswer = (question, index) => {
    return (
      <>
        <div
          className='proposal-summary__preview'
          style={{ backgroundImage: `url(${previewImage})` }}
        >
          <div className='wrapper'>
            <div className='header'>
              <div className='header__ask'>
                <Image src={questionPreview} />
              </div>
              <div className='header__progress'>
                <span className='question-answered--style'>Questions Answered</span>
                <span className='question-answered--count'>{index + 1}/{questionsCount}</span>
              </div>
              <div className='header__cancel'>
                <Image src={cancelPreview} />
              </div>
            </div>
            <div className='content'>
              <h1>
                {question.description}
              </h1>
            </div>
            <div className='footer'>
              {question.type === 'TYPE_TRUE_OR_FALSE' && renderAnswerField(question).trueOrFalse}
              {question.type === 'TYPE_MULTIPLE_CHOICE' && renderAnswerField(question).multipleChoice}
              {question.type === 'TYPE_FILL_THE_BLANK' && renderAnswerField(question).fillTheBlank}
            </div>
          </div>
        </div>
      </>
    );
  };

  const renderQuestions = (question, index) => {
    return (
      <div className='proposal-summary__wrapper'>
        <div className='proposal-summary__content'>
          <div className='proposal-summary__content file__name'>
            Questions & Answers no. {index + 1}
          </div>
          <Row>
            <Col xs={6}>
              {renderQuestionPreviewWithoutAnswer(question, index)}
            </Col>
            <Col xs={6}>
              {renderQuestionPreviewWithAnswer(question, index)}
            </Col>
          </Row>
        </div>
      </div>
    );
  };

  return (
    <div>
      {renderQuestions(questionData, previewIndex)}
    </div>
  );
};

export default ProposalQuestionPreview;
