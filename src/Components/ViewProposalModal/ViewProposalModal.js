import React, { useEffect, useState } from 'react';
import {
  Carousel, Col,
  Modal, Row
} from 'react-bootstrap';
import { X } from 'react-bootstrap-icons';
import PropTypes from 'prop-types';
import axios from 'axios';
import VideoPlayer from 'react-simple-video-player';
import { useDispatch, useSelector } from 'react-redux';
import Button from '../Button/Button';
import Label from '../Label/Label';
import './ViewProposalModal.scss';
import ProposalQuestionPreview from '../ProposalQuestionPreview/ProposalQuestionPreview';
import { VIDEO_OBJECTS } from '../../Constants/Constants';
import { multipleAnswerField } from '../../Helpers/StaticField';
import { submitQuestion } from '../../Actions/Question';
import {
  list as creativeWorkActionList,
  reset as creativeWorkActionListReset
} from '../../Generated/actions/creativework/list';

const ViewProposalModal = ({
  showModal, setShowModal, mappedImages,
  proposalData, organizationData, addToFinalRanking, cardIndex
}) => {
  const dispatch = useDispatch();
  const [currentSlide, setCurrentSlide] = useState(0);
  const [videoData, setVideoData] = useState({});
  const questions = useSelector((state) => state.questions);
  const creativeWorkList = useSelector((state) => state.creativework.list);

  useEffect(() => {
    if (organizationData.creativeWork !== undefined) {
      const creativeWorkId = organizationData.creativeWork
        && organizationData.creativeWork.split('/')[2];
      dispatch(creativeWorkActionList(
        `/creative_works/${creativeWorkId}/has_parts?order%5Bordering%5D=asc`
      ));
    }
    return () => {
      dispatch(creativeWorkActionListReset(creativeWorkList.eventSource));
    };
  }, [organizationData]);

  useEffect(() => {
    (async () => {
      if (proposalData.video !== undefined) {
        const videoId = proposalData.video && proposalData.video.split('')[2];
        const { data } = axios.get(`${VIDEO_OBJECTS}/${videoId}`);
        setVideoData(data);
      }
    })();
  }, [proposalData]);

  useEffect(() => {
    if (creativeWorkList.retrieved) {
      const completeQuestionData = creativeWorkList.retrieved
        && creativeWorkList.retrieved['hydra:member']
          .map((filteredQuestion) => ({
            ...filteredQuestion,
            description: filteredQuestion.description || '',
            answerKey: filteredQuestion.answerKey || '',
            visualBrief: filteredQuestion.visualBrief || '',
            answers: [],
            answerPreview: multipleAnswerField,
            image: filteredQuestion.image || null
          }));
      dispatch(submitQuestion(completeQuestionData));
    }
  }, [creativeWorkList.retrieved]);

  const handleSelect = (selectedIndex) => {
    setCurrentSlide(selectedIndex);
  };

  const renderHeading = () => {
    return (
      <div className='view-proposal-modal-heading'>
        <span>{organizationData.name}</span>
        <div className='view-proposal-modal-heading-members'>
          {organizationData.members && organizationData.members.map((member) => (
            <img
              src={mappedImages[member]}
              alt=""
              className='lesson-request-detail-ranked-team-avatar'
            />
          ))}
        </div>
      </div>
    );
  };

  const renderAnimation = () => {
    return (
      <div className='proposal-summary__wrapper'>
        <div className='proposal-summary__content'>
          <div className='proposal-summary__content file__name'>
            {videoData.name}
          </div>
          <div className='proposal-summary_content file__vide'>
            <Row>
              <Col xs={12}>
                <VideoPlayer
                  url={videoData.contentUrl}
                />
              </Col>
            </Row>
          </div>
        </div>
      </div>
    );
  };

  const renderContent = () => {
    return (
      <Carousel
        interval={null}
        indicators={false}
        onSelect={handleSelect}
      >
        <Carousel.Item>
          {renderAnimation()}
        </Carousel.Item>

        {questions.map((question, index) => (
          <Carousel.Item>
            <ProposalQuestionPreview
              questionData={question}
              previewIndex={index}
              questionsCount={questions.length}
            />
          </Carousel.Item>
        ))}
      </Carousel>
    );
  };

  return (
    <Modal
      show={showModal}
      dialogClassName='free-material__modal'
      onHide={() => {
        setShowModal();
        setCurrentSlide(0);
      }}
    >
      <div
        className='close'
        onClick={() => setShowModal()}
      >
        <X />
      </div>
      <Modal.Body>
        {renderHeading()}
        <Label type={organizationData.status} />
        {renderContent()}
        <span className='view-proposal-modal-tally' >{currentSlide + 1} of {1 + questions.length}</span>
        <div className='free-material__action'>
          <Row>
            <div className='view-proposal-modal-btn' >
              <Button type='secondary' block={true} onClick={() => {
                addToFinalRanking(cardIndex);
                setShowModal(false);
              }}>
                <i className='view-proposal-modal-btn-heart fa fa-heart' />
                <span className='view-proposal-modal-btn-text'>Lorem Ipsum Dolor</span>
              </Button>
            </div>
          </Row>
        </div>
      </Modal.Body>
    </Modal>
  );
};

ViewProposalModal.propTypes = {
  showModal: PropTypes.bool,
  setShowModal: PropTypes.func,
};

export default ViewProposalModal;
