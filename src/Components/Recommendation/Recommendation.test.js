import React from 'react';
import { shallow } from 'enzyme';
import Recommendation from './Recommendation';

describe('<Recommendation />', () => {
  let props = {};
  beforeEach(() => {
    props = {
      ratingValue: 1,
      name: '',
      job: '',
      company: '',
      date: '',
      skills: [],
      testimonial: '',
    };
  });

  it('render correctly', () => {
    const wrapper = shallow(<Recommendation {...props} />);
    expect(wrapper.exists()).toMatchSnapshot();
  });
});
