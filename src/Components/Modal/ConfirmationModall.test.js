import React from 'react';
import { shallow } from 'enzyme';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import ConfirmationModal from './ConfirmationModal';
import { convertToSnapshot } from '../../Utils/TestUtils/TestUtils';

const mockStore = configureStore([]);
describe('<ConfirmationModal />', () => {
  const setState = jest.fn();
  const useDispatch = jest.fn();
  const useStateSpy = jest.spyOn(React, 'useState');
  useStateSpy.mockImplementation((init) => [init, setState, useDispatch]);
  let store;
  let props = {};
  beforeEach(() => {
    props = {
      isShowModal: false,
      setIsShowModal: false,
    };
    store = mockStore({
      OrganizationStore: { create: { created: null } },
    });
  });

  const renderComponent = () => {
    return shallow(
      <Provider store={store}>
        <ConfirmationModal props={props} />
      </Provider>
    );
  };

  it('render correctly', () => {
    const renderedComponent = renderComponent();

    const actualComponent = convertToSnapshot(renderedComponent);

    expect(actualComponent).toMatchSnapshot();
  });
});
