import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { Image, Modal } from 'react-bootstrap';
import './ConfirmationModal.scss';
import { X } from 'react-bootstrap-icons';
import signInAlby from '../../Assets/Images/signin-modal-alby.png';
import Button from '../Button/Button';
import HandleEscapeKey from '../Input/HandleEscapeKey';

const ConfirmationModal = (props) => {
  const {
    isShowModal,
    setIsShowModal,
    headerMsg,
    bodyMsg,
    handleSubmit
  } = props;

  const [isRequest, setIsRequest] = useState(true);

  const handleSendRequest = () => {
    handleSubmit();
    setIsRequest(false);
    setIsShowModal(false);
  };

  const handleCloseModal = () => {
    setIsShowModal(false);
    setIsRequest(true);
  };

  return (
    <div className='join-team-modal'>
      <Modal
        show={isShowModal}
        dialogClassName='join-team-modal-box'
        backdrop='static'
      >
        <Modal.Body>
          <div className='join-team-modal-box-close'>
            <X onClick={handleCloseModal} />
          </div>
          <div className='join-team-illustration'>
            <Image src={signInAlby} />
          </div>
          <div className='join-team-modal-box-content'>
            {isRequest && (
              <>
                <h4>Are you sure want to {headerMsg} ?</h4>
                <p>{bodyMsg}</p>
                <Button block={true} onClick={handleSendRequest}>
                 Yes
                </Button>
              </>
            )}
          </div>
        </Modal.Body>
      </Modal>
      <HandleEscapeKey handleOnEscapeKey={handleCloseModal} />
    </div>
  );
};

export default ConfirmationModal;

ConfirmationModal.propTypes = {
  bodyMsg: PropTypes.string,
  handleSubmit: PropTypes.string.isRequired,
  headerMsg: PropTypes.string,
  isShowModal: PropTypes.bool.isRequired,
  setIsShowModal: PropTypes.func.isRequired
};
