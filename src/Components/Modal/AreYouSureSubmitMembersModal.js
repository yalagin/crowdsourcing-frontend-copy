import React, { useRef } from 'react';
import { Modal, Form } from 'react-bootstrap';
import { ArrowLeft } from 'react-bootstrap-icons';
import RenderRecipients from '../AddFriend/RenderRecipients';
import './AreYouSureSubmitMembersModal.scss';
import Button from '../Button/Button';

const AreYouSureSubmitMembersModal = (props) => {
  const {
    show,
    handleClose,
    handleGoBack,
    recipients,
    handleSubmitConfirmationModal,
    handleCancelRecipient,
    handleClearRecipients,
  } = props;
  const inputEl = useRef(null);

  const handleSubmit = () => {
    handleSubmitConfirmationModal(inputEl.current.checked);
  };

  return (<>
    <Modal
      show={show}
      onHide={handleClose}
      dialogClassName='add-friend-modal'
      backdrop='static'
      centered
    >
    <Modal.Header closeButton >
      <ArrowLeft
        className='add-friend-modal-back'
        onClick={handleGoBack}
      />
      <Modal.Title>
        Are you sure you want <br />
        to invite them ?
      </Modal.Title>
    </Modal.Header>
    <Modal.Body>
      <div className='add-friend-modal-done'>
        <div className={'blue-border'}>
          <div className="clear-all" onClick={handleClearRecipients}>CLEAR ALL</div>
          <RenderRecipients recipients={recipients} handleCancelRecipient={handleCancelRecipient}/>
        </div>
        <p className={'invite-disclosure'}>
          You can invite people as many as you want but you can only create a team <br/>
          up to 4 people for each proposal.</p>
        <div className={'confirmation-modal-badge bg-light'}>
          <Form.Check
            type="switch"
            label="I'm also open to anyone who asks to join my team"
            id="confirmation-switch"
            className={'confirmation-switch'}
            ref={inputEl}
          />
        </div>
        <Button onClick={handleGoBack} type="secondary" halfWidth={true}>Invite more people</Button>
        <Button onClick={handleSubmit} type="submit" halfWidth={true}>Send invitation</Button>
      </div>
    </Modal.Body>
    </Modal>
  </>);
};

export default AreYouSureSubmitMembersModal;
