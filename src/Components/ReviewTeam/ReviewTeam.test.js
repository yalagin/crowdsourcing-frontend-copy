import React from 'react';
import { shallow } from 'enzyme';
import ReviewTeam from './ReviewTeam';

describe('<ReviewTeam />', () => {
  let props = {};
  beforeEach(() => {
    props = {
      show: false,
      handleClose: () => {},
      teamMembers: [],
    };
  });

  it('render correctly', () => {
    const wrapper = shallow(<ReviewTeam {...props} />);
    expect(wrapper.exists()).toMatchSnapshot();
  });
});
