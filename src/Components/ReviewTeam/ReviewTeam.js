import React, { useState } from 'react';
import { Modal } from 'react-bootstrap';
import { ArrowLeft, StarFill } from 'react-bootstrap-icons';
import './ReviewTeam.scss';
import Rating from 'react-rating';
import Button from '../Button/Button';
import reviewSuccess from '../../Assets/Images/review-success.png';

const ReviewTeam = (props) => {
  const reviews = [];
  const { show, handleClose, teamMembers } = props;
  const [isRenderTeamMembers, setIsRenderTeamMembers] = useState(true);
  const [isReviewMember, setIsReviewMember] = useState(false);
  const [member, setMember] = useState({});
  const [ratingValue, setRatingValue] = useState(0);
  const [textArea, setTextArea] = useState('');
  const [isReview, setIsReview] = useState(false);
  const [skills, setSkills] = useState([
    {
      id: 0,
      name: 'Communication',
      status: false,
    },
    {
      id: 1,
      name: 'Functional Skill',
      status: false,
    },
    {
      id: 2,
      name: 'Organization',
      status: false,
    },
    {
      id: 3,
      name: 'Teamwork',
      status: false,
    },
    {
      id: 4,
      name: 'Leadership',
      status: false,
    },
  ]);

  const removeDuplicate = (array, key) => {
    return array.reduce((arr, item) => {
      const removed = arr.filter((i) => i[key] !== item[key]);
      return [...removed, item];
    }, []);
  };

  const skillsResult = removeDuplicate(skills, 'name').sort((a, b) => {
    return -(b.id - a.id || b.name.localeCompare(a.name));
  });

  const skillset = skillsResult.filter((item) => item.status === true);

  const handleAddReview = (items) => {
    setIsRenderTeamMembers(false);
    setIsReviewMember(true);
    setMember(items);
  };

  const handleBackReview = () => {
    setIsRenderTeamMembers(true);
    setIsReviewMember(false);
    setIsReview(false);
  };

  const handleRating = (rate) => {
    setRatingValue(rate);
  };

  const handleAddSkill = (name, id, status) => {
    setSkills([...skills, { id, name, status: !status }]);
  };

  const handleChangeTextArea = (event) => {
    setTextArea(event.target.value);
  };

  const handleCancelReview = () => {
    setIsRenderTeamMembers(false);
    setIsReviewMember(true);
    setIsReview(false);
  };

  const handleSubmit = () => {
    reviews.push([
      {
        name: member.name,
        skillset,
        review: textArea,
        rating: ratingValue,
        isReview: true,
      },
    ]);
    setIsRenderTeamMembers(false);
    setIsReviewMember(false);
    setIsReview(true);
  };

  const handleAnotherReview = () => {
    setIsReview(false);
    setIsRenderTeamMembers(true);
  };

  const renderTeamMembers = () => {
    return (
      <>
        {teamMembers.map((item, index) => (
          <div key={index} className='review-team-modal-member-profile'>
            <div className='review-team-modal-member-profile-avatar'>
              <img src={item.picture} alt='' />
            </div>
            <div className='review-team-modal-member-profile-info'>
              <h2>{item.name}</h2>
              <p className='review-team-modal-member-profile-info-company'>
                {item.company}
              </p>
              {item.admin ? (
                <button
                  disabled
                  type='disabled'
                  className='review-team-modal-member-profile-info-admin'
                >
                  ADMIN
                </button>
              ) : null}
            </div>
            <div className='review-team-modal-member-profile-add-review'>
              <Button
                onClick={() => handleAddReview(item)}
                className='review-team-modal-member-profile-add-review-button'
              >
                <StarFill /> Review
              </Button>
            </div>
          </div>
        ))}
      </>
    );
  };

  const renderReviewMember = () => {
    return (
      <div className='review-team-modal-review'>
        <div className='review-team-modal-review-avatar'>
          <img src={member.picture} alt='' />
        </div>
        <div className='review-team-modal-member-profile-info'>
          <h2>{member.name}</h2>
          <p className='review-team-modal-member-profile-info-company'>
            {member.company} <div className='middle-dot' />{' '}
            {member.tags.length === 1
              ? member.tags
              : `${member.tags} + ${member.tags.length - 1} Lainnya`}
          </p>
          <Rating
            onChange={(rate) => handleRating(rate)}
            className='review-team-modal-member-profile-info-rating'
            emptySymbol={
              <StarFill className='review-team-modal-member-profile-info-rating-non-active' />
            }
            fullSymbol={
              <StarFill className='review-team-modal-member-profile-info-rating-active' />
            }
            start={0}
            stop={5}
            step={1}
            initialRating={ratingValue}
          />
        </div>
        {ratingValue === 0 ? null : (
          <div className='review-team-modal-member-profile-review'>
            <p className='review-team-modal-member-profile-review-title'>
              {ratingValue <= 3
                ? 'WHAT MAKES HER TERRIBLE'
                : 'WHAT MAKES HER EXCEPTIONAL?'}
            </p>
            <div className='review-team-modal-member-profile-review-skill'>
              {skillsResult.map((item) => (
                <Button
                  key={item.id}
                  onClick={() =>
                    handleAddSkill(item.name, item.id, item.status)
                  }
                  type={item.status ? null : 'secondary'}
                >
                  {item.name}
                </Button>
              ))}
            </div>
            <div className='review-team-modal-member-profile-review-optional'>
              <p className='review-team-modal-member-profile-review-optional-title'>
                SAY SOMETHING MORE ABOUT FOR HER/HIS BRILIANCE! (OPTIONAL)
              </p>
              <textarea
                placeholder='Type something here'
                className='review-team-modal-member-profile-review-optional-textarea'
                onChange={handleChangeTextArea}
              />
              <Button
                type={skillset.length !== 0 ? null : 'disabled'}
                block={true}
                onClick={handleSubmit}
              >
                Kirim Review
              </Button>
            </div>
          </div>
        )}
      </div>
    );
  };

  const renderReviewDone = () => {
    return (
      <div className='review-team-modal-done'>
        <div className='review-team-modal-done-image'>
          <img src={reviewSuccess} alt='' />
        </div>
        <div className='review-team-modal-done-caption'>
          <h4>Yay! Your review has been sent!</h4>
          <p>
            Thank you for being honest and objective to each other. Your review
            means a lot for her/his growth :)
          </p>
        </div>
        <div className='review-team-modal-done-button'>
          <Button block={true} onClick={handleAnotherReview}>
            Ok, Give Another Review
          </Button>
        </div>
      </div>
    );
  };

  return (
    <div className='review-team'>
      <Modal
        show={show}
        onHide={handleClose}
        dialogClassName='review-team-modal'
        backdrop='static'
        centered
      >
        <Modal.Header closeButton>
          {isReviewMember ? (
            <button
              className='review-team-modal-back'
              onClick={handleBackReview}
            >
              <ArrowLeft />
            </button>
          ) : null}
          {isReview ? (
            <button
              className='review-team-modal-back'
              onClick={handleCancelReview}
            >
              <ArrowLeft />
            </button>
          ) : null}
          <Modal.Title>
            {isRenderTeamMembers ? 'Review Team' : `Review ${member.name}`}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {isRenderTeamMembers ? renderTeamMembers() : null}
          {isReviewMember ? renderReviewMember() : null}
          {isReview ? renderReviewDone() : null}
        </Modal.Body>
      </Modal>
    </div>
  );
};

export default ReviewTeam;
