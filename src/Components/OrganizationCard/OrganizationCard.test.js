import React from 'react';
import { shallow } from 'enzyme';
import { convertToSnapshot } from '../../Utils/TestUtils/TestUtils';
import OrganizationCard from './OrganizationCard';

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useSelector: jest.fn((fn) => fn),
  useDispatch: jest.fn()
}));

describe('OrganizationCard', () => {
  const renderComponent = () => {
    return shallow(
      <OrganizationCard
        teamName='Christine and Team'
        lessonName='Science Lesson'
        type='completed'
        organization={{
          '@id': '/organizations/08c2ccfc-be45-3d11-a778-dbaf32fda5ee',
          '@type': 'http://schema.org/Organization',
          id: '08c2ccfc-be45-3d11-a778-dbaf32fda5ee',
          name: 'fugiat',
          members: [
            '/users/8190442f-fcad-3d5e-b2b3-364ea0ca54b4',
            '/users/9b71a45e-5a63-3cdc-ab51-139de3725046',
            '/users/fca181ad-4ad0-3af2-9cbf-322dfa85793a'
          ],
          founder: '/users/8190442f-fcad-3d5e-b2b3-364ea0ca54b4',
          organizationUnconfirmedMembers: [
            '/organization_unconfirmed_members/8cdfc88f-19f8-3706-8416-ce03ccc4c8a3',
            '/organization_unconfirmed_members/d15389f3-fed4-3dca-8a5d-3904e16546fa'
          ],
          project: '/projects/7f119b1b-0954-32b7-925b-c9bb8c47468e',
          creativeWork: '/creative_works/1e1a9535-d426-3eb8-9edd-dd88c3ef954f',
          openForJoin: true,
          locked: false,
          status: 'STATUS_NEED_PARTNER'
        }}/>
    );
  };

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('#render', () => {
    it('should render component OrganizationCard correctly when invoked', () => {
      const renderedComponent = renderComponent();

      const actualComponent = convertToSnapshot(renderedComponent);

      expect(actualComponent).toMatchSnapshot();
    });
  });
});
