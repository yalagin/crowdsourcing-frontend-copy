import { Col, Row } from 'react-bootstrap';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import OrganizationCard from '../OrganizationCard/OrganizationCard';
import NoProjectsTab from '../NoProjectsTab/NoProjectsTab';
import GetUserDataBasedOnOrganizations from './GetUserDataBasedOnOrganizations';
import RenderFilterButtons from './RenderFilterButtons';

const ProjectListTab = () => {
  const organizationList = useSelector((state) => state.organization.searchList);
  const [refreshUsersOrg, setRefreshUsersOrg] = useState(false);
  // eslint-disable-next-line no-unused-vars
  const data = GetUserDataBasedOnOrganizations(true);

  return (
        <>
            {/* we get organizations in filter */}
            <RenderFilterButtons
                refreshUsersOrg={refreshUsersOrg}
                setRefreshUsersOrg={setRefreshUsersOrg}
            />
            {organizationList.retrieved && organizationList.retrieved['hydra:totalItems']
              ? (
                    <Row className='my-projects-container-row'>
                        {organizationList.retrieved && organizationList.retrieved['hydra:member']
                          .map((userOrganization) => (
                                <Col xs={6} as='div' className='my-projects-container-row-col'>
                                    <OrganizationCard
                                        key={userOrganization.id}
                                        organization={userOrganization}
                                        setRefreshUsersOrg={setRefreshUsersOrg}
                                    />
                                </Col>
                          ))}
                    </Row>
              )
              : (
                    <NoProjectsTab />
              )}
        </>
  );
};

export default ProjectListTab;
