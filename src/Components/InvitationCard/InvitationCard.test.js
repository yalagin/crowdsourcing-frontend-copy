import React from 'react';
import { shallow } from 'enzyme';
import { convertToSnapshot } from '../../Utils/TestUtils/TestUtils';
import InvitationCard from './InvitationCard';

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useSelector: jest.fn((fn) => fn),
  useDispatch: jest.fn()
}));

describe('InvitationCard', () => {
  const renderComponent = () => {
    return shallow(
      <InvitationCard
          unconfirmedMember={
            {
              '@id': '/organization_unconfirmed_members/1be6ad93-e96f-3e5f-a542-f865c2dd4fd2',
              '@type': 'http://schema.org/Organization',
              id: '1be6ad93-e96f-3e5f-a542-f865c2dd4fd2',
              member: '/users/f30d3f66-6fcb-302e-ad11-016675b86328',
              organization: '/organizations/c10a3881-58c9-3879-8596-2728a29a5aad',
              status: 'STATUS_REJECTED'
            }
          }
          setRefresh={() => {}}
      />
    );
  };

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('#render', () => {
    it('should render component InvitationCard correctly when invoked', () => {
      const renderedComponent = renderComponent();

      const actualComponent = convertToSnapshot(renderedComponent);

      expect(actualComponent).toMatchSnapshot();
    });
  });
});
