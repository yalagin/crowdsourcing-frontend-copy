import React from 'react';
import NoProjectImage from '../../Assets/Images/open-to-anyone.png';
import Button from '../Button/Button';

const NoProjectsTab = () => {
  return (
        <div className='my-projects-no-projects'>
            <img className='my-projects-no-projects-img' src={NoProjectImage} alt="" />
            <span className='my-projects-no-projects-heading'>You have No Projects Yet</span>
            <span className='my-projects-no-projects-heading-sub'>
          Find projects that you want to create, and invite others to join you!
        </span>
            <div className='my-projects-no-projects-btn'>
                <Button block>Browse Project</Button>
            </div>
        </div>
  );
};
export default NoProjectsTab;
