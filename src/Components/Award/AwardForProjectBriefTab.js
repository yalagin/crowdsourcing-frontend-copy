import PropTypes from 'prop-types';
import { Col, Row } from 'react-bootstrap';
import React from 'react';

const AwardForProjectBriefTab = ({ award }) => {
  return (
        <div className={'question-answer-overview-content-wrapper'}>
            <Row>
                <Col xs={2}>
                    <h5>Award for {award.rank} place</h5>
                </Col>
                <Col xs={10}>
                    <p>
                        {award.name}
                    </p>
                </Col>
            </Row>
        </div>
  );
};
export default AwardForProjectBriefTab;

AwardForProjectBriefTab.propTypes = {
  award: PropTypes.object
};
