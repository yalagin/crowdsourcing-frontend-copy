import React from 'react';
import { shallow } from 'enzyme';
import StartProposalModal from './StartProposalModal';

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useSelector: jest.fn((fn) => fn),
  useDispatch: jest.fn()
}));

describe('<StartProposalModal />', () => {
  let props = {};
  beforeEach(() => {
    props = {
      isShowModal: false,
      setIsShowModal: false,
    };
  });

  xit('render correctly', () => {
    const wrapper = shallow(<StartProposalModal {...props} />);
    expect(wrapper.exists()).toMatchSnapshot();
  });
});
