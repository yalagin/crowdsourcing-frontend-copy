import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import {
  ChevronDown,
  ChevronUp
} from 'react-bootstrap-icons';
import './ProfileDropdown.scss';
import avatarImage from '../../Assets/Images/avatar-image.png';

const ProfileDropdown = (props) => {
  const [isActiveDropdown, setIsActiveDropdown] = useState(false);
  const {
    name
  } = props;
  const wrapperRef = useRef(null);
  const loggedUser = JSON.parse(localStorage.getItem('user'));

  const handleClickOutside = (event) => {
    if (
      wrapperRef.current
      && !wrapperRef.current.contains(event.target)
    ) {
      setIsActiveDropdown(false);
    }
  };

  useEffect(() => {
    document.addEventListener('click', handleClickOutside, true);
    return () => {
      document.removeEventListener('click', handleClickOutside, true);
    };
  });

  const handleActiveDropdown = () => {
    setIsActiveDropdown(!isActiveDropdown);
  };

  const resultName = name.split(' ');

  return (
    <div className='profile-dropdown' ref={wrapperRef}>
      <div
        className={`profile-dropdown-button 
        ${isActiveDropdown ? 'profile-dropdown-button-active' : ''}`}
        onClick={handleActiveDropdown}
      >
        <div className={`profile-dropdown-button-avatar 
        ${isActiveDropdown ? 'profile-dropdown-button-avatar-active' : ''}`}>
          <img src={avatarImage} alt=''/>
        </div>
        <p>
          {resultName.length >= 2
            ? `${resultName[0]} ${resultName[1].charAt(0).toUpperCase()}.`
            : name}
        </p>
        <div className='profile-dropdown-button-arrow'>
          {isActiveDropdown ? <ChevronUp /> : <ChevronDown />}
        </div>
      </div>
      {isActiveDropdown
      && <div
        className='profile-dropdown-content'>
        <ul>
          <li>
            <NavLink to='/profile'>
              View Profile
            </NavLink>
          </li>
          <li>
            <NavLink to='/my-projects'>
              My Projects
            </NavLink>
          </li>
          {loggedUser.roles.includes('ROLE_ADMIN') && (
            <li>
              <NavLink to='/admin/lesson-requests'>
                Admin
              </NavLink>
            </li>)}
          <li>
            <NavLink to='/logout'>
              Sign Out
            </NavLink>
          </li>
        </ul>
      </div>}
    </div>
  );
};

ProfileDropdown.propTypes = {
  name: PropTypes.string,
  dropdownContents: PropTypes.array
};

ProfileDropdown.defaultProps = {
  name: '',
  dropdownContents: []
};

export default ProfileDropdown;
