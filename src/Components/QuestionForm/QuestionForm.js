import React, {
  useEffect, useRef, useState
} from 'react';
import {
  Col, Form,
  Image, Row
} from 'react-bootstrap';
import { v4 as uuidv4 } from 'uuid';
import CharacterCounter from 'react-character-counter';
import { CardImage, TrashFill, X } from 'react-bootstrap-icons';
import './QuestionForm.scss';
import { useDispatch, useSelector } from 'react-redux';
import { cloneDeep } from 'lodash';
import axios from 'axios';
import Button from '../Button/Button';
import trueOrFalseIcon from '../../Assets/Images/true-or-false.png';
import multipleChoiceIcon from '../../Assets/Images/multiple-choice.png';
import fillTheBlankIcon from '../../Assets/Images/fill-the-blank.png';
import { submitQuestion, uploadImageToS3 } from '../../Actions/Question';
import { generateRandomString } from '../../Helpers/Common';
import {
  multipleChoiceField,
  trueOrFalseField
} from '../../Helpers/StaticField';
import { ANSWERS_URL, QUESTIONS_URL } from '../../Constants/Constants';
import { useFetch } from '../../Hooks/UseFetch/UseFetch';

const QuestionForm = (props) => {
  const dispatch = useDispatch();
  const questionList = useSelector((state) => state.questions);
  const errorUploadImage = useSelector((state) => state.questions.errorUploadImage);
  const [trueOrFalseAnswer, setTrueOrFalseAnswer] = useState(cloneDeep(trueOrFalseField));
  const [multipleChoiceAnswer, setMultipleChoiceAnswer] = useState(cloneDeep(multipleChoiceField));
  const [fillTheBlankAnswer, setFillTheBlankAnswer] = useState([]);
  const inputFile = useRef(null);
  const {
    questionData, questionIndex, setPreviewCorrectAnswer,
    previewContent, setPreviewContent
  } = props;
  const fetchAnswerURL = `${QUESTIONS_URL}/${questionData.id}/connected_answers?order%5Bordering%5D=asc`;
  const { fetchedData: { data: answerData } } = useFetch(fetchAnswerURL);

  useEffect(() => {
    if (answerData && questionData.type === 'TYPE_TRUE_OR_FALSE') {
      const trueOrFalse = [...trueOrFalseAnswer];
      answerData['hydra:member'].map((answer, index) => {
        trueOrFalseAnswer[index]['@id'] = answer['@id'];
        trueOrFalseAnswer[index].id = answer.id;
        trueOrFalseAnswer[index].name = answer.description;
        trueOrFalseAnswer[index].selected = answer.correct;
        if (answer.correct === true) {
          questionList[questionIndex].haveOneCorrectAnswer = true;
        }
        return trueOrFalse;
      });
      setTrueOrFalseAnswer(trueOrFalse);
    }
  }, [answerData, questionData]);

  useEffect(() => {
    if (answerData && questionData.type === 'TYPE_MULTIPLE_CHOICE') {
      const answers = answerData['hydra:member'].slice(0, 4);
      const multipleChoice = [...multipleChoiceAnswer];
      answers.map((answer, index) => {
        multipleChoice[index]['@id'] = answer['@id'];
        multipleChoice[index].id = answer.id;
        multipleChoice[index].name = answer.description || '';
        if (answer.correct === true) {
          multipleChoice[index].selected = true;
          questionList[questionIndex].haveOneCorrectAnswer = true;
        }
        return multipleChoice;
      });
      setMultipleChoiceAnswer(multipleChoice);
    }
  }, [answerData, questionData]);

  useEffect(() => {
    const listOfQuestion = [...questionList];
    multipleChoiceAnswer.map((answer, index) => {
      listOfQuestion[questionIndex].answerPreview[index].name = answer.name;
      return listOfQuestion;
    });
    dispatch(submitQuestion(listOfQuestion));
  }, [multipleChoiceAnswer]);

  useEffect(() => {
    if (answerData && questionData.type === 'TYPE_FILL_THE_BLANK') {
      const fillTheBlank = [];
      answerData['hydra:member'].map((answer) => {
        const generateAnswer = {
          ...answer,
          name: answer.description
        };
        fillTheBlank.push(generateAnswer);
        return fillTheBlank;
      });
      setFillTheBlankAnswer(fillTheBlank);
    }
  }, [answerData, questionData]);

  const updateQuestion = async (updateData) => {
    const listOfQuestion = [...questionList];
    try {
      await axios.put(`${QUESTIONS_URL}/${listOfQuestion[questionIndex].id}`, updateData);
    } catch (error) {
      throw new Error(error);
    }
  };

  const updateAnswer = async (answerIndex, updateData) => {
    const listOfQuestion = [...questionList];
    try {
      if (listOfQuestion[questionIndex].connectedAnswers[answerIndex] === undefined) {
        const createAnswer = {
          id: updateData.id,
          creator: questionData.creator,
          parentItem: `/questions/${questionData.id}`,
          description: updateData.description,
          ordering: updateData.ordering
        };
        return await axios.post(`${ANSWERS_URL}`, createAnswer);
      }
      const getAnswerId = listOfQuestion[questionIndex].connectedAnswers[answerIndex].split('/');
      return await axios.put(`${ANSWERS_URL}/${getAnswerId[2]}`, { description: updateData.description });
    } catch (error) {
      throw new Error(error);
    }
  };

  const handleUpdateCorrectAnswer = async (answerId) => {
    const listOfQuestion = [...questionList];
    try {
      if (listOfQuestion[questionIndex].connectedAnswers) {
        const getAnswerId = answerId.split('/');
        await axios.put(`${ANSWERS_URL}/${getAnswerId[2]}`, { correct: true });
        const updateToFalse = listOfQuestion[questionIndex].connectedAnswers
          .filter((answer) => answer !== answerId);
        updateToFalse.map(async (answer) => {
          const getUpdateToFalseId = answer.split('/');
          await axios.put(`${ANSWERS_URL}/${getUpdateToFalseId[2]}`, { correct: false });
        });
        questionList[questionIndex].haveOneCorrectAnswer = true;
      }
    } catch (error) {
      throw new Error(error);
    }
  };

  const handleInputQuestion = (event) => {
    const { target: { value } } = event;
    const listOfQuestion = [...questionList];
    listOfQuestion[questionIndex].description = value;
    dispatch(submitQuestion(listOfQuestion));
  };

  const handleAnswerKey = (event) => {
    const { target: { value } } = event;
    const listOfQuestion = [...questionList];
    listOfQuestion[questionIndex].answerKey = value;
    dispatch(submitQuestion(listOfQuestion));
  };

  const handleDeleteQuestion = async (event, questionId) => {
    event.preventDefault();
    event.stopPropagation();
    try {
      const listOfQuestion = [...questionList];
      const resultQuestion = listOfQuestion.filter((question) => questionId !== question.id);
      if (previewContent > 0) {
        setPreviewContent(resultQuestion.length - 1);
      }
      if (previewContent === 0) {
        setPreviewContent(0);
      }
      const deletedQuestion = listOfQuestion.filter((question) => question.id === questionId);
      if (deletedQuestion[0].connectedAnswers.length !== 0) {
        deletedQuestion[0].connectedAnswers.map(async (answer) => {
          const getAnswerId = answer.split('/');
          await axios.delete(`${ANSWERS_URL}/${getAnswerId[2]}`);
        });
      }
      await axios.delete(`${QUESTIONS_URL}/${questionId}`);
      dispatch(submitQuestion(resultQuestion));
    } catch (error) {
      throw new Error(error);
    }
  };

  const renderQuestionIcon = (questionType) => {
    if (questionType === 'TYPE_TRUE_OR_FALSE') {
      return (
        <Image src={trueOrFalseIcon}/>
      );
    }
    if (questionType === 'TYPE_MULTIPLE_CHOICE') {
      return (
        <Image src={multipleChoiceIcon}/>
      );
    }
    if (questionType === 'TYPE_FILL_THE_BLANK') {
      return (
        <Image src={fillTheBlankIcon} width='18'/>
      );
    }
    return null;
  };

  const handleOnBlurQuestionInput = async () => {
    await updateQuestion({ description: questionList[questionIndex].description });
  };

  const renderQuestionInput = (questionType) => {
    return (
      <div className='question-form-input'>
        <div className='question-form-input-icon'>
          <div className='question-form-input-icon-round'>
            {renderQuestionIcon(questionType)}
          </div>
          <h5>
            {questionIndex + 1}
          </h5>
        </div>
         <CharacterCounter
           value={questionList[questionIndex].description || ''}
           maxLength={80}
         >
          <Form.Control
            className=''
            type='text'
            maxLength={80}
            value={questionList[questionIndex].description}
            placeholder='Type your question here'
            onChange={handleInputQuestion}
            onFocus={() => setPreviewContent(questionIndex)}
            onBlur={() => handleOnBlurQuestionInput()}
          />
         </CharacterCounter>
      </div>
    );
  };

  const handleAnswerTrueOrFalseSwitch = async (event, index, answerId) => {
    const listOfTrueOrFalseQuestion = [...trueOrFalseAnswer];
    setPreviewContent(questionIndex);
    if (event.target.value === 'on') {
      if (index === 0) {
        listOfTrueOrFalseQuestion[0].selected = true;
        listOfTrueOrFalseQuestion[1].selected = false;
      }
      if (index === 1) {
        listOfTrueOrFalseQuestion[0].selected = false;
        listOfTrueOrFalseQuestion[1].selected = true;
      }
      await handleUpdateCorrectAnswer(answerId);
      return setTrueOrFalseAnswer(listOfTrueOrFalseQuestion);
    }
    return null;
  };

  const renderAnswerTrueOrFalse = () => {
    return trueOrFalseAnswer.map((answer, index) => (
      <Col xs={6}>
        <div
          className={`
            question-form-answer
            ${answer.selected && 'question-form-answer-selected'}
          `}
        >
          <div className='question-form-answer-items'>
            <div className='question-form-answer-option'>
              {answer.option}
            </div>
            <div className='question-form-answer-text'>{answer.name}</div>
          </div>
          <div className='question-form-answer-switch'>
             <Form.Check
              onChange={(event) => handleAnswerTrueOrFalseSwitch(event, index, answer['@id'])}
              checked={answer.selected}
              type='switch'
              id={`${generateRandomString(5)}-switch`}
              label=''
             />
          </div>
        </div>
      </Col>
    ));
  };

  const handleOnChangeMultiple = (event, index) => {
    const { target: { value } } = event;
    const listOfMultipleChoice = [...multipleChoiceAnswer];
    const lisOfQuestion = [...questionList];
    setPreviewContent(questionIndex);
    listOfMultipleChoice[index].name = value;
    lisOfQuestion[questionIndex].answerPreview[index].name = value;
    setMultipleChoiceAnswer(listOfMultipleChoice);
    dispatch(submitQuestion(lisOfQuestion));
  };

  const handleAnswerMultipleSwitch = async (event, answer, answerId) => {
    const listOfMultipleChoice = [...multipleChoiceAnswer];
    setPreviewContent(questionIndex);
    if (event.target.value === 'on') {
      if (answer === 'A') {
        listOfMultipleChoice[0].selected = true;
        listOfMultipleChoice[1].selected = false;
        listOfMultipleChoice[2].selected = false;
        listOfMultipleChoice[3].selected = false;
      }
      if (answer === 'C') {
        listOfMultipleChoice[0].selected = false;
        listOfMultipleChoice[1].selected = true;
        listOfMultipleChoice[2].selected = false;
        listOfMultipleChoice[3].selected = false;
      }
      if (answer === 'B') {
        listOfMultipleChoice[0].selected = false;
        listOfMultipleChoice[1].selected = false;
        listOfMultipleChoice[2].selected = true;
        listOfMultipleChoice[3].selected = false;
      }
      if (answer === 'D') {
        listOfMultipleChoice[0].selected = false;
        listOfMultipleChoice[1].selected = false;
        listOfMultipleChoice[2].selected = false;
        listOfMultipleChoice[3].selected = true;
      }
      await handleUpdateCorrectAnswer(answerId);
      return setMultipleChoiceAnswer(listOfMultipleChoice);
    }
    return null;
  };

  const handleUpdateAnswerMultiple = async (index, description) => {
    await updateAnswer(index, { description });
  };

  const renderAnswerMultiple = () => {
    return (
      <>
        {multipleChoiceAnswer && multipleChoiceAnswer.map((answer, index) => (
          <Col key={index} xs={6}>
            <div
              className={`question-form-answer
                  ${answer.selected && 'question-form-answer-selected'}`}
            >
              <div className='question-form-answer-items'>
                <div className='question-form-answer-option'>
                  {answer.option}
                </div>
                <div className='question-form-answer-text'>
                   <CharacterCounter
                    value={answer.name || ''}
                    maxLength={25}
                   >
                    <Form.Control
                      maxLength={25}
                      type='text'
                      className='question-form-answer-text-input'
                      value={answer.name}
                      onChange={(event) => handleOnChangeMultiple(event, index)}
                      onFocus={() => setPreviewContent(questionIndex)}
                      onBlur={() => handleUpdateAnswerMultiple(index, answer.name)}
                    />
                   </CharacterCounter>
                </div>
              </div>
              <div className='question-form-answer-switch'>
                 <Form.Check
                  checked={answer.selected}
                  onChange={(event) => handleAnswerMultipleSwitch(event, answer.option, answer['@id'])}
                  type='switch'
                  id={`${generateRandomString(5)}-switch`}
                  label=''
                 />
              </div>
            </div>
          </Col>
        ))}
      </>
    );
  };

  const handleOnChangeOption = (event, index) => {
    const { target: { value } } = event;
    const listOfFillTheBlank = [...fillTheBlankAnswer];
    setPreviewContent(questionIndex);
    listOfFillTheBlank[index].name = value;
    const listOfQuestion = [...questionList];
    listOfQuestion[questionIndex].answers = listOfFillTheBlank;
    dispatch(submitQuestion(listOfQuestion));
    setFillTheBlankAnswer(listOfFillTheBlank);
  };

  const handleGenerateInputAnswer = async () => {
    try {
      const listOfFillTheBlank = [...fillTheBlankAnswer];
      setPreviewContent(questionIndex);
      const createAnswer = {
        id: uuidv4(),
        creator: questionData.creator,
        parentItem: `/questions/${questionData.id}`,
        ordering: listOfFillTheBlank.length,
        correct: true
      };
      await axios.post(`${ANSWERS_URL}`, createAnswer);
      listOfFillTheBlank.push(createAnswer);
      const listOfQuestion = [...questionList];
      listOfQuestion[questionIndex].answers = listOfFillTheBlank;
      dispatch(submitQuestion(listOfQuestion));
      setFillTheBlankAnswer(listOfFillTheBlank);
    } catch (error) {
      throw new Error(error);
    }
  };

  const handleCancelAnswer = async (index, answerId) => {
    try {
      setPreviewContent(questionIndex);
      const fillTheBlankAnswerOption = [...fillTheBlankAnswer];
      fillTheBlankAnswerOption.splice(index, 1);
      const listOfQuestion = [...questionList];
      listOfQuestion[questionIndex].answers = fillTheBlankAnswerOption;
      dispatch(submitQuestion(listOfQuestion));
      setFillTheBlankAnswer(fillTheBlankAnswerOption);
      await axios.delete(`${ANSWERS_URL}/${answerId}`);
    } catch (error) {
      throw new Error(error);
    }
  };

  const handleUpdateAnswerFillBlank = async (index) => {
    try {
      const listOfFillTheBlank = [...fillTheBlankAnswer];
      const updateData = {
        description: listOfFillTheBlank[index].name
      };
      if (questionList[questionIndex].answers[index]) {
        const getAnswerId = questionList[questionIndex].answers[index].id;
        await axios.put(`${ANSWERS_URL}/${getAnswerId}`, updateData);
      }
    } catch (error) {
      throw new Error(error);
    }
  };

  const renderAnswerFillTheBlank = () => {
    return (
      <Col xs={12}>
        <Row>
          <div className='question-form-answer-fill' id='question-form-answer-fill'>
            {fillTheBlankAnswer.map((answer, index) => (
              <Col xs={12} key={index}>
                <div
                  className='question-form-answer'
                >
                  <div className='question-form-answer-items'>
                    <div className='question-form-answer-option'>
                      {index + 1}
                    </div>
                    <div className='question-form-answer-text'>
                       <CharacterCounter
                        value={answer.name || ''}
                        maxLength={25}
                       >
                        <Form.Control
                          type='text'
                          className='question-form-answer-text-input'
                          onChange={(event) => handleOnChangeOption(event, index)}
                          value={answer.name}
                          maxLength={25}
                          onFocus={() => setPreviewContent(questionIndex)}
                          onBlur={() => handleUpdateAnswerFillBlank(index, answer.name)}
                        />
                       </CharacterCounter>
                    </div>
                  </div>
                  <div
                    className='question-form-answer-cancel'
                    onClick={() => handleCancelAnswer(index, answer.id)}
                  >
                    <X/>
                  </div>
                </div>
              </Col>
            ))}
          </div>
          <Col xs={12}>
            <p
              className='question-form-tooltip'
              onClick={() => handleGenerateInputAnswer()}>
              Click here to add one or more correct answers
            </p>
          </Col>
        </Row>
      </Col>
    );
  };

  const answerKeyOnFocus = () => {
    setPreviewContent(questionIndex);
    setPreviewCorrectAnswer(questionIndex);
  };

  const handleOnBlurAnswerKey = async () => {
    await updateQuestion({ answerKey: questionList[questionIndex].answerKey });
    setPreviewCorrectAnswer(undefined);
  };

  const renderAnswerKeyInput = () => {
    return (
      <div className='question-form-answer-key'>
        <Form.Group>
          <Form.Label>Answer Key</Form.Label>
           <CharacterCounter
            value={questionList[questionIndex].answerKey || ''}
            maxLength={150}>
            <Form.Control
              as="textarea"
              className='question-form-answer-key-textarea'
              maxLength={150}
              placeholder='Jelaskan tentang alasan jawaban yang benar'
              value={questionList[questionIndex].answerKey}
              onChange={handleAnswerKey}
              onFocus={answerKeyOnFocus}
              onBlur={() => handleOnBlurAnswerKey()}
            />
           </CharacterCounter>
        </Form.Group>
      </div>
    );
  };

  const handleVisualBrief = (event) => {
    const { target: { value } } = event;
    const listOfQuestion = [...questionList];
    listOfQuestion[questionIndex].visualBrief = value;
    dispatch(submitQuestion(listOfQuestion));
  };

  const visualBriefOnFocus = () => {
    setPreviewContent(questionIndex);
  };

  const handleOnBlurVisualBrief = async () => {
    await updateQuestion({ visualBrief: questionList[questionIndex].visualBrief });
  };

  const renderVisualBriefInput = () => {
    return (
      <div className='question-form-answer-key'>
        <Form.Group>
          <Form.Label>
            Visual Brief to Designer (Optional)
            <div className='question-form-visual-brief-label-mark' >?
             <div className='question-form-visual-brief-label-mark-tooltip' >
               Help designer to visualize the lesson
              </div>
            </div>
          </Form.Label>
          <CharacterCounter
            value={questionList[questionIndex].visualBrief || ''}
            maxLength={150}>
            <Form.Control
              as="textarea"
              className='question-form-answer-key-textarea'
              maxLength={150}
              placeholder='Explain how this background image should look, what elements it has'
              value={questionList[questionIndex].visualBrief}
              onChange={handleVisualBrief}
              onFocus={() => visualBriefOnFocus()}
              onBlur={() => handleOnBlurVisualBrief()}
            />
          </CharacterCounter>
        </Form.Group>
      </div>
    );
  };

  const handleUploadFile = () => {
    setPreviewContent(questionIndex);
    inputFile.current.click();
  };

  const handleOnChangeFile = async (event) => {
    event.preventDefault();
    event.persist();
    event.stopPropagation();
    const { target: { files } } = event;
    if (files[0] && files[0] !== undefined) {
      const getTypeImage = files[0].type.split('/');
      const fileType = {
        fileExtension: getTypeImage[1]
      };
      const uploadedData = await dispatch(uploadImageToS3(fileType, files[0]));
      if (uploadedData) {
        const newListOfQuestion = [...questionList];
        newListOfQuestion[questionIndex].image = uploadedData['@id'];
        dispatch(submitQuestion(newListOfQuestion));
        const updateData = {
          image: uploadedData['@id']
        };
        await updateQuestion(updateData);
      }
    }
  };

  const renderUploadAndDeleteButton = () => {
    return (
      <div className='question-form-buttons'>
        <input
          type='file'
          id='upload-file-question'
          ref={inputFile}
          style={{ display: 'none' }}
          onChange={handleOnChangeFile}
          onFocus={() => setPreviewContent(questionIndex)}
        />
        <div className='question-form-upload'>
          <Button onClick={handleUploadFile}>
            <CardImage/> Choose File (.JPG)
          </Button>
          {errorUploadImage && 'File must be .JPG format!'}
        </div>
        <div
          className='question-form-buttons-remove'
          onClick={(event) => handleDeleteQuestion(event, questionData.id)}
        >
          <TrashFill/>
        </div>
      </div>
    );
  };

  const optionContents = {
    trueOrFalse: renderAnswerTrueOrFalse(),
    multipleChoice: renderAnswerMultiple(),
    fillTheBlank: renderAnswerFillTheBlank()
  };

  return (
    <div
      className='question-form'
      id={`question-form-id-${questionIndex + 1}`}
      onClick={() => setPreviewContent(questionIndex)}
    >
      <Row>
        <Col xs={12}>
          {renderQuestionInput(questionData.type)}
        </Col>
      </Row>
      <Row>
        {questionData.type === 'TYPE_TRUE_OR_FALSE' && optionContents.trueOrFalse}
        {questionData.type === 'TYPE_MULTIPLE_CHOICE' && optionContents.multipleChoice}
        {questionData.type === 'TYPE_FILL_THE_BLANK' && optionContents.fillTheBlank}
      </Row>
      <Row>
        <Col xs={12}>
          {renderAnswerKeyInput()}
        </Col>
      </Row>
      <Row>
        <Col xs={12}>
          {renderVisualBriefInput()}
        </Col>
      </Row>
      <Row>
        <Col xs={12}>
          {renderUploadAndDeleteButton()}
        </Col>
      </Row>
    </div>
  );
};

export default QuestionForm;
