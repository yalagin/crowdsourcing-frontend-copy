import React from 'react';
import { Col, Image, Row } from 'react-bootstrap';
import { Scrollbars } from 'react-custom-scrollbars';
import Input from '../Input/Input';
import submitPreview from '../../Assets/Images/submit-preview.png';
import correctIcon from '../../Assets/Images/correct-icon.png';
import questionPreview from '../../Assets/Images/question-preview.png';
import cancelPreview from '../../Assets/Images/cancel-preview.png';
import './SmallQuestionPreview.scss';

const SmallQuestionPreview = (props) => {
  const { questionData, questionIndex, questionLength } = props;
  const renderAnswerField = (question) => ({
    trueOrFalse: (
      <div className='footer__answer margin-top--80'>
        <div className='footer__answer-button'>
          <h1>True</h1>
        </div>
        <div className='footer__answer-button'>
          <h1>False</h1>
        </div>
      </div>
    ),
    multipleChoice: (
      <>
        <div className='footer__answer'>
          <div className='footer__answer-button'>
            <h1>{question.type === 'multiple-choice' && question.multipleOptions[0].name}</h1>
          </div>
          <div className='footer__answer-button'>
            <h1>{question.type === 'multiple-choice' && question.multipleOptions[1].name}</h1>
          </div>
        </div>
        <div className='footer__answer'>
          <div className='footer__answer-button'>
            <h1>{question.type === 'multiple-choice' && question.multipleOptions[2].name}</h1>
          </div>
          <div className='footer__answer-button'>
            <h1>{question.type === 'multiple-choice' && question.multipleOptions[3].name}</h1>
          </div>
        </div>
      </>
    ),
    fillTheBlank: (
      <div className='footer__answer margin-top--80'>
        <Input placeholder='TYPE ANSWER HERE'/>
        <Image src={submitPreview}/>
      </div>
    )
  });

  const renderCorrectAnswerPreview = (answerKey) => {
    return (
      <div className='mask'>
        <div className='mask__text'>
          <h1>Yay</h1>
        </div>
        <div className='mask__icon'>
          <Image src={correctIcon}/>
        </div>
        <div className='mask__description'>
          <h1>Correct</h1>
          <div className='mask__key'>
            <Scrollbars
              autoHeight
              autoHeightMin={0}
              autoHeightMax={70}
              autoHide={false}
            >
              <p>{answerKey}</p>
            </Scrollbars>
          </div>
          <div className='mask__button'>
            <div className='mask__button image'>
              Close
            </div>
          </div>
          <span>Report to Us</span>
        </div>
      </div>
    );
  };

  const renderQuestionPreviewWithAnswer = (question, index) => {
    return (
      <div className='wrapper'>
        <div
          className='small-question__preview'
          style={{ backgroundImage: `url(${question.image})` }}
        >
          <div className='wrapper'>
            <div className='header'>
              <div className='header__ask'>
                <Image src={questionPreview}/>
              </div>
              <div className='header__progress'>
                <span className='question-answered--style'>Questions Answered</span>
                <span className='question-answered--count'>{index + 1}/{questionLength}</span>
              </div>
              <div className='header__cancel'>
                <Image src={cancelPreview}/>
              </div>
            </div>
            <div className='content'>
              <h1>
                {question.question}
              </h1>
            </div>
            <div className='footer'>
              {question.type === 'true-or-false' && renderAnswerField(question).trueOrFalse}
              {question.type === 'multiple-choice' && renderAnswerField(question).multipleChoice}
              {question.type === 'fill-the-blank' && renderAnswerField(question).fillTheBlank}
            </div>
          </div>
        </div>
        {renderCorrectAnswerPreview(question.answerKey)}
      </div>
    );
  };

  const renderQuestionPreviewWithoutAnswer = (question, index) => {
    return (
      <>
        <div
          className='small-question__preview'
          style={{ backgroundImage: `url(${question.image})` }}
        >
          <div className='wrapper'>
            <div className='header'>
              <div className='header__ask'>
                <Image src={questionPreview}/>
              </div>
              <div className='header__progress'>
                <span className='question-answered--style'>Questions Answered</span>
                <span className='question-answered--count'>{index + 1}/{questionLength}</span>
              </div>
              <div className='header__cancel'>
                <Image src={cancelPreview}/>
              </div>
            </div>
            <div className='content'>
              <h1>
                {question.question}
              </h1>
            </div>
            <div className='footer'>
              {question.type === 'true-or-false' && renderAnswerField(question).trueOrFalse}
              {question.type === 'multiple-choice' && renderAnswerField(question).multipleChoice}
              {question.type === 'fill-the-blank' && renderAnswerField(question).fillTheBlank}
            </div>
          </div>
        </div>
      </>
    );
  };

  const renderQuestions = (question, index) => {
    return (
      <div className='small-question__wrapper'>
        <div className='small-question__content'>
          <div className='small-question__question-indicator'>
            Question {index + 1} of {questionLength}
          </div>
          <Row>
            <Col xs={6}>
              {renderQuestionPreviewWithoutAnswer(question, index)}
            </Col>
            <Col xs={6}>
              {renderQuestionPreviewWithAnswer(question, index)}
            </Col>
          </Row>
        </div>
      </div>
    );
  };

  return (
    <div className='small-question'>
      {renderQuestions(questionData, questionIndex)}
    </div>
  );
};

export default SmallQuestionPreview;
