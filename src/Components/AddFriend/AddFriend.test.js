import React from 'react';
import { shallow } from 'enzyme';
import AddFriend from './AddFriend';

describe('<AddFriend />', () => {
  let props = {};
  beforeEach(() => {
    props = {
      show: false,
      handleClose: () => {},
      dataMembers: [],
      handleChange: () => {},
      search: '',
      recipients: [],
      handleClickAddRecipient: () => {},
      handleCancelRecipient: () => {},
      counter: 0,
      handleSubmitRecipients: () => {},
      isSubmit: false,
    };
  });

  it('render correctly', () => {
    const wrapper = shallow(<AddFriend {...props} />);
    expect(wrapper.exists()).toMatchSnapshot();
  });
});
