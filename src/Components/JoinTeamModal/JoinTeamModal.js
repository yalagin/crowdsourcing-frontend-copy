import React, { useState } from 'react';
import { Image, Modal } from 'react-bootstrap';
import './JoinTeamModal.scss';
import { X, PersonPlusFill } from 'react-bootstrap-icons';
import { v4 as uuidv4 } from 'uuid';
import { useDispatch } from 'react-redux';
import signInAlby from '../../Assets/Images/signin-modal-alby.png';
import Button from '../Button/Button';
import {
  join as organizationUnconfirmedMembersActionJoin
} from '../../Generated/actions/organizationunconfirmedmembers/create';

const JoinTeamModal = (props) => {
  const dispatch = useDispatch();
  const { isShowModal, setIsShowModal, organization } = props;
  const [isRequest, setIsRequest] = useState(true);
  const [isDone, setIsDone] = useState(false);
  const [isCompleteProfile, setIsCompleteProfile] = useState(false);

  const handleSendRequest = () => {
    if (localStorage.getItem('id')) {
      dispatch(organizationUnconfirmedMembersActionJoin(
        {
          id: uuidv4(),
          organization: organization['@id'],
          member: localStorage.getItem('id')
        }
      ));
    }
    setIsRequest(false);
    setIsDone(true);
  };

  const handleDoneRequest = () => {
    setIsDone(false);
    setIsCompleteProfile(true);
  };

  const handleCloseModal = () => {
    setIsShowModal(false);
  };

  return (
    <div className='join-team-modal'>
      <Modal
        show={isShowModal}
        dialogClassName='join-team-modal-box'
        backdrop='static'
      >
        <Modal.Body>
          <div className='join-team-modal-box-close'>
            <X onClick={handleCloseModal} />
          </div>
          <div className='join-team-illustration'>
            <Image src={signInAlby} />
          </div>
          <div className='join-team-modal-box-content'>
            {isRequest && (
              <>
                <h4>Are you sure want to join  team?</h4>
                <p>Create a great team with teammates and win this contest!</p>
                <Button block={true} onClick={handleSendRequest}>
                  <PersonPlusFill /> Send Request
                </Button>
              </>
            )}
            {isDone && (
              <>
                <h4>Yass!</h4>
                <h4>Your Request has been sent!</h4>
                <p>
                  Once your request has approved, you will be notified. Good
                  luck and have fun!
                </p>
                <Button onClick={handleDoneRequest} block={true}>
                  Close
                </Button>
              </>
            )}
            {isCompleteProfile && (
              <>
                <h4>
                  Please complete your profile to help the team know you better
                </h4>
                <p>
                  Opps! Sorry you have to complete your profile first before
                  start joining the team. Okay?
                </p>
                <Button onClick={handleCloseModal} block={true}>
                  Complete My Profile
                </Button>
              </>
            )}
          </div>
        </Modal.Body>
      </Modal>
    </div>
  );
};

export default JoinTeamModal;
