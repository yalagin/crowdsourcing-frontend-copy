/* eslint-disable consistent-return */
import React from 'react';
import { Modal } from 'react-bootstrap';
import PropTypes from 'prop-types';
import Img from '../../Assets/Images/IMG.png';
import Button from '../Button/Button';
import './StartProposalConfirmModal.scss';

const StartProposalConfirmModal = ({
  isShowModal,
  setIsShowModal,
  details,
}) => {
  const renderButtons = () => {
    if (!details.isSingleButton) {
      return (
        <>
          <div className='start-proposal-confirm-modal-btn-flex-btn' >
            <Button type='secondary' block onClick={() => setIsShowModal(false)} >{details.noText}</Button>
          </div>
          <div className='start-proposal-confirm-modal-btn-flex-btn' >
            <Button block onClick={() => details.yesClick()} >{details.yesText}</Button>
          </div>
        </>
      );
    }
    return (
      <Button block onClick={() => details.yesClick()} >{details.yesText}</Button>
    );
  };

  return (
    <Modal show={isShowModal}
      dialogClassName='start-proposal-confirm-modal'
      backdrop='static'
      centered
    >
      <i className='fa fa-times start-proposal-confirm-modal-close' onClick={() => setIsShowModal(false)} />
      <img src={Img} alt="" />
      <p className='start-proposal-confirm-modal-heading' >
        {(details).heading}
      </p>
      <p className='start-proposal-confirm-modal-sub'>
        {(details).sub}
      </p>
      <div className='start-proposal-confirm-modal-btn-flex' >
        {renderButtons()}
      </div>
    </Modal>
  );
};

StartProposalConfirmModal.propTypes = {
  isShowModal: PropTypes.bool,
  setIsShowModal: PropTypes.func,
  details: PropTypes.object,
};

StartProposalConfirmModal.defaultProps = {
  details: {},
};

export default StartProposalConfirmModal;
