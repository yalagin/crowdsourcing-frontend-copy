import React from 'react';
import { shallow } from 'enzyme';
import StartProposalConfirmModal from './StartProposalConfirmModal';

describe('<StartProposalConfirmModal />', () => {
  let props = {};
  beforeEach(() => {
    props = {
      isShowModal: false,
      setIsShowModal: false,
    };
  });

  it('render correctly', () => {
    const wrapper = shallow(<StartProposalConfirmModal {...props} />);
    expect(wrapper.exists()).toMatchSnapshot();
  });
});
