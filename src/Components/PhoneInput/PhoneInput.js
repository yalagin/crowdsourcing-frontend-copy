import React from 'react';
import { Form } from 'react-bootstrap';
import PropTypes from 'prop-types';
import './PhoneInput.scss';

const PhoneInput = ({ controlId, label, errorMessage }) => {
  return (
    <div className='form-input'>
      <Form.Group controlId={controlId}>
        <Form.Label className='form-input-label'>{label}</Form.Label>
        <div className='phone-input' >
          <div className='phone-input-country-code' >
            +62
          </div>
          <input type="text" className='phone-input-number' />
        </div>
        <Form.Control.Feedback type="invalid">
          {errorMessage}
        </Form.Control.Feedback>
      </Form.Group>
    </div>
  );
};

PhoneInput.propTypes = {
  controlId: PropTypes.string,
  label: PropTypes.string,
  errorMessage: PropTypes.any,
};

export default PhoneInput;
