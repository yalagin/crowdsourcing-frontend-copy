import React from 'react';
import { shallow } from 'enzyme';
import JoinProjectModal from './JoinProjectModal';

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useSelector: jest.fn((fn) => fn),
  useDispatch: jest.fn()
}));

describe('<JoinProjectModal />', () => {
  let props = {};
  beforeEach(() => {
    props = {
      isShowModal: false,
      setIsShowModal: false,
      renderTitle: () => {},
      renderViewAndDeadline: () => {}
    };
  });

  it('render correctly', () => {
    const wrapper = shallow(<JoinProjectModal {...props} />);
    expect(wrapper.exists()).toMatchSnapshot();
  });
});
