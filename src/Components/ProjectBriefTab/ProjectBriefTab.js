import PropTypes from 'prop-types';
import { Col, Image, Row } from 'react-bootstrap';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import iconBook from '../../Assets/Images/ico-book.png';
import { list as awardListAction, reset as resetAwardListAction } from '../../Generated/actions/award/list';
import GetThemeAndSubThemeNameFormObject from '../DataHelpers/GetThemeAndSubThemeNameFormObject';
import AwardForProjectBriefTab from '../Award/AwardForProjectBriefTab';
import EducationalLevel from '../EducationalLevel/EducationalLevel';

const ProjectBriefTab = ({ projectDetail }) => {
  const dispatch = useDispatch();

  const awardEvent = useSelector((state) => state.award.list.eventSource);
  const awardList = useSelector((state) => state.award.list.retrieved);
  const requestDetails = projectDetail || {};
  useEffect(() => {
    if (projectDetail) {
      dispatch(awardListAction(`/awards?order[rank]=asc&project=${projectDetail['@id']}`));
    }
    return () => {
      dispatch(resetAwardListAction(awardEvent));
    };
  }, [projectDetail]);

  const [awards, setAwards] = useState([]);

  useEffect(() => {
    if (awardList && awardList['hydra:totalItems']) {
      setAwards(awardList['hydra:member']);
    }
  }, [awardList]);

  return (
        <div>
            <Row>
                <Col xs={3}>
                    <div className='question-answer-overview-title'>
                        <Row>
                            <Col xs={12}>
                                <Image src={iconBook} fluid />
                            </Col>
                            <Col className="mt-2" xs={10}>
                                <h1>Ringkasan <br /> Pelajaran</h1>
                            </Col>
                        </Row>
                    </div>
                </Col>
                <Col xs={8}>
                    <div className='question-answer-overview-content'>
                        <div className='question-answer-overview-content-wrapper'>
                            <Row>
                                <Col xs={2}>
                                    <h5>Material</h5>
                                </Col>
                                <Col xs={10}>
                                    <p>
                                        <EducationalLevel
                                            type='with-text'
                                            educationalLevel={projectDetail && projectDetail.educationalLevel}
                                        /> &nbsp;
                                        {requestDetails.material }
                                    </p>
                                </Col>
                            </Row>
                        </div><div className='question-answer-overview-content-wrapper'>
                        <Row>
                            <Col xs={2}>
                                <h5>Tema</h5>
                            </Col>
                            <Col xs={10}>
                                <p>
                                    <GetThemeAndSubThemeNameFormObject projectDetail={projectDetail}/>
                                </p>
                            </Col>
                        </Row>
                    </div>
                        <div className='question-answer-overview-content-wrapper'>
                            <Row>
                                <Col xs={2}>
                                    <h5>Objektif</h5>
                                </Col>
                                <Col xs={10}>
                                    <p>{requestDetails.objective}</p>
                                </Col>
                            </Row>
                        </div>
                        <div className='question-answer-overview-content-wrapper'>
                            <Row>
                                <Col xs={2}>
                                    <h5>Aktifitas</h5>
                                </Col>
                                <Col xs={10}>
                                    <p>{requestDetails.activity}</p>
                                </Col>
                            </Row>
                        </div>
                        <div className={'question-answer-overview-content-wrapper'}>
                            <Row>
                                <Col xs={2}>
                                    <h5>Penilaian</h5>
                                </Col>
                                <Col xs={10}>
                                    <p>
                                        {requestDetails.assessment}
                                    </p>
                                </Col>
                            </Row>
                        </div>
                        {awards && awards.map((award) =>
                            <AwardForProjectBriefTab award={award} key={award.id} />)}
                    </div>
                </Col>
            </Row>
        </div>
  );
};

export default ProjectBriefTab;

ProjectBriefTab.propTypes = {
  projectDetail: PropTypes.object
};
