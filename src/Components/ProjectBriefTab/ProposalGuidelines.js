import { Col, Image, Row } from 'react-bootstrap';
import React, { useState } from 'react';
import CheckList from '../../Assets/Images/checklist.png';
import Animation from '../../Assets/Images/animation.png';
import Button from '../Button/Button';
import Bubble from '../../Assets/Images/bubble.png';

const ProposalGuidelines = () => {
  const [selectedGuideline, setSelectedGuideline] = useState('animation');
  return (
    <Row as='div' className='contestant-proposal-guidelines'>
      <Col xs={3}>
        <div className='question-answer-overview-title'>
          <Row>
            <Col xs={12}>
              <Image src={CheckList} fluid/>
            </Col>
            <Col className="mt-2" xs={10}>
              <h1>Pedoman <br/> Proposal</h1>
            </Col>
            <span className='contestant-proposal-gold-text contestant-proposal-guidelines-left-sub'>
                Learn How we Work
            </span>
          </Row>
        </div>
      </Col>

      <Col xs={9} as='div' className='contestant-proposal-guidelines-right'>
        <div className='question-answer-overview-content'>
          <div className=''>
            Each lesson submitted must consist of:
          </div>
          <div className='contestant-proposal-guidelines-cards'>
            <div
              className={`contestant-proposal-guidelines-cards-card 
              ${selectedGuideline === 'animation' ? 'active' : ''}`}
              onClick={() => setSelectedGuideline('animation')}
            >
              <span className='contestant-proposal-guidelines-cards-card-heading small'>1 - 2 minutes</span>
              <span className='contestant-proposal-guidelines-cards-card-heading'>Animation</span>
              <span className='contestant-proposal-guidelines-cards-card-text-example'>Click to view example</span>
              <img src={Animation} alt="" className='contestant-proposal-guidelines-cards-card-img'/>
              <div className='contestant-proposal-guidelines-cards-card-circle-small'/>
              <div className='contestant-proposal-guidelines-cards-card-circle-big'/>
              <div className='contestant-proposal-guidelines-cards-card-btn'>
                <Button type='secondary' block>Download Brief Template (for Teacher)</Button>
              </div>
            </div>

            <div
              className={`contestant-proposal-guidelines-cards-card 
              ${selectedGuideline === 'questionAnswers' ? 'active' : ''}`}
              onClick={() => setSelectedGuideline('questionAnswers')}
            >
              <span className='contestant-proposal-guidelines-cards-card-heading small'>15 (Fifteen)</span>
              <span className='contestant-proposal-guidelines-cards-card-heading'>Questions & Answers</span>
              <span className='contestant-proposal-guidelines-cards-card-text-example'>Click to view example</span>
              <img src={Bubble} alt="" className='contestant-proposal-guidelines-cards-card-img'/>
              <div className='contestant-proposal-guidelines-cards-card-circle-small'/>
              <div className='contestant-proposal-guidelines-cards-card-circle-big'/>
              <div className='contestant-proposal-guidelines-cards-card-btn'>
                <Button type='secondary' block>Download Visual Guideline (for Designer)</Button>
              </div>
            </div>
          </div>
        </div>
      </Col>
    </Row>
  );
};

export default ProposalGuidelines;
