import React from 'react';
import { shallow } from 'enzyme';
import ContestOptionModal from './ContestOptionModal';

describe('<ContestOptionModal />', () => {
  let props = {};
  beforeEach(() => {
    props = {
      show: false,
      handleClose: () => {},
      handleClickAddFriend: () => {},
      handleClickOpenToAnyone: () => {},
    };
  });

  it('render correctly', () => {
    const wrapper = shallow(<ContestOptionModal {...props} />);
    expect(wrapper.exists()).toMatchSnapshot();
  });
});
