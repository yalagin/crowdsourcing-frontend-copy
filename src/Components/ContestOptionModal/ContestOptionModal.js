import React from 'react';
import { Modal } from 'react-bootstrap';
import { ArrowRight } from 'react-bootstrap-icons';
import './ContestOptionModal.scss';

const ContestOptionModal = (props) => {
  const {
    show,
    handleClose,
    handleClickAddFriend,
    handleClickOpenToAnyone,
  } = props;

  return (
    <div className='contest-option'>
      <Modal
        show={show}
        onHide={handleClose}
        dialogClassName='contest-option-modal'
        backdrop='static'
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title>
            Win Contests with
            <br />
            the best team :)
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div
            className='contest-option-modal-content'
            onClick={handleClickAddFriend}
          >
            <div className='contest-option-modal-content-text'>
              <p className='contest-option-modal-content-text-title'>
                Invite other participants
              </p>
              <p className='contest-option-modal-content-text-caption'>
                Find the members you think are cool!
              </p>
            </div>
            <div className='contest-option-modal-content-arrow'>
              <ArrowRight />
            </div>
          </div>
          <div
            className='contest-option-modal-content'
            onClick={handleClickOpenToAnyone}
          >
            <div className='contest-option-modal-content-text'>
              <p className='contest-option-modal-content-text-title'>
                Open to anyone
              </p>
              <p className='contest-option-modal-content-text-caption'>
                let's try new people joining you
              </p>
            </div>
            <div className='contest-option-modal-content-arrow'>
              <ArrowRight />
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </div>
  );
};

export default ContestOptionModal;
