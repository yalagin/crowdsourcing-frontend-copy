import React, { useEffect } from 'react';
import { Redirect } from 'react-router-dom';

const Logout = () => {
  useEffect(() => {
    localStorage.clear();
    window.location.reload();
  }, []);

  return (
    <Redirect to='/'/>
  );
};

export default Logout;
