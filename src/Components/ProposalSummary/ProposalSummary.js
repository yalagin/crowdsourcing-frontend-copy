import React, { useState } from 'react';
import './ProposalSummary.scss';
import PropTypes from 'prop-types';
import {
  Row, Col,
  Modal, ProgressBar, Carousel
} from 'react-bootstrap';
import 'react-bootstrap-carousel/dist/react-bootstrap-carousel.css';
// import VideoPlayer from 'react-simple-video-player';
import VideoPlayer from 'react-simple-video-player';
import { useSelector } from 'react-redux';
import { X } from 'react-bootstrap-icons';
import { isEmpty } from 'lodash';
import axios from 'axios';
import { useHistory } from 'react-router-dom';
import Label from '../Label/Label';
import Button from '../Button/Button';
import { CREATIVE_WORKS_URL } from '../../Constants/Constants';
import ProposalQuestionPreview from '../ProposalQuestionPreview/ProposalQuestionPreview';

const ProposalSummary = (props) => {
  const {
    isShowSummary, setIsShowSummary,
    proposalFile, creativeWorkId
  } = props;
  const history = useHistory();
  const [slideIndex, setSlideIndex] = useState(0);
  const questions = useSelector((state) => state.questions);

  const handleCloseSummary = () => {
    setIsShowSummary(false);
    setSlideIndex(0);
  };

  const handleOnSelect = (indexValue) => {
    setSlideIndex(indexValue);
  };

  const handleQuestionErrors = () => {
    let errorCounter = 0;
    for (let i = 0; i < questions.length; ++i) {
      if (questions[i].description === ''
        || questions[i].connectedAnswers.length === 0
        || questions[i].answerKey === ''
        || questions[i].visualBrief === ''
        || questions[i].haveOneCorrectAnswer === false
        || questions[i].image === null) {
        errorCounter += 1;
      }
    }
    return errorCounter;
  };

  const calculateQuestionsPercent = () => {
    return questions.length * 3;
  };

  const calculatePercent = () => {
    let currentProgress = 0;
    if (proposalFile.zipFile.contentUrl !== '') {
      currentProgress += 35;
    }
    if (proposalFile.mp4File.contentUrl !== '') {
      currentProgress += 20;
    }
    if (!isEmpty(questions) && handleQuestionErrors() === 0) {
      currentProgress += calculateQuestionsPercent();
    }
    return currentProgress;
  };

  const handleSubmitProposal = async () => {
    try {
      if (handleQuestionErrors < 0 && calculatePercent >= 100) {
        await axios.put(`${CREATIVE_WORKS_URL}/${creativeWorkId}/submit`, {});
        history.push('/my-projects');
      }
    } catch (error) {
      throw new Error(error);
    }
  };

  const renderProposalSummary = () => {
    return (
      <div className='proposal-summary__wrapper'>
        <div className='proposal-summary__content'>
          <div className='proposal-summary__content task'>
            <div className='proposal-summary__content proposal__progress'>
              <h1>Proposal Progress</h1>
              <h1>{calculatePercent()}%</h1>
            </div>
            <div className='proposal-summary__content progress__bar'>
              <ProgressBar now={calculatePercent} />
            </div>
            <div className='proposal-summary__content task__wrapper'>
              <div className='proposal-summary__content task__item'>
                <div className='proposal-summary_content task-description'>
                  <h1>Voice Over Text (.docx)</h1>
                  <p>Uploaded</p>
                </div>
                <div className='proposal-summary_content task-label'>
                  <Label type='STATUS_COMPLETED' />
                </div>
              </div>
              <div className='proposal-summary__content task__item'>
                <div className='proposal-summary_content task-description'>
                  <h1>After Effect Folder</h1>
                  <p>{proposalFile.zipFile.contentUrl !== '' ? 'Uploaded' : 'Not Uploaded'}</p>
                </div>
                <div className='proposal-summary_content task-label'>
                  {proposalFile.zipFile.contentUrl !== ''
                    ? <Label type='STATUS_COMPLETED' />
                    : <Label type='STATUS_NEED_PARTNER_TEXT'>Need File</Label>}
                </div>
              </div>
              <div className='proposal-summary__content task__item'>
                <div className='proposal-summary_content task-description'>
                  <h1>Animation Movie</h1>
                  <p>{proposalFile.mp4File.contentUrl !== '' ? 'Uploaded' : 'Not Uploaded'}</p>
                </div>
                <div className='proposal-summary_content task-label'>
                  {proposalFile.mp4File.contentUrl !== ''
                    ? <Label type='STATUS_COMPLETED' />
                    : <Label type='STATUS_NEED_PARTNER_TEXT'>Need File</Label>}
                </div>
              </div>
              <div className='proposal-summary__content task__item'>
                <div className='proposal-summary_content task-description'>
                  <h1>Question & Answers</h1>
                  <p>{questions.length} of 15 Done
                  {handleQuestionErrors() > 0 && `• ${handleQuestionErrors()} Errors found`}</p>
                </div>
                <div className='proposal-summary_content task-label'>
                  {handleQuestionErrors() > 0 && <Label type='STATUS_OVERDUE'>Error</Label>}
                  {(handleQuestionErrors() === 0 && questions.length === 15) && <Label type='STATUS_COMPLETED' />}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };

  const renderVoiceOverText = () => {
    return (
      <div className='proposal-summary__wrapper'>
        <div className='proposal-summary__content'>
          <div className='proposal-summary__content file__name'>
            Voice-over-text.docx
          </div>
          <div className='proposal-summary__contant file-vot__preview'>
            <div className='strip--1' />
            <div className='strip--2' />
            <div className='strip--3' />
          </div>
        </div>
      </div>
    );
  };

  const renderAnimation = () => {
    return (
      <div className='proposal-summary__wrapper'>
        <div className='proposal-summary__content'>
          <div className='proposal-summary__content file__name'>
            {proposalFile.mp4File.name}
          </div>
          <div className='proposal-summary_content file__vide'>
            <Row>
              <Col xs={12}>
                <VideoPlayer
                  url={proposalFile.mp4File.contentUrl}
                />
              </Col>
            </Row>
          </div>
        </div>
      </div>
    );
  };

  const renderAfterEffect = () => {
    return (
      <div className='proposal-summary__wrapper'>
        <div className='proposal-summary__content'>
          <div className='proposal-summary__content file__name'>
            {proposalFile.zipFile.name}
          </div>
        </div>
      </div>
    );
  };

  return (
    <div className='proposal-summary'>
      <Modal
        show={isShowSummary}
        dialogClassName='proposal-summary__modal'
        onHide={handleCloseSummary}
      >
        <div
          className='close'
          onClick={handleCloseSummary}
        >
          <X />
        </div>
        <Modal.Body>
          <div className='proposal-summary__title'>
            <h1>Proposal Summary</h1>
          </div>
          <Carousel
            interval={null}
            onSelect={handleOnSelect}
            indicators={false}
            defaultActiveIndex={slideIndex}
          >
            <Carousel.Item>
              {renderProposalSummary()}
            </Carousel.Item>
            <Carousel.Item>
              {renderVoiceOverText()}
            </Carousel.Item>
            <Carousel.Item>
              {renderAnimation()}
            </Carousel.Item>
            <Carousel.Item>
              {renderAfterEffect()}
            </Carousel.Item>
            {questions.map((question, index) => (
              <Carousel.Item>
                <ProposalQuestionPreview
                  questionData={question}
                  previewIndex={index}
                  questionsCount={questions.length}
                />
              </Carousel.Item>
            ))}
          </Carousel>
          <div className='proposal-summary__indicator'>
            <span>{slideIndex + 1} of {4 + questions.length}</span>
          </div>
          <div className='proposal-summary__action'>
            <Row>
              <Col>
                <Button block={true} type='secondary'>Check again</Button>
              </Col>
              <Col>
                <Button
                  block={true}
                  type='main'
                  onClick={handleSubmitProposal}
                >
                  Submit Proposal
                </Button>
              </Col>
            </Row>
          </div>
        </Modal.Body>
      </Modal>
    </div>
  );
};

ProposalSummary.propTypes = {
  isShowSummary: PropTypes.bool.isRequired,
  setIsShowSummary: PropTypes.func.isRequired
};

export default ProposalSummary;
