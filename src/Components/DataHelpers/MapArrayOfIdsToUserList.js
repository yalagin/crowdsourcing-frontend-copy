import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

const MapArrayOfIdsToUserList = ({ arrayOfUserIds }) => {
  const userList = useSelector((state) => state.user.list);
  const [listOfUsers, setListOfUsers] = useState(false);

  useEffect(() => {
    if (userList.retrieved && arrayOfUserIds && arrayOfUserIds instanceof Array) {
      setListOfUsers(
        userList.retrieved['hydra:member'].filter(
          (user) => {
            return arrayOfUserIds.includes(user['@id']);
          }
        )
      );
    }
  }, [userList.retrieved, arrayOfUserIds]);

  return listOfUsers;
};
export default MapArrayOfIdsToUserList;
