import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { X } from 'react-bootstrap-icons';
import {
  Carousel, Col, Image,
  Modal, Row
} from 'react-bootstrap';
import ReactPlayer from 'react-player';
import { cloneDeep } from 'lodash';
import Button from '../Button/Button';
import './FreeMaterialModal.scss';
import iconBook from '../../Assets/Images/ico-book.png';
import Label from '../Label/Label';
import { generateRandomId } from '../../Helpers/Common';
import { multipleChoiceField, trueOrFalseField } from '../../Helpers/StaticField';
import SmallQuestionPreview from '../SmallQuestionPreview/SmallQuestionPreview';

const FreeMaterialModal = (props) => {
  const { isShowMaterial, setIsShowMaterial, onDownloadClick } = props;
  const [slideIndex, setSlideIndex] = useState(0);
  const trueOrFalse = cloneDeep(trueOrFalseField);
  const multipleChoice = cloneDeep(multipleChoiceField);
  const listOfQuestion = [
    {
      questionId: generateRandomId(),
      type: 'true-or-false',
      question: 'Hello World',
      answer: '',
      answerKey: 'Testing',
      options: [],
      trueOrFalseOptions: trueOrFalse,
      multipleOptions: multipleChoice,
      image: null,
    },
    {
      questionId: generateRandomId(),
      type: 'true-or-false',
      question: 'Test 2',
      answer: '',
      answerKey: 'Testing',
      options: [],
      trueOrFalseOptions: trueOrFalse,
      multipleOptions: multipleChoice,
      image: null,
    }
  ];

  const handleCloseMaterial = () => {
    setIsShowMaterial(false);
    setSlideIndex(0);
  };

  const handleOnSelect = (indexValue) => {
    setSlideIndex(indexValue);
  };

  const renderLessonVideo = () => {
    return (
      <Row>
        <Col xs={12}>
          <h5 className='free-material__small-title'>Learning Video</h5>
        </Col>
        <Col xs={12}>
          <ReactPlayer url='https://www.youtube.com/watch?v=ysz5S6PUM-U' />
        </Col>
      </Row>
    );
  };

  const renderLessonDescription = () => {
    return (
      <Row>
        <Col xs={12}>
          <div className='free-material__lesson-title'>
            <Row>
              <Col xs={2}>
                <Image src={iconBook} width='64' />
              </Col>
              <Col xs={10}>
                <h1>Lesson Summary</h1>
                <Label type='with-text'>4 SD</Label>
                <Label type='with-text'>Ilmu Pengetahuan Alam</Label>
              </Col>
            </Row>
          </div>
        </Col>
        <Col xs={12}>
          <div className='free-material__description'>
            <div className='wrapper'>
              <Row>
                <Col xs={2}>
                  <h5>Tema</h5>
                </Col>
                <Col xs={10}>
                  <p>Aritmatika dengan Bilangan Utuh | Pembulatan dan Perkiraan</p>
                </Col>
              </Row>
            </div>
            <div className='wrapper'>
              <Row>
                <Col xs={2}>
                  <h5>Objektif</h5>
                </Col>
                <Col xs={10}>
                  <p>Demonstrate rounding and approximation from addition, subtraction,
                    multiplication and division of two whole numbers and/or fractions</p>
                </Col>
              </Row>
            </div>
            <div className='wrapper no-border-bottom'>
              <Row>
                <Col xs={2}>
                  <h5>Aktifitas</h5>
                </Col>
                <Col xs={10}>
                  <p>Learning about rounding whole numbers and solve mathematical questions</p>
                </Col>
              </Row>
            </div>
          </div>
        </Col>
      </Row>
    );
  };

  return (
    <div className='free-material'>
      <Modal
        show={isShowMaterial}
        dialogClassName='free-material__modal'
        onHide={handleCloseMaterial}
      >
        <div
          className='close'
          onClick={handleCloseMaterial}
        >
          <X />
        </div>
        <Modal.Body>
          <div className='free-material__title'>
            <h1>The title of the lessons here</h1>
          </div>
          <Carousel
            interval={null}
            onSelect={handleOnSelect}
            indicators={false}
            defaultActiveIndex={slideIndex}
          >
            <Carousel.Item>
              {renderLessonVideo()}
              {renderLessonDescription()}
            </Carousel.Item>
            {listOfQuestion.map((question, index) => (
              <Carousel.Item>
                <SmallQuestionPreview
                  questionData={question}
                  questionIndex={index}
                  questionLength={listOfQuestion.length}
                />
                {renderLessonDescription()}
              </Carousel.Item>
            ))}
          </Carousel>
          <div className='free-material__action'>
            <Row>
              <Col>
                <Button block={true} type='secondary'>Get Paid to Make Content</Button>
              </Col>
              <Col>
                <Button block={true} type='main' onClick={() => onDownloadClick()} >Download Apps Full Version</Button>
              </Col>
            </Row>
          </div>
        </Modal.Body>
      </Modal>
    </div>
  );
};

FreeMaterialModal.propTypes = {
  onDownloadClick: PropTypes.func,
  isShowMaterial: PropTypes.bool,
  setIsShowMaterial: PropTypes.func,
};

export default FreeMaterialModal;
