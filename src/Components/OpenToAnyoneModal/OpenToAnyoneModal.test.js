import React from 'react';
import { shallow } from 'enzyme';
import OpenToAnyoneModal from './OpenToAnyoneModal';

describe('<OpenToAnyoneModal />', () => {
  let props = {};
  beforeEach(() => {
    props = {
      show: false,
      handleClose: () => {},
      handleNextScreen: () => {},
      isNextScreen: false,
    };
  });

  it('render correctly', () => {
    const wrapper = shallow(<OpenToAnyoneModal {...props} />);
    expect(wrapper.exists()).toMatchSnapshot();
  });
});
