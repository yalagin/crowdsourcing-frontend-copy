const initialState = {};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'IS_ERROR_COMPLETE_PROFILE':
      return {
        ...state,
        isErrorCompleteProfile: action.payload
      };
    default:
      return state;
  }
};
