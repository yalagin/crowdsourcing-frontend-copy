import axios from 'axios';
import { renderHook } from '@testing-library/react-hooks';
import { useFetch } from './UseFetch';
import UseFetch from '../../Utils/UseFetch/UseFetch';

const { doReFetch } = UseFetch;

jest.mock('axios', () => ({
  get: jest.fn()
}));

jest.mock('../../Utils/UseFetch/UseFetch', () => ({
  doReFetch: jest.fn()
}));

describe('UseFetch', () => {
  beforeEach(() => {
    localStorage.setItem('token', 'mockToken');
  });

  afterEach(() => {
    localStorage.clear();
  });

  it('should call axios get with url passed and token and return mock data when invoked', async () => {
    const url = 'test';
    const mockData = 'response';
    const header = {
      headers: { Authorization: `Bearer ${localStorage.getItem('token')}` }
    };
    axios.get.mockImplementation(() =>
      Promise.resolve({
        status: 200,
        data: mockData
      }));

    const { result: actualResult, waitForNextUpdate } = renderHook(() => useFetch(url));
    await waitForNextUpdate();

    expect(axios.get).toHaveBeenCalledWith(url, header);
    expect(actualResult.current.toString()).toEqual({
      fetchedData: {
        data: mockData,
        status: 200
      },
      loading: false,
      callReFetch: () => doReFetch()
    }.toString());
  });

  it('should call axios get with url passed and token with params data when params is passed', async () => {
    const url = 'test';
    const mockData = 'response';
    const mockCommunities = { communities: ['ABC', 'DEF', '123'] };
    const header = {
      headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
      params: mockCommunities
    };
    axios.get.mockImplementation(() =>
      Promise.resolve({
        status: 200,
        data: mockData
      }));

    const { result: actualResult, waitForNextUpdate } = renderHook(() => useFetch(url, mockCommunities));
    await waitForNextUpdate();

    expect(axios.get).toHaveBeenCalledWith(url, header);
    expect(actualResult.current.toString()).toEqual({
      fetchedData: {
        data: mockData,
        status: 200
      },
      loading: false,
      callReFetch: () => doReFetch()
    }.toString());
  });

  it('should set loading to false when request returns an error', async () => {
    const mockCommunities = { communities: ['ABC', 'DEF', '123'] };
    const url = 'localhost/';
    axios.get.mockImplementation(() => Promise.reject(Error));

    const { result: actualResult, waitForNextUpdate } = renderHook(() => useFetch(url, mockCommunities));
    await waitForNextUpdate();

    expect(actualResult.current.toString()).toEqual({
      fetchedData: [],
      loading: false,
      callReFetch: () => doReFetch()
    }.toString());
  });

  it('should call doReFetch when callReFetch is invoked', async () => {
    const url = 'test';
    const mockData = 'response';
    axios.get.mockImplementation(() =>
      Promise.resolve({
        status: 200,
        data: mockData
      }));

    const { result: actualResult, waitForNextUpdate } = renderHook(() => useFetch(url));
    await waitForNextUpdate();

    actualResult.current.callReFetch();
    expect(doReFetch).toHaveBeenCalled();
  });
});
