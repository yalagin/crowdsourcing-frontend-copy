import { isErrorState, isSuccessRegisterState } from './AuthSelector';

describe('AuthSelector', () => {
  it('should return state isError when isErrorState is invoked', () => {
    const mockErrorState = {
      auth: {
        isError: true
      }
    };

    const isError = isErrorState(mockErrorState);

    expect(isError).toBe(true);
  });

  it('should return state isSuccessRegister when isSuccessRegister is invoked', () => {
    const mockSuccessState = {
      auth: {
        isSuccessRegister: true
      }
    };

    const isSuccessRegister = isSuccessRegisterState(mockSuccessState);

    expect(isSuccessRegister).toBe(true);
  });
});
