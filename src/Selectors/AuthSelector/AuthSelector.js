export const isErrorState = (state) => state.auth.isError;

export const isSuccessRegisterState = (state) => state.auth.isSuccessRegister;
