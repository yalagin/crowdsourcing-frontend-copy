import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { retrieve, reset } from '../../actions/lesson/show';
import { del } from '../../actions/lesson/delete';

class Show extends Component {
  static propTypes = {
    retrieved: PropTypes.object,
    loading: PropTypes.bool.isRequired,
    error: PropTypes.string,
    eventSource: PropTypes.instanceOf(EventSource),
    retrieve: PropTypes.func.isRequired,
    reset: PropTypes.func.isRequired,
    deleteError: PropTypes.string,
    deleteLoading: PropTypes.bool.isRequired,
    deleted: PropTypes.object,
    del: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.props.retrieve(decodeURIComponent(this.props.match.params.id));
  }

  componentWillUnmount() {
    this.props.reset(this.props.eventSource);
  }

  del = () => {
    if (window.confirm('Are you sure you want to delete this item?'))
      this.props.del(this.props.retrieved);
  };

  render() {
    if (this.props.deleted) return <Redirect to=".." />;

    const item = this.props.retrieved;

    return (
      <div>
        <h1>Show {item && item['@id']}</h1>

        {this.props.loading && (
          <div className="alert alert-info" role="status">
            Loading...
          </div>
        )}
        {this.props.error && (
          <div className="alert alert-danger" role="alert">
            <span className="fa fa-exclamation-triangle" aria-hidden="true" />{' '}
            {this.props.error}
          </div>
        )}
        {this.props.deleteError && (
          <div className="alert alert-danger" role="alert">
            <span className="fa fa-exclamation-triangle" aria-hidden="true" />{' '}
            {this.props.deleteError}
          </div>
        )}

        {item && (
          <table className="table table-responsive table-striped table-hover">
            <thead>
              <tr>
                <th>Field</th>
                <th>Value</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row">id</th>
                <td>{item['id']}</td>
              </tr>
              <tr>
                <th scope="row">creator</th>
                <td>{this.renderLinks('organizations', item['creator'])}</td>
              </tr>
              <tr>
                <th scope="row">createdFor</th>
                <td>{this.renderLinks('projects', item['createdFor'])}</td>
              </tr>
              <tr>
                <th scope="row">hasParts</th>
                <td>{this.renderLinks('lesson_questions', item['hasParts'])}</td>
              </tr>
              <tr>
                <th scope="row">originalCreativeWork</th>
                <td>{this.renderLinks('creative_works', item['originalCreativeWork'])}</td>
              </tr>
              <tr>
                <th scope="row">document</th>
                <td>{this.renderLinks('image_objects', item['document'])}</td>
              </tr>
              <tr>
                <th scope="row">archive</th>
                <td>{this.renderLinks('archive_objects', item['archive'])}</td>
              </tr>
              <tr>
                <th scope="row">video</th>
                <td>{this.renderLinks('video_objects', item['video'])}</td>
              </tr>
              <tr>
                <th scope="row">transcript</th>
                <td>{item['transcript']}</td>
              </tr>
              <tr>
                <th scope="row">published</th>
                <td>{item['published']}</td>
              </tr>
            </tbody>
          </table>
        )}
        <Link to=".." className="btn btn-primary">
          Back to list
        </Link>
        {item && (
          <Link to={`/lessons/edit/${encodeURIComponent(item['@id'])}`}>
            <button className="btn btn-warning">Edit</button>
          </Link>
        )}
        <button onClick={this.del} className="btn btn-danger">
          Delete
        </button>
      </div>
    );
  }

  renderLinks = (type, items) => {
    if (Array.isArray(items)) {
      return items.map((item, i) => (
        <div key={i}>{this.renderLinks(type, item)}</div>
      ));
    }

    return (
      <Link to={`../../${type}/show/${encodeURIComponent(items)}`}>
        {items}
      </Link>
    );
  };
}

const mapStateToProps = state => ({
  retrieved: state.lesson.show.retrieved,
  error: state.lesson.show.error,
  loading: state.lesson.show.loading,
  eventSource: state.lesson.show.eventSource,
  deleteError: state.lesson.del.error,
  deleteLoading: state.lesson.del.loading,
  deleted: state.lesson.del.deleted
});

const mapDispatchToProps = dispatch => ({
  retrieve: id => dispatch(retrieve(id)),
  del: item => dispatch(del(item)),
  reset: eventSource => dispatch(reset(eventSource))
});

export default connect(mapStateToProps, mapDispatchToProps)(Show);
