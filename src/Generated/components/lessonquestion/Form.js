import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import PropTypes from 'prop-types';

class Form extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    error: PropTypes.string
  };

  renderField = data => {
    data.input.className = 'form-control';

    const isInvalid = data.meta.touched && !!data.meta.error;
    if (isInvalid) {
      data.input.className += ' is-invalid';
      data.input['aria-invalid'] = true;
    }

    if (this.props.error && data.meta.touched && !data.meta.error) {
      data.input.className += ' is-valid';
    }

    return (
      <div className={`form-group`}>
        <label
          htmlFor={`lessonquestion_${data.input.name}`}
          className="form-control-label"
        >
          {data.input.name}
        </label>
        <input
          {...data.input}
          type={data.type}
          step={data.step}
          required={data.required}
          placeholder={data.placeholder}
          id={`lessonquestion_${data.input.name}`}
        />
        {isInvalid && <div className="invalid-feedback">{data.meta.error}</div>}
      </div>
    );
  };

  render() {
    return (
      <form onSubmit={this.props.handleSubmit}>
        <Field
          component={this.renderField}
          name="id"
          type="text"
          placeholder=""
          required={true}
        />
        <Field
          component={this.renderField}
          name="isPartOf"
          type="text"
          placeholder=""
        />
        <Field
          component={this.renderField}
          name="description"
          type="text"
          placeholder="Translatable a description of the item"
          required={true}
        />
        <Field
          component={this.renderField}
          name="visualBrief"
          type="text"
          placeholder="Translatable string|null a visual brief for the designer to create the question image"
        />
        <Field
          component={this.renderField}
          name="answerKey"
          type="text"
          placeholder="Translatable the answer key that is shown as a hint"
        />
        <Field
          component={this.renderField}
          name="type"
          type="text"
          placeholder="a type of item"
          required={true}
        />
        <Field
          component={this.renderField}
          name="image"
          type="text"
          placeholder="An image of the item. This can be a \[\[URL\]\] or a fully described \[\[ImageObject\]\]."
        />
        <Field
          component={this.renderField}
          name="ordering"
          type="number"
          placeholder="this is for convenient sorting"
          normalize={v => parseFloat(v)}
        />
        <Field
          component={this.renderField}
          name="originalQuestion"
          type="text"
          placeholder="original question"
        />

        <button type="submit" className="btn btn-success">
          Submit
        </button>
      </form>
    );
  }
}

export default reduxForm({
  form: 'lessonquestion',
  enableReinitialize: true,
  keepDirtyOnReinitialize: true
})(Form);
