import React from 'react';
import { Route } from 'react-router-dom';
import { List, Create, Update, Show } from '../components/videoobject/';

export default [
  <Route path="/video_objects/create" component={Create} exact key="create" />,
  <Route path="/video_objects/edit/:id" component={Update} exact key="update" />,
  <Route path="/video_objects/show/:id" component={Show} exact key="show" />,
  <Route path="/video_objects/" component={List} exact strict key="list" />,
  <Route path="/video_objects/:page" component={List} exact strict key="page" />
];
