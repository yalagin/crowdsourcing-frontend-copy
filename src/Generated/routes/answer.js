import React from 'react';
import { Route } from 'react-router-dom';
import { List, Create, Update, Show } from '../components/answer/';

export default [
  <Route path="/answers/create" component={Create} exact key="create" />,
  <Route path="/answers/edit/:id" component={Update} exact key="update" />,
  <Route path="/answers/show/:id" component={Show} exact key="show" />,
  <Route path="/answers/" component={List} exact strict key="list" />,
  <Route path="/answers/:page" component={List} exact strict key="page" />
];
