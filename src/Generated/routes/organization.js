import React from 'react';
import { Route } from 'react-router-dom';
import { List, Create, Update, Show } from '../components/organization/';

export default [
  <Route path="/organizations/create" component={Create} exact key="create" />,
  <Route path="/organizations/edit/:id" component={Update} exact key="update" />,
  <Route path="/organizations/show/:id" component={Show} exact key="show" />,
  <Route path="/organizations/" component={List} exact strict key="list" />,
  <Route path="/organizations/:page" component={List} exact strict key="page" />
];
