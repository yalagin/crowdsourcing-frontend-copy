import React from 'react';
import { Route } from 'react-router-dom';
import { List, Create, Update, Show } from '../components/addressregion/';

export default [
  <Route path="/address_regions/create" component={Create} exact key="create" />,
  <Route path="/address_regions/edit/:id" component={Update} exact key="update" />,
  <Route path="/address_regions/show/:id" component={Show} exact key="show" />,
  <Route path="/address_regions/" component={List} exact strict key="list" />,
  <Route path="/address_regions/:page" component={List} exact strict key="page" />
];
