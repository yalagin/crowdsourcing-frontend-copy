import React from 'react';
import { Route } from 'react-router-dom';
import { List, Create, Update, Show } from '../components/archiveobject/';

export default [
  <Route path="/archive_objects/create" component={Create} exact key="create" />,
  <Route path="/archive_objects/edit/:id" component={Update} exact key="update" />,
  <Route path="/archive_objects/show/:id" component={Show} exact key="show" />,
  <Route path="/archive_objects/" component={List} exact strict key="list" />,
  <Route path="/archive_objects/:page" component={List} exact strict key="page" />
];
