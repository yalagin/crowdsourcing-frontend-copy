import React from 'react';
import { Route } from 'react-router-dom';
import { List, Create, Update, Show } from '../components/subtheme/';

export default [
  <Route path="/subthemes/create" component={Create} exact key="create" />,
  <Route path="/subthemes/edit/:id" component={Update} exact key="update" />,
  <Route path="/subthemes/show/:id" component={Show} exact key="show" />,
  <Route path="/subthemes/" component={List} exact strict key="list" />,
  <Route path="/subthemes/:page" component={List} exact strict key="page" />
];
