import React from 'react';
import { Route } from 'react-router-dom';
import { List, Create, Update, Show } from '../components/mediaobject/';

export default [
  <Route path="/media_objects/create" component={Create} exact key="create" />,
  <Route path="/media_objects/edit/:id" component={Update} exact key="update" />,
  <Route path="/media_objects/show/:id" component={Show} exact key="show" />,
  <Route path="/media_objects/" component={List} exact strict key="list" />,
  <Route path="/media_objects/:page" component={List} exact strict key="page" />
];
