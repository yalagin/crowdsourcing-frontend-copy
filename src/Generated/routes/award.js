import React from 'react';
import { Route } from 'react-router-dom';
import { List, Create, Update, Show } from '../components/award/';

export default [
  <Route path="/awards/create" component={Create} exact key="create" />,
  <Route path="/awards/edit/:id" component={Update} exact key="update" />,
  <Route path="/awards/show/:id" component={Show} exact key="show" />,
  <Route path="/awards/" component={List} exact strict key="list" />,
  <Route path="/awards/:page" component={List} exact strict key="page" />
];
