import React from 'react';
import { Route } from 'react-router-dom';
import { List, Create, Update, Show } from '../components/lessonquestion/';

export default [
  <Route path="/lesson_questions/create" component={Create} exact key="create" />,
  <Route path="/lesson_questions/edit/:id" component={Update} exact key="update" />,
  <Route path="/lesson_questions/show/:id" component={Show} exact key="show" />,
  <Route path="/lesson_questions/" component={List} exact strict key="list" />,
  <Route path="/lesson_questions/:page" component={List} exact strict key="page" />
];
