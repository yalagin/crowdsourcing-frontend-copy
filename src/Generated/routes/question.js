import React from 'react';
import { Route } from 'react-router-dom';
import { List, Create, Update, Show } from '../components/question/';

export default [
  <Route path="/questions/create" component={Create} exact key="create" />,
  <Route path="/questions/edit/:id" component={Update} exact key="update" />,
  <Route path="/questions/show/:id" component={Show} exact key="show" />,
  <Route path="/questions/" component={List} exact strict key="list" />,
  <Route path="/questions/:page" component={List} exact strict key="page" />
];
