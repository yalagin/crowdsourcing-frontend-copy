import React from 'react';
import { Route } from 'react-router-dom';
import { List, Create, Update, Show } from '../components/userconfirmation/';

export default [
  <Route path="/user_confirmations/create" component={Create} exact key="create" />,
  <Route path="/user_confirmations/edit/:id" component={Update} exact key="update" />,
  <Route path="/user_confirmations/show/:id" component={Show} exact key="show" />,
  <Route path="/user_confirmations/" component={List} exact strict key="list" />,
  <Route path="/user_confirmations/:page" component={List} exact strict key="page" />
];
