import React from 'react';
import { Route } from 'react-router-dom';
import { List, Create, Update, Show } from '../components/theme/';

export default [
  <Route path="/themes/create" component={Create} exact key="create" />,
  <Route path="/themes/edit/:id" component={Update} exact key="update" />,
  <Route path="/themes/show/:id" component={Show} exact key="show" />,
  <Route path="/themes/" component={List} exact strict key="list" />,
  <Route path="/themes/:page" component={List} exact strict key="page" />
];
