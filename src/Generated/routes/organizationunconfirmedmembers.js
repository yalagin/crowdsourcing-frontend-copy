import React from 'react';
import { Route } from 'react-router-dom';
import { List, Create, Update, Show } from '../components/organizationunconfirmedmembers/';

export default [
  <Route path="/organization_unconfirmed_members/create" component={Create} exact key="create" />,
  <Route path="/organization_unconfirmed_members/edit/:id" component={Update} exact key="update" />,
  <Route path="/organization_unconfirmed_members/show/:id" component={Show} exact key="show" />,
  <Route path="/organization_unconfirmed_members/" component={List} exact strict key="list" />,
  <Route path="/organization_unconfirmed_members/:page" component={List} exact strict key="page" />
];
