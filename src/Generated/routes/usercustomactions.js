import React from 'react';
import { Route } from 'react-router-dom';
import { List, Create, Update, Show } from '../components/usercustomactions/';

export default [
  <Route path="/user_custom_actions/create" component={Create} exact key="create" />,
  <Route path="/user_custom_actions/edit/:id" component={Update} exact key="update" />,
  <Route path="/user_custom_actions/show/:id" component={Show} exact key="show" />,
  <Route path="/user_custom_actions/" component={List} exact strict key="list" />,
  <Route path="/user_custom_actions/:page" component={List} exact strict key="page" />
];
