import React from 'react';
import { Route } from 'react-router-dom';
import { List, Create, Update, Show } from '../components/lesson/';

export default [
  <Route path="/lessons/create" component={Create} exact key="create" />,
  <Route path="/lessons/edit/:id" component={Update} exact key="update" />,
  <Route path="/lessons/show/:id" component={Show} exact key="show" />,
  <Route path="/lessons/" component={List} exact strict key="list" />,
  <Route path="/lessons/:page" component={List} exact strict key="page" />
];
