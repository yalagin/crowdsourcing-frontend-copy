import React from 'react';
import { Route } from 'react-router-dom';
import { List, Create, Update, Show } from '../components/documentobject/';

export default [
  <Route path="/document_objects/create" component={Create} exact key="create" />,
  <Route path="/document_objects/edit/:id" component={Update} exact key="update" />,
  <Route path="/document_objects/show/:id" component={Show} exact key="show" />,
  <Route path="/document_objects/" component={List} exact strict key="list" />,
  <Route path="/document_objects/:page" component={List} exact strict key="page" />
];
