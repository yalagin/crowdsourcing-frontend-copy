import React from 'react';
import { Route } from 'react-router-dom';
import { List, Create, Update, Show } from '../components/imageobject/';

export default [
  <Route path="/image_objects/create" component={Create} exact key="create" />,
  <Route path="/image_objects/edit/:id" component={Update} exact key="update" />,
  <Route path="/image_objects/show/:id" component={Show} exact key="show" />,
  <Route path="/image_objects/" component={List} exact strict key="list" />,
  <Route path="/image_objects/:page" component={List} exact strict key="page" />
];
