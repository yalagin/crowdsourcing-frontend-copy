import React from 'react';
import { Route } from 'react-router-dom';
import { List, Create, Update, Show } from '../components/creativework/';

export default [
  <Route path="/creative_works/create" component={Create} exact key="create" />,
  <Route path="/creative_works/edit/:id" component={Update} exact key="update" />,
  <Route path="/creative_works/show/:id" component={Show} exact key="show" />,
  <Route path="/creative_works/" component={List} exact strict key="list" />,
  <Route path="/creative_works/:page" component={List} exact strict key="page" />
];
