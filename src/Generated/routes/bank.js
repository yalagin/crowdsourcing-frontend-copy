import React from 'react';
import { Route } from 'react-router-dom';
import { List, Create, Update, Show } from '../components/bank/';

export default [
  <Route path="/banks/create" component={Create} exact key="create" />,
  <Route path="/banks/edit/:id" component={Update} exact key="update" />,
  <Route path="/banks/show/:id" component={Show} exact key="show" />,
  <Route path="/banks/" component={List} exact strict key="list" />,
  <Route path="/banks/:page" component={List} exact strict key="page" />
];
