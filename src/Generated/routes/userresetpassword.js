import React from 'react';
import { Route } from 'react-router-dom';
import { List, Create, Update, Show } from '../components/userresetpassword/';

export default [
  <Route path="/user_reset_passwords/create" component={Create} exact key="create" />,
  <Route path="/user_reset_passwords/edit/:id" component={Update} exact key="update" />,
  <Route path="/user_reset_passwords/show/:id" component={Show} exact key="show" />,
  <Route path="/user_reset_passwords/" component={List} exact strict key="list" />,
  <Route path="/user_reset_passwords/:page" component={List} exact strict key="page" />
];
