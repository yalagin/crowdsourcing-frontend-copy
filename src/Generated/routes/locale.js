import React from 'react';
import { Route } from 'react-router-dom';
import { List, Create, Update, Show } from '../components/locale/';

export default [
  <Route path="/locales/create" component={Create} exact key="create" />,
  <Route path="/locales/edit/:id" component={Update} exact key="update" />,
  <Route path="/locales/show/:id" component={Show} exact key="show" />,
  <Route path="/locales/" component={List} exact strict key="list" />,
  <Route path="/locales/:page" component={List} exact strict key="page" />
];
