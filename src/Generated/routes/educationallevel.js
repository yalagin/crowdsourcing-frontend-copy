import React from 'react';
import { Route } from 'react-router-dom';
import { List, Create, Update, Show } from '../components/educationallevel/';

export default [
  <Route path="/educational_levels/create" component={Create} exact key="create" />,
  <Route path="/educational_levels/edit/:id" component={Update} exact key="update" />,
  <Route path="/educational_levels/show/:id" component={Show} exact key="show" />,
  <Route path="/educational_levels/" component={List} exact strict key="list" />,
  <Route path="/educational_levels/:page" component={List} exact strict key="page" />
];
