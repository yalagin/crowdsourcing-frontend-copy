import { combineReducers } from 'redux';
import list from './list';
import create from './create';
import update from './update';
import del from './delete';
import show from './show';
import searchList from "./searchList";

export default combineReducers({ list, searchList,  create, update, del, show });
