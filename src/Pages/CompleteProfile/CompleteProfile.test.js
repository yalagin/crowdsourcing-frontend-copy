import React from 'react';
import { shallow } from 'enzyme';
import CompleteProfile from './CompleteProfile';
import { convertToSnapshot } from '../../Utils/TestUtils/TestUtils';

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useSelector: jest.fn((fn) => fn),
  useDispatch: jest.fn()
}));

describe('CompleteProfile', () => {
  const renderComponent = () => {
    return shallow(
      <CompleteProfile />
    );
  };

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('#render', () => {
    it('should render component CompleteProfile correctly when invoked', () => {
      const renderedComponent = renderComponent();

      const actualComponent = convertToSnapshot(renderedComponent);

      expect(actualComponent).toMatchSnapshot();
    });
  });
});
