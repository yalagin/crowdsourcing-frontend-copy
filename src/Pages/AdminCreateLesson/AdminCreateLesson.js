import React, { useEffect, useState } from 'react';
import { ArrowLeft, CalendarEventFill } from 'react-bootstrap-icons';
import { Form } from 'react-bootstrap';
import CharacterCounter from 'react-character-counter';
import Calendar from 'react-calendar';
import { useHistory } from 'react-router-dom';
import moment from 'moment';
import { useDispatch, useSelector } from 'react-redux';
import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';
import _ from 'lodash';
import Logo from '../../Assets/Images/logo.png';
import DropdownFormComponent from '../../Components/DropdownFormComponent/DropdownFormComponent';
import Button from '../../Components/Button/Button';
import StartProposalConfirmModal from '../../Components/StartProposalConfirmModal/StartProposalConfirmModal';
import PublishModal from '../../Components/PublishModal/PublishModal';
import './AdminCreateLesson.scss';
import { list as tagsActionList, reset as tagsActionReset } from '../../Generated/actions/tags/list';
import {
  list as educationalLevelActionList,
  reset as educationalLevelActionReset
} from '../../Generated/actions/educationallevel/list';
import { PROJECT_URL } from '../../Constants/Constants';
import { reset as projectActionReset, retrieve as projectActionDetail } from '../../Generated/actions/project/show';
import { reset as subthemeActionListReset, list as subthemeActionList } from '../../Generated/actions/subtheme/list';
import RenderAwardsForm from './RenderAwardsForm';

const AdminCreateLesson = (props) => {
  // States
  const History = useHistory();
  const dispatch = useDispatch();

  // initial setter for project
  const [lessonDetails, setLessonDetails] = useState({
    title: '',
    subjects: '',
    grade: '',
    deadline: null,
    subtheme: '',
    objective: '',
    activity: '',
    assessment: '',
    material: '',
  });

  const listOfTags = useSelector((state) => state.tags.list);
  const listOfEducationalLevel = useSelector((state) => state.educationallevel.list);
  const subthemeList = useSelector((state) => state.subtheme.list);
  const projectDetail = useSelector((state) => state.project.show);

  // getting the project
  const [updatePage, setUpdatePage] = useState(true);
  useEffect(() => {
    if (updatePage) {
      dispatch(projectActionDetail(`/projects/${props.match.params.page}`));
      setUpdatePage(false);
    }
    return () => {
      dispatch(projectActionReset(projectDetail.eventSource));
    };
  }, [dispatch, projectDetail.eventSource, updatePage]);

  // setting data form project
  useEffect(() => {
    if (projectDetail.retrieved) {
      const projectDetailData = projectDetail.retrieved && projectDetail.retrieved;
      setLessonDetails({
        title: projectDetailData.title || '',
        subjects: projectDetailData.tags[0] || '',
        grade: projectDetailData.educationallevel || '',
        deadline: projectDetailData.endTime || '',
        subtheme: projectDetailData.subtheme || '',
        objective: projectDetailData.objective || '',
        activity: projectDetailData.activity || '',
        assessment: projectDetailData.assessment || '',
        material: projectDetailData.material || '',
      });
    }
  }, [projectDetail.retrieved]);

  // handle update todo should be in another component also we should not jump back and forth between redux and axios
  const handleUpdateField = async (updatedData) => {
    try {
      await axios.put(`${PROJECT_URL}/${props.match.params.page}`, updatedData);
      setUpdatePage(true);
    } catch (error) {
      throw new Error(error);
    }
  };
  // handle update

  // subtheme part
  useEffect(() => {
    dispatch(subthemeActionList());
    return () => {
      dispatch(subthemeActionListReset(subthemeList.eventSource));
    };
  }, [dispatch, subthemeList.eventSource]);
  const [subthemeOptions, setSubthemeOptions] = useState({});
  const [selectedSubtheme, setSelectedSubtheme] = useState(null);
  useEffect(() => {
    if (subthemeList.retrieved) {
      setSubthemeOptions(subthemeList.retrieved['hydra:member']
        .map((subtheme) => {
          const label = subtheme.name;
          // todo show theme also
          // if(subtheme.theme){
          //   label +=" | " + subtheme.theme.name;
          // }
          return ({
            label,
            value: subtheme['@id']
          });
        }));
    }
  }, [subthemeList.retrieved]);
  useEffect(() => {
    if (projectDetail.retrieved && !_.isEmpty(subthemeOptions)) {
      setSelectedSubtheme(subthemeOptions
        .filter((subtheme) => {
          return subtheme.value === (projectDetail.retrieved
                && projectDetail.retrieved.subtheme);
        }));
    }
  }, [projectDetail.retrieved, subthemeOptions]);
  const handleChangeSubtheme = async (subtheme) => {
    const object = { ...lessonDetails, subtheme: subtheme.value };
    await setLessonDetails(object);
    await handleUpdateField({ subtheme: subtheme.value });
  };
  // end subtheme part

  // grade part
  useEffect(() => {
    dispatch(
      educationalLevelActionList(
        '/educational_levels?order%5Bordering%5D=asc&pagination=false'
      )
    );
    return () => {
      dispatch(educationalLevelActionReset(listOfEducationalLevel.eventSource));
    };
  }, [dispatch, listOfEducationalLevel.eventSource]);
  const [gradeOptions, setGradeOptions] = useState({});
  const [selectedGrade, setSelectedGrade] = useState(null);
  useEffect(() => {
    if (listOfEducationalLevel.retrieved) {
      setGradeOptions(listOfEducationalLevel.retrieved['hydra:member']
        .map((educationalLevel) => ({
          label: educationalLevel.level,
          value: educationalLevel['@id']
        })));
    }
  }, [listOfEducationalLevel.retrieved]);
  useEffect(() => {
    if (projectDetail.retrieved && !_.isEmpty(gradeOptions)) {
      setSelectedGrade(gradeOptions
        .filter((grade) => {
          return grade.value === (projectDetail.retrieved
                && projectDetail.retrieved.educationalLevel);
        }));
    }
  }, [projectDetail.retrieved, gradeOptions]);
  const handleChangeGrade = async (grade) => {
    const object = { ...lessonDetails, grade: grade.value };
    await setLessonDetails(object);
    await handleUpdateField({ educationalLevel: grade.value });
  };
  // end grade part

  // subject part
  useEffect(() => {
    dispatch(tagsActionList('/tags?role=ROLE_TEACHER&pagination=false'));
    return () => {
      dispatch(tagsActionReset(listOfTags.eventSource));
    };
  }, [dispatch, listOfTags.eventSource]);
  const [subjectOptions, setSubjectOptions] = useState({});
  const [selectedSubject, setSelectedSubject] = useState(null);
  useEffect(() => {
    if (listOfTags.retrieved) {
      setSubjectOptions(listOfTags.retrieved['hydra:member']
        .map((tag) => ({
          label: tag.name,
          value: tag['@id']
        })));
    }
  }, [listOfTags.retrieved]);
  useEffect(() => {
    if (projectDetail.retrieved && !_.isEmpty(subjectOptions)) {
      setSelectedSubject(subjectOptions
        .filter((grade) => {
          return grade.value === (projectDetail.retrieved
                && projectDetail.retrieved.tags[0]);
        }));
    }
  }, [projectDetail.retrieved, subjectOptions]);
  const handleChangeSubject = async (subject) => {
    const object = { ...lessonDetails, subjects: subject.value };
    await setLessonDetails(object);
    await handleUpdateField({ tags: [subject.value] });
  };
  // end subject part

  const [showCalendar, setShowCalendar] = useState(false);

  const [showPublishModal, setShowPublishModal] = useState(false);

  const [showPublishedModal, setShowPublishedModal] = useState(false);

  const [showDeleteModal, setShowDeleteModal] = useState(false);

  const handleCreateLesson = async () => {
    try {
      const projectData = {
        id: uuidv4(),
        minimumNumberOfQuestions: 15,
        isDraft: true
      };
      const { data } = await axios.post(PROJECT_URL, projectData);
      if (data) {
        History.push(`/admin/lesson-request/${data.id}/edit`);
      }
    } catch (error) {
      throw new Error(error);
    }
  };

  const publishedModalDetails = {
    show: showPublishedModal,
    setShow: setShowPublishedModal,
    heading: 'Yay! your request has been made. Hope it’s going well!',
    sub: 'You can check the progress on Ongoing Lesson Request or create another request!',
    isSingleButton: false,
    noText: 'Check Request',
    noClick: () => {
      History.push('/admin/lesson-requests');
      setShowPublishedModal();
    },
    yesText: 'Create Another',
    yesClick: async () => {
      await handleCreateLesson();
      setShowPublishedModal();
    },
  };

  const handleDeleteLesson = async (projectId) => {
    try {
      await axios.delete(`${PROJECT_URL}/${projectId}`);
      History.push('/admin/lesson-requests');
    } catch (error) {
      throw new Error(error);
    }
  };

  const deleteModalDetails = {
    show: showDeleteModal,
    setShow: setShowDeleteModal,
    heading: 'Are you sure you want to delete this request?',
    sub: 'You can’t undo your action after you delete it. You can always save it on draft and edit it later',
    isSingleButton: false,
    noText: "No, I'd like to edit",
    yesText: 'Yes, delete please',
    yesClick: async () => {
      await handleDeleteLesson(props.match.params.page);
      setShowDeleteModal();
    },
  };

  // Functions

  const onChange = async (name, value) => {
    const object = { ...lessonDetails, [name]: value };
    await setLessonDetails(object);
  };

  const toggleCalendar = () => {
    if (showCalendar) setShowCalendar();
    else setShowCalendar(true);
  };

  const handlePublish = async () => {
    setShowPublishModal();
    await handleUpdateField({ isDraft: false });
    setShowPublishedModal(true);
  };

  // Render Functions
  const renderHeader = () => {
    return (
      <div className='admin-create-lesson-header' >
        <span onClick={() => History.goBack()} className='admin-create-lesson-header-arrow' >
          <ArrowLeft />
        </span>
        <span className='admin-create-lesson-header-text' >Request Form</span>
        <img src={Logo} alt="" className='admin-create-lesson-header-img' />
      </div>
    );
  };

  const renderHeadlineForm = () => {
    return (
      <div className='admin-create-lesson-headline-form' >
        <span className='admin-create-lesson-form-heading' >Headline</span>

        <div className='admin-create-lesson-form-field' >

          <div>
            <span className='admin-create-lesson-form-field-label' >Lesson Title</span>
            <span className='admin-create-lesson-form-field-label-sub' >
              Try to make it clear about the lesson's request
            </span>
          </div>

          <div className='admin-create-lesson-form-field-textarea-container' >
            <CharacterCounter maxLength={150} value={lessonDetails.title}>
              <Form.Control
                as="textarea"
                className='admin-create-lesson-form-field-textarea'
                maxLength={150}
                rows={5}
                value={lessonDetails.title}
                placeholder='Explain how this background image should look, what elements it has'
                onChange={(e) => onChange('title', e.target.value)}
                onBlur={() => handleUpdateField({ title: lessonDetails.title })}
              />
            </CharacterCounter>
          </div>

        </div>

        <div className='admin-create-lesson-form-field' >
          <span className='admin-create-lesson-form-field-label' >Subjects</span>
          <div className='admin-create-lesson-form-field-dropdown-container' >
            <div className='admin-create-lesson-dropdown' >
              <DropdownFormComponent
                placeholder='Choose Subjects'
                options={subjectOptions}
                onChange={handleChangeSubject}
                value={selectedSubject}
              />
            </div>
            <div className='admin-create-lesson-dropdown' >
              <DropdownFormComponent
                placeholder='Choose Grade'
                options={gradeOptions}
                onChange={handleChangeGrade}
                value={selectedGrade}
              />
            </div>
            <div className='admin-create-lesson-dropdown' >
              <DropdownFormComponent
                  placeholder='Choose Subtheme'
                  options={subthemeOptions}
                  onChange={handleChangeSubtheme}
                  value={selectedSubtheme}
              />
            </div>
          </div>
        </div>

        <div className='admin-create-lesson-form-field' >
          <span className='admin-create-lesson-form-field-label' >Deadline</span>
          <div className='admin-create-lesson-date'>
            <span className={`admin-create-lesson-date-text ${lessonDetails.deadline ? 'active' : ''}`} >
              {lessonDetails.deadline ? moment(lessonDetails.deadline).format('dddd, DD MMMM YYYY') : 'Choose Date'}
            </span>
            <span
              onClick={() => toggleCalendar()}
              className='admin-create-lesson-date-icon-bg'
            >
              <CalendarEventFill />
            </span>

            {showCalendar && <div className='admin-lesson-requests-filter-expand period' >
              <Calendar
                className='admin-lesson-requests-calendar'
                next2Label={null}
                prev2Label={null}
                onChange={async (value) => {
                  onChange('deadline', value);
                  await handleUpdateField({ endTime: value });
                  setShowCalendar();
                }}
              />
            </div>}

          </div>
        </div>
      </div>
    );
  };

  const renderDetailBriefForm = () => {
    return (
      <div className='admin-create-lesson-detail-form' >
        <span className='admin-create-lesson-form-heading' >Detail Brief</span>

        <div className='admin-create-lesson-form-field' >

          <div>
            <span className='admin-create-lesson-form-field-label' >Material</span>
            <span className='admin-create-lesson-form-field-label-sub' >
             material
        </span>
          </div>

          <div className='admin-create-lesson-form-field-textarea-container' >
            <CharacterCounter maxLength={150} value={lessonDetails.material}>
              <Form.Control
                as="textarea"
                className='admin-create-lesson-form-field-textarea'
                maxLength={150}
                rows={5}
                value={lessonDetails.material}
                placeholder='Explain how this background image should look, what elements it has'
                onChange={(e) => onChange('material', e.target.value)}
                onBlur={() => handleUpdateField({ material: lessonDetails.material })}
              />
            </CharacterCounter>
          </div>

        </div>

        <div className='admin-create-lesson-form-field' >

          <div>
            <span className='admin-create-lesson-form-field-label' >Objective</span>
            <span className='admin-create-lesson-form-field-label-sub' >
              Explanation about what should be written on objective
        </span>
          </div>

          <div className='admin-create-lesson-form-field-textarea-container' >
            <CharacterCounter maxLength={150} value={lessonDetails.objective}>
              <Form.Control
                as="textarea"
                className='admin-create-lesson-form-field-textarea'
                maxLength={150}
                rows={5}
                value={lessonDetails.objective}
                placeholder='Explain how this background image should look, what elements it has'
                onChange={(e) => onChange('objective', e.target.value)}
                onBlur={() => handleUpdateField({ objective: lessonDetails.objective })}
              />
            </CharacterCounter>
          </div>

        </div>

        <div className='admin-create-lesson-form-field' >

          <div>
            <span className='admin-create-lesson-form-field-label' >Activity</span>
            <span className='admin-create-lesson-form-field-label-sub' >
              What does activity mean
        </span>
          </div>

          <div className='admin-create-lesson-form-field-textarea-container' >
            <CharacterCounter maxLength={150} value={lessonDetails.activity}>
              <Form.Control
                as="textarea"
                className='admin-create-lesson-form-field-textarea'
                maxLength={150}
                rows={5}
                value={lessonDetails.activity}
                placeholder='Explain how this background image should look, what elements it has'
                onChange={(e) => onChange('activity', e.target.value)}
                onBlur={() => handleUpdateField({ activity: lessonDetails.activity })}
              />
            </CharacterCounter>
          </div>

        </div>

        <div className='admin-create-lesson-form-field' >

          <div>
            <span className='admin-create-lesson-form-field-label' >Assesment</span>
            <span className='admin-create-lesson-form-field-label-sub' >
              Tell teacher about the goals of the lesson
        </span>
          </div>

          <div className='admin-create-lesson-form-field-textarea-container' >
            <CharacterCounter maxLength={150} value={lessonDetails.assessment}>
              <Form.Control
                as="textarea"
                className='admin-create-lesson-form-field-textarea'
                maxLength={150}
                rows={5}
                value={lessonDetails.assessment}
                placeholder='Explain how this background image should look, what elements it has'
                onChange={(e) => onChange('assessment', e.target.value)}
                onBlur={() => handleUpdateField({ assessment: lessonDetails.assessment })}
              />
            </CharacterCounter>
          </div>
        </div>
      </div>
    );
  };

  const renderButtons = () => {
    return (
      <div className='admin-create-lesson-btn' >
        <div className='admin-create-lesson-btn-publish' >
          <Button block onClick={() => setShowPublishModal(true)} >Publish Request</Button>
        </div>
        <div className='admin-create-lesson-btn-delete' >
          <Button
            type='secondary'
            block
            onClick={() => setShowDeleteModal(true)}
          >
            Delete
          </Button>
        </div>
      </div>
    );
  };

  return (
    <div className='admin-create-lesson' >
      {renderHeader()}
      {renderHeadlineForm()}
      {renderDetailBriefForm()}
      <RenderAwardsForm projectDetailData={projectDetail.retrieved}/>
      {renderButtons()}
      <PublishModal
        showModal={showPublishModal}
        setShowModal={setShowPublishModal}
        onPublishClick={handlePublish}
        publishModalData={projectDetail.retrieved}/>
      <StartProposalConfirmModal
        isShowModal={showPublishedModal}
        setIsShowModal={setShowPublishedModal}
        details={publishedModalDetails}
      />
      <StartProposalConfirmModal
        isShowModal={showDeleteModal}
        setIsShowModal={setShowDeleteModal}
        details={deleteModalDetails}
      />
    </div>
  );
};

export default AdminCreateLesson;
