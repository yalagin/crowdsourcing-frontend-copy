import React from 'react';
import {
  Container, Row, Col,
  Form, Button, Image, Alert
} from 'react-bootstrap';
import './SignUp.scss';
import { Link } from 'react-router-dom';
import { isEmpty } from 'lodash';
import { useFormik } from 'formik';
import { useDispatch } from 'react-redux';
import { v4 as uuidv4 } from 'uuid';
import signUpIllustration from '../../Assets/Images/signup-illustration.png';
import Input from '../../Components/Input/Input';
import { signUpSchema } from '../../Schemas/AuthenticationSchema';
import { signUp } from '../../Actions/Auth';
import { capitalizeFirst } from '../../Helpers/Common';

const SignUp = (props) => {
  const dispatch = useDispatch();
  const formik = useFormik({
    initialValues: {
      username: '',
      email: '',
      password: '',
      acceptTerms: false
    },
    validationSchema: signUpSchema,
  });

  const {
    setFieldValue, errors, values,
    validateForm, setStatus, status
  } = formik;

  const { history } = props;

  const handleSignUp = async (userData) => {
    const validate = await validateForm();
    if (isEmpty(validate)) {
      const registerUser = {
        id: uuidv4(),
        username: userData.username,
        email: userData.email,
        password: userData.password
      };
      dispatch(signUp(registerUser, history, setStatus));
    }
  };

  const handleErrorSignUp = () => {
    return status && (
      <div className='sign-up-form-input-error'>
        <Alert variant='danger'>
          {status.map((statusMessage) => (
            <ul key={statusMessage}>
              <li>
                {`${capitalizeFirst(statusMessage.propertyPath)}: ${statusMessage.message}`}
              </li>
            </ul>
          ))}
        </Alert>
      </div>
    );
  };

  const handleOnChangeName = (event) => {
    const { target: { value } } = event;
    setFieldValue('username', value);
  };

  const handleOnChangeEmail = (event) => {
    const { target: { value } } = event;
    setFieldValue('email', value);
  };

  const handleOnChangePassword = (event) => {
    const { target: { value } } = event;
    setFieldValue('password', value);
  };

  const handleOnChangeTerms = () => {
    setFieldValue('acceptTerms', !values.acceptTerms);
  };

  const renderSignUpForm = () => {
    return (
      <div className='sign-up-form-input'>
        {handleErrorSignUp()}
        <Form>
          <Input
            controlId='nameInput'
            label='Full Name'
            type='text'
            placeholder='Enter Your Full Name'
            isInvalid={!isEmpty(errors.username)}
            errorMessage={errors.username}
            onChange={handleOnChangeName}
          />
          <Input
            controlId='emailInput'
            label='Email Address'
            type='email'
            placeholder='Email Address'
            isInvalid={!isEmpty(errors.email)}
            errorMessage={errors.email}
            onChange={handleOnChangeEmail}
          />
          <Input
            controlId='passwordInput'
            label='Password'
            type='password'
            placeholder='Minimum 6 Characters'
            isInvalid={!isEmpty(errors.password)}
            errorMessage={errors.password}
            onChange={handleOnChangePassword}
          />
          <Form.Group controlId='terms-and-policy'>
            <Form.Check
              className='sign-up-form-input-label-checkbox'
              type='checkbox'
              onChange={handleOnChangeTerms}
              isInvalid={!isEmpty(errors.acceptTerms)}
              label={(
                <span>
                  I've read and accepted&nbsp;
                  <Link to='/' className='sign-up-form-input-terms-policy'>
                    Terms of Service and Privacy Policy
                  </Link>
                </span>
              )}
            />
            <Form.Control.Feedback type="invalid">
              {errors.acceptTerms}
            </Form.Control.Feedback>
          </Form.Group>
        </Form>
        <Row>
          <Col xs={12}>
            <Button
              className='sign-up-form-input-button'
              type='submit'
              onClick={() => handleSignUp(values)}
            >
              Sign Up
            </Button>
          </Col>
          <Col xs={12} align='center'>
            <div className='sign-up-form-input-link'>
              Already a member? &nbsp;
              <Link to='/login' className='sign-up-form-input-link-sign-in'>
                Sign In
              </Link>
            </div>
          </Col>
        </Row>
      </div>
    );
  };

  return (
    <div className='sign-up'>
      <Container fluid>
        <Row>
          <Col xs={6}>
            <div className='sign-up-left-section'>
              <div className='sign-up-round'>
                <div className='sign-up-greetings'>
                  <h1>
                    Ayo ikut serunya <br />
                    berkarya bersama <br />
                    di Papan Pintar
                  </h1>
                </div>
              </div>
              <div className='sign-up-illustration'>
                <Image src={signUpIllustration} width='475' />
              </div>
            </div>
          </Col>
          <Col xs={6}>
            <div className='sign-up-right-section'>
              <div className={`sign-up-form ${status && 'set-min-height'}`}>
                <Row>
                  <Col xs={12}>
                    <h1 className='sign-up-form-title'>Sign Up</h1>
                  </Col>
                </Row>
                <Row>
                  <Col xs={12}>
                    {renderSignUpForm()}
                  </Col>
                </Row>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default SignUp;
