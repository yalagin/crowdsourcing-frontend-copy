import React from 'react';
import { shallow } from 'enzyme';
import * as Formik from 'formik';
import { useDispatch } from 'react-redux';
import SignUp from './SignUp';
import { convertToSnapshot } from '../../Utils/TestUtils/TestUtils';

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useSelector: jest.fn((fn) => fn),
  useDispatch: jest.fn()
}));

describe('SignUp', () => {
  const mockSetFieldValue = jest.fn();
  const mockHandleSubmit = jest.fn();
  const mockSetStatus = jest.fn();
  const mockForm = (errorMessage = {}) => ({
    setFieldValue: mockSetFieldValue,
    handleSubmit: mockHandleSubmit,
    setStatus: mockSetStatus,
    status: [
      {
        propertyPath: 'username',
        message: 'error'
      }
    ],
    values: {
      acceptTerms: false
    },
    errors: {
      username: '',
      email: '',
      password: ''
    },
    validateForm: jest.fn(() => errorMessage)
  });
  const renderComponent = () => {
    return shallow(
      <SignUp />
    );
  };

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('#render', () => {
    it('should render component SignUp correctly when invoked', () => {
      const renderedComponent = renderComponent();

      const actualComponent = convertToSnapshot(renderedComponent);

      expect(actualComponent).toMatchSnapshot();
    });
  });

  describe('#handleOnChangeName', () => {
    it('should call setFieldValue when handleOnChangeName is invoked', () => {
      const mockNameValue = {
        target: {
          value: 'Koming Yosua'
        }
      };
      jest
        .spyOn(Formik, 'useFormik')
        .mockImplementation(() => mockForm());
      const renderedComponent = renderComponent();

      const { props: { onChange } } = renderedComponent.find('Input').get(0);
      onChange(mockNameValue);

      expect(mockSetFieldValue).toHaveBeenCalledWith('username', 'Koming Yosua');
    });
  });

  describe('#handleOnChangeEmail', () => {
    it('should call setFieldValue when handleOnChangeEmail is invoked', () => {
      const mockEmailValue = {
        target: {
          value: 'helloworld@gmail.com'
        }
      };
      jest
        .spyOn(Formik, 'useFormik')
        .mockImplementation(() => mockForm());
      const renderedComponent = renderComponent();

      const { props: { onChange } } = renderedComponent.find('Input').get(1);
      onChange(mockEmailValue);

      expect(mockSetFieldValue).toHaveBeenCalledWith('email', 'helloworld@gmail.com');
    });
  });

  describe('#handleOnChangePassword', () => {
    it('should call setFieldValue when handleOnChangePassword is invoked', () => {
      const mockPasswordValue = {
        target: {
          value: 'test123'
        }
      };
      jest
        .spyOn(Formik, 'useFormik')
        .mockImplementation(() => mockForm());
      const renderedComponent = renderComponent();

      const { props: { onChange } } = renderedComponent.find('Input').get(2);
      onChange(mockPasswordValue);

      expect(mockSetFieldValue).toHaveBeenCalledWith('password', 'test123');
    });
  });

  describe('#handleOnChangePassword', () => {
    it('should call setFieldValue when handleOnChangePassword is invoked', () => {
      const mockPasswordValue = {
        target: {
          value: 'test123'
        }
      };
      jest
        .spyOn(Formik, 'useFormik')
        .mockImplementation(() => mockForm());
      const renderedComponent = renderComponent();

      const { props: { onChange } } = renderedComponent.find('Input').get(2);
      onChange(mockPasswordValue);

      expect(mockSetFieldValue).toHaveBeenCalledWith('password', 'test123');
    });
  });

  describe('#handleOnChangeTerms', () => {
    it('should call setFieldValue when handleOnChangeTerms is invoked', () => {
      jest
        .spyOn(Formik, 'useFormik')
        .mockImplementation(() => mockForm());
      const renderedComponent = renderComponent();

      const { props: { onChange } } = renderedComponent.find('.sign-up-form-input-label-checkbox').get(0);
      onChange();

      expect(mockSetFieldValue).toHaveBeenCalledWith('acceptTerms', true);
    });
  });

  describe('#handleSignUp', () => {
    it('should call dispatch function when handleSignUp is invoked', async () => {
      const dispatch = jest.fn();
      jest
        .spyOn(Formik, 'useFormik')
        .mockImplementation(() => mockForm());
      useDispatch.mockReturnValue(dispatch);
      const renderedComponent = renderComponent();

      const submitButton = renderedComponent.find('Button').at(0);
      submitButton.simulate('click', {
        preventDefault: () => {}
      });

      expect(useDispatch).toHaveBeenCalled();
    });
  });
});
