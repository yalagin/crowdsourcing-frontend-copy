import React from 'react';
import { shallow } from 'enzyme';
import Profile from './Profile';
import { convertToSnapshot } from '../../Utils/TestUtils/TestUtils';

describe('Profile', () => {
  const setState = jest.fn();
  const useDispatch = jest.fn();
  const useStateSpy = jest.spyOn(React, 'useState');
  useStateSpy.mockImplementation((init) => [init, setState, useDispatch]);

  const renderComponent = () => {
    return shallow(
      <Profile />
    );
  };

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render component ContestantProposal correctly when invoked', () => {
    const renderedComponent = renderComponent();

    const actualComponent = convertToSnapshot(renderedComponent);

    expect(actualComponent).toMatchSnapshot();
  });
});
