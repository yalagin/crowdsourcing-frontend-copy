import React, { useState } from 'react';
import { Container, Row, /* Col */ } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import NavigationBar from '../../Components/NavigationBar/NavigationBar';
import Recommendation from '../../Components/Recommendation/Recommendation';
// import OrganizationCard from '../../Components/OrganizationCard/OrganizationCard';
import profilePicture from '../../Assets/Images/profile-picture.png';
import Button from '../../Components/Button/Button';
import './Profile.scss';
import stars from '../../Assets/Images/profile-stars.svg';
import trophy from '../../Assets/Images/profile-trophy.svg';

const recommendations = [
  {
    ratingValue: 3,
    name: 'Kevin Aldo',
    job: 'Teacher Grade 4',
    company: 'SD Negri 6 Jakarta',
    date: '22 July 2020',
    skills: ['Teamwork', 'Functional Skill'],
    testimonial:
      `John is driven, committed and a rising star in the Indonesian digital and social media marketing industry.
      It was an absolute pleasure working and I am very happy to learn that his business 
      has grown from stregth to strength since the tie-up.`,
  },
  {
    ratingValue: 4,
    name: 'Gunawan',
    job: 'Teacher Grade 4',
    company: 'SD Negri 6 Jakarta',
    date: '22 July 2020',
    skills: ['Teammate', 'Functional Skill'],
    testimonial:
      `John is driven, committed and a rising star in the Indonesian digital and social media marketing industry. 
      It was an absolute pleasure working and I am very happy to learn that his business 
      has grown from stregth to strength since the tie-up.`,
  },
];

const Profile = () => {
  const history = useHistory();
  const [isRenderReview, setIsRenderReview] = useState(true);
  const [isRenderContest, setIsRenderContest] = useState(false);
  const [isReceived, setIsReceived] = useState(true);

  const handleSwitchTab = () => {
    setIsRenderReview(!isRenderReview);
    setIsRenderContest(!isRenderContest);
  };

  const handleRecommendation = () => {
    setIsReceived(!isReceived);
  };

  const renderHeader = () => {
    return (
      <div className='profile-header'>
        <div className='profile-header-image'>
          <img src={profilePicture} alt='' />
        </div>
        <div className='profile-header-details'>
          <div className='profile-header-details-name'>
            <h1>Jennifer Wiryawan</h1>
            <Button type='secondary' onClick={() => history.push('/profile/edit')} >Edit Profile</Button>
          </div>
          <h5>Designer - Illustration & Animation at Digital Agency</h5>
          <div className='profile-header-social-media'>
            <Button>Linkedin</Button>
            <Button>Dribble</Button>
            <Button>Behance</Button>
          </div>
        </div>
      </div>
    );
  };

  const renderTab = () => {
    return (
      <div className='profile-tabs'>
        <div
          className='profile-tabs-tab'
          id={isRenderReview && 'active'}
          onClick={handleSwitchTab}
        >
          <div className='profile-tabs-tab-image'>
            <img src={stars} alt='' />
          </div>
          <div
            className='profile-tabs-tab-text'
            id={isRenderReview && 'text-active'}
          >
            <h1>4.5</h1>
            <p>Overall Review</p>
          </div>
        </div>
        <div
          className='profile-tabs-tab'
          id={isRenderContest && 'active'}
          onClick={handleSwitchTab}
        >
          <div className='profile-tabs-tab-image'>
            <img src={trophy} alt='' />
          </div>
          <div
            className='profile-tabs-tab-text'
            id={isRenderContest && 'text-active'}
          >
            <h1>25</h1>
            <p>Contest Won</p>
          </div>
        </div>
        <div className='profile-tabs-tab' />
      </div>
    );
  };

  const renderReview = () => {
    return (
      <div className='profile-reviews'>
        <div className='profile-reviews-button'>
          <Button
            type={!isReceived ? 'secondary' : 'primary'}
            onClick={handleRecommendation}
          >
            Received Recommendation
          </Button>
          <Button
            type={isReceived ? 'secondary' : 'primary'}
            onClick={handleRecommendation}
          >
            Given Recommendation
          </Button>
        </div>
        {recommendations.map((item) => (
          <Recommendation
            key={item.name}
            ratingValue={item.ratingValue}
            name={item.name}
            job={item.job}
            company={item.company}
            date={item.date}
            skills={item.skills}
            testimonial={item.testimonial}
          />
        ))}
      </div>
    );
  };

  const renderContest = () => {
    return (
      <div className='profile-contests'>
        <div className='profile-contests-earnings'>
          <p>Your total earnings since August 2019</p>
          <h1>Rp. 15.000.000</h1>
        </div>
        <div className='profile-contests-cards'>
          <Row>
            {/* <Col xs={6}> */}
            {/*  <br /> */}
            {/*  <OrganizationCard */}
            {/*    teamName='Christine & Team' */}
            {/*    lessonName='Science Lesson - Grade 4' */}
            {/*    isWinner={true} */}
            {/*    type='completed' */}
            {/*    time='12 Sept 2020' */}
            {/*    organizationId={''} */}
            {/*  /> */}
            {/* </Col> */}
            {/* <Col xs={6}> */}
            {/*  <br /> */}
            {/*  <OrganizationCard */}
            {/*    teamName='Christine & Team' */}
            {/*    lessonName='Science Lesson - Grade 4' */}
            {/*    isWinner={true} */}
            {/*    type='completed' */}
            {/*    time='12 Sept 2020' */}
            {/*    organizationId={''} */}
            {/*  /> */}
            {/* </Col> */}
            {/* <Col xs={6}> */}
            {/*  <br /> */}
            {/*  <OrganizationCard */}
            {/*    teamName='Christine & Team' */}
            {/*    lessonName='Science Lesson - Grade 4' */}
            {/*    isWinner={true} */}
            {/*    type='completed' */}
            {/*    time='12 Sept 2020' */}
            {/*    organizationId={''} */}
            {/*  /> */}
            {/* </Col> */}
            {/* <Col xs={6}> */}
            {/*  <br /> */}
            {/*  <OrganizationCard */}
            {/*    teamName='Christine & Team' */}
            {/*    lessonName='Science Lesson - Grade 4' */}
            {/*    isWinner={true} */}
            {/*    type='completed' */}
            {/*    time='12 Sept 2020' */}
            {/*    organizationId={''} */}
            {/*  /> */}
            {/* </Col> */}
            {/* <Col xs={6}> */}
            {/*  <br /> */}
            {/*  <OrganizationCard */}
            {/*    teamName='Christine & Team' */}
            {/*    lessonName='Science Lesson - Grade 4' */}
            {/*    isWinner={true} */}
            {/*    type='completed' */}
            {/*    time='12 Sept 2020' */}
            {/*    organizationId={''} */}
            {/*  /> */}
            {/* </Col> */}
            {/* <Col xs={6}> */}
            {/*  <br /> */}
            {/*  <OrganizationCard */}
            {/*    teamName='Christine & Team' */}
            {/*    lessonName='Science Lesson - Grade 4' */}
            {/*    isWinner={true} */}
            {/*    type='completed' */}
            {/*    time='12 Sept 2020' */}
            {/*    organizationId={''} */}
            {/*  /> */}
            {/* </Col> */}
          </Row>
        </div>
      </div>
    );
  };

  return (
    <>
      <NavigationBar />
      <Container>
        {renderHeader()}
        {renderTab()}
        {isRenderReview && renderReview()}
        {isRenderContest && renderContest()}
      </Container>
    </>
  );
};

export default Profile;
