import React from 'react';
import {
  Container,
  Row,
  Col,
  Accordion
} from 'react-bootstrap';
import NavigationBar from '../../Components/NavigationBar/NavigationBar';
import Label from '../../Components/Label/Label';
import FindChallenge from '../../Assets/Images/find-challenge.png';
import FindTeam from '../../Assets/Images/find-team.png';
import OnlineMeet from '../../Assets/Images/online-meet.png';
import Button from '../../Components/Button/Button';
import CheckedIcon from '../../Assets/Images/checked-icon.png';
import './HowItWorks.scss';

const HowItWorks = () => {
  const activeStage = 1;

  const renderStages = () => {
    return (
      <div className='how-it-works-stages' >
        <div className={`how-it-works-stages-stage ${activeStage >= 1 ? 'active' : ''}`} >
          <div className='how-it-works-stages-stage-num' >
            1
          <div className='how-it-works-stages-stage-desc' >Find a Challenge</div>
          </div>
          <div className={`how-it-works-stages-stage-tail ${activeStage > 1 ? 'active' : ''}`} />
        </div>
        <div className={`how-it-works-stages-stage ${activeStage >= 2 ? 'active' : ''}`} >
          <div className='how-it-works-stages-stage-num' >
            2
          <div className='how-it-works-stages-stage-desc' >Find a Team</div>
          </div>
          <div className={`how-it-works-stages-stage-tail ${activeStage > 2 ? 'active' : ''}`} />
        </div>
        <div className={`how-it-works-stages-stage ${activeStage >= 3 ? 'active' : ''}`} >
          <div className='how-it-works-stages-stage-num' >
            3
          <div className='how-it-works-stages-stage-desc' >Work Together</div>
          </div>
          <div className={`how-it-works-stages-stage-tail ${activeStage > 3 ? 'active' : ''}`} />

        </div>
        <div className={`how-it-works-stages-stage ${activeStage >= 4 ? 'active' : ''}`} >
          <div className='how-it-works-stages-stage-num' >
            4
          <div className='how-it-works-stages-stage-desc' >Finalize</div>
          </div>
          <div className={`how-it-works-stages-stage-tail ${activeStage > 4 ? 'active' : ''}`} />
        </div>
        <div className={`how-it-works-stages-stage-num last ${activeStage >= 5 ? 'active' : ''}`} >
          !
          <div className='how-it-works-stages-stage-desc' >
            <Button>Get Started</Button>
          </div>
        </div>
      </div>
    );
  };

  const renderHeader = () => {
    return (
      <div className='how-it-works-header' >
        <h1 className='how-it-works-header-heading' >How It Works</h1>
        <p className='how-it-works-header-sub-one' >
          Titik Pintar is creating a gamified curriculum for children in Indonesia.
        </p>
        <p className='how-it-works-header-sub' >
          Can your team create the best, smartest, funniest and helpful animations and questions?
        </p>
        {renderStages()}
      </div>
    );
  };

  const renderFindChallenge = () => {
    return (
      <Row as='div' className='how-it-works-find-challenge' >
        <Col xl={4} className='how-it-works-find-challenge-left' >
          <div>
            <Label type='STATUS_IN_PROGRESS' >FIND A CHALLENGE</Label>
            <h3 className='how-it-works-step-heading' >
              Browse the <span className='how-it-works-blue-text' >‘Request’</span> to find what lessons to be created
            </h3>
            <p className='how-it-works-step-text' >
              Click Lesson Request to find and select lessons that you would like to compete!
            </p>
          </div>
        </Col>
        <Col xl={7} >
          <img src={FindChallenge} alt="" />
        </Col>
      </Row>
    );
  };

  const renderFindTeam = () => {
    return (
      <Row as='div' className='how-it-works-find-team' >
        <Col xs={6} as='div' className='how-it-works-find-team-img-container' >
          <img src={FindTeam} alt="" />
          <div className='how-it-works-find-team-img-container-btn' >
            <Button block > <i className=' fa fa-user-plus' /> Kirim Permintaan Bergabung</Button>
          </div>
        </Col>
        <Col xs={6} className='how-it-works-find-team-right' >
          <div>
            <Label type='STATUS_IN_PROGRESS' >FIND A TEAM</Label>
            <h3 className='how-it-works-step-heading' >
              Join the team who is in need of partner or find your match by inviting them join you!
            </h3>
            <p className='how-it-works-step-text' >
              We believe that a team can run the world!
              Find your best teammate by collaborating or invite and check their
              review to find the best partner
          </p>
          </div>
        </Col>
      </Row>
    );
  };

  const renderWorkTogether = () => {
    return (
      <Row as='div' className='how-it-works-work-together' >
        <Col xs={6} >
          <Label type='STATUS_IN_PROGRESS' >WORK TOGETHER</Label>
          <h3 className='how-it-works-step-heading' >
            Collaborate on any platform you like to create the best possible lesson.
          </h3>
          <p className='how-it-works-step-text' >
            The teachers are responsible to create the concept for the scenes, texts
            and animation. Download and follow the templates to help you guys
            collaborate on how things should look - Back and forth until everyone
            is happy! ;)
        </p>
          <div>
            <Button type='secondary' block >Download Concept Brief Template (for Teacher)</Button>
          </div>
        </Col>
        <Col xs={6} >
          <img src={OnlineMeet} alt="" />
        </Col>
      </Row>
    );
  };

  const renderAccordion = () => {
    return (
      <Accordion as='div' eventKey="0" >
        <Accordion.Toggle as='div' eventKey="0" className='how-it-works-accordion-heading' >
          2 mins Animation Requirements
          <i className='fa fa-chevron-down how-it-works-accordion-heading-pointer' />
        </Accordion.Toggle>
        <Accordion.Collapse eventKey="0" as='div' className='how-it-works-accordion-collapse' >
          <div>
            <div className='how-it-works-accordion-point' >
              <img src={CheckedIcon} alt="" className='how-it-works-accordion-point-check' />
              <p className='how-it-works-accordion-point-text' >
                Follow {' '}
                <span className='how-it-works-gold-text' >
                  Titik Pintar’s visual style
                </span>
                {' '}closely as possible
              </p>
            </div>
            <div className='how-it-works-accordion-point' >
              <img src={CheckedIcon} alt="" className='how-it-works-accordion-point-check' />
              <p className='how-it-works-accordion-point-text' >
                Create
                <span className='how-it-works-bold' > 1-2 minutes </span>
                animation or 4 screens of animation video
              based on lesson’s request
              </p>
            </div>
            <div className='how-it-works-accordion-point' >
              <img src={CheckedIcon} alt="" className='how-it-works-accordion-point-check' />
              <p className='how-it-works-accordion-point-text' >

                Write the <span className='how-it-works-bold' > voice-over spoken </span> on our template and
              reupload the PDF file vers.
              </p>
            </div>
            <div className='how-it-works-accordion-point' >
              <img src={CheckedIcon} alt="" className='how-it-works-accordion-point-check' />
              <p className='how-it-works-accordion-point-text' >
                Upload the rendered mp4 file (1080p) & final After Effect files
                (reduce & collected, AEP and all related files)
              </p>
            </div>
          </div>
        </Accordion.Collapse>

        <Accordion.Toggle as='div' eventKey="1" className='how-it-works-accordion-heading' >
          15 Q&A’s Requirements
          <i className='fa fa-chevron-down how-it-works-accordion-heading-pointer' />
        </Accordion.Toggle>
        <Accordion.Collapse eventKey="1" as='div' className='how-it-works-accordion-collapse' >
          <div>
            <div className='how-it-works-accordion-point' >
              <img src={CheckedIcon} alt="" className='how-it-works-accordion-point-check' />
              <p className='how-it-works-accordion-point-text' >
                Follow {' '}
                <span className='how-it-works-gold-text' >
                  Titik Pintar’s visual style
                </span>
                {' '}closely as possible
              </p>
            </div>
            <div className='how-it-works-accordion-point' >
              <img src={CheckedIcon} alt="" className='how-it-works-accordion-point-check' />
              <p className='how-it-works-accordion-point-text' >
                Use our template or make sure for the
                <span className='how-it-works-bold' >safe space area</span>
              </p>
            </div>
            <div className='how-it-works-accordion-point' >
              <img src={CheckedIcon} alt="" className='how-it-works-accordion-point-check' />
              <p className='how-it-works-accordion-point-text' >
                Create 15 Q&A based on the lesson you’ve made
                (should be realted with the animation video)
            </p>
            </div>
            <div className='how-it-works-accordion-point' >
              <img src={CheckedIcon} alt="" className='how-it-works-accordion-point-check' />
              <p className='how-it-works-accordion-point-text' >
                Submit Ai’s file to help us review your work
              </p>
            </div>
          </div>
        </Accordion.Collapse>
      </Accordion>
    );
  };

  const renderRequirements = () => {
    return (
      <Row as='div' className='how-it-works-requirements' >
        <Col xs={6} >
          <img src={OnlineMeet} alt="" />
        </Col>
        <Col xs={6} >
          <Label type='STATUS_IN_PROGRESS' >ON PROGRESS</Label>
          <h3 className='how-it-works-step-heading' >
            Complete task and make sure your final file follow the requirements ;)
          </h3>
          <p className='how-it-works-step-text' >
            Once you’re happy with your design, don’t forget to make sure the details
            like sizing and file’s requirements.
          </p>
          {renderAccordion()}
          <div className='how-it-works-requirements-btn' >
            <Button block type='secondary' >Download All Visual Guide (for Designer)</Button>
          </div>
        </Col>
      </Row>
    );
  };

  const renderFooter = () => {
    return (
      <div className='how-it-works-footer' >
        <h2 className='how-it-works-footer-heading' >
          Join Now & <span className='how-it-works-blue-text' > Win IDR 2 million! </span>
        </h2>
        <p className='how-it-works-footer-text' >
          The contest will end and the number 1 lesson will be awarded the prize!
          Kids will love to learn and play with your excellent lesson! Thank you very much
        </p>
        <div className='how-it-works-footer-btn' >
          <Button block >Get Started</Button>
        </div>
      </div>
    );
  };

  return (
    <>
      <NavigationBar active='caraKerja' />
      {renderHeader()}
      <Container className='my-projects-container'>
        {renderFindChallenge()}
        {renderFindTeam()}
        {renderWorkTogether()}
        {renderRequirements()}
      </Container>
      {renderFooter()}
    </>

  );
};

export default HowItWorks;
