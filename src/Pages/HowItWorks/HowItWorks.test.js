import React from 'react';
import { shallow } from 'enzyme';
import HowItWorks from './HowItWorks';
import { convertToSnapshot } from '../../Utils/TestUtils/TestUtils';

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useSelector: jest.fn((fn) => fn),
  useDispatch: jest.fn()
}));

describe('<HowItWorks />', () => {
  const setState = jest.fn();
  const useStateSpy = jest.spyOn(React, 'useState');
  useStateSpy.mockImplementation((init) => [init, setState]);

  const renderComponent = () => {
    return shallow(
      <HowItWorks />
    );
  };

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render component HowItWorks correctly when invoked', () => {
    const renderedComponent = renderComponent();

    const actualComponent = convertToSnapshot(renderedComponent);

    expect(actualComponent).toMatchSnapshot();
  });
});
