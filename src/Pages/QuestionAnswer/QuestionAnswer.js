import React, { useEffect, useRef, useState } from 'react';
import {
  Col, Container, Image, Row
} from 'react-bootstrap';
import './QuestionAnswer.scss';
import {
  ArrowLeft, CardImage, PlusCircleFill, TrashFill
} from 'react-bootstrap-icons';
import { isEmpty } from 'lodash';
import { useDispatch, useSelector } from 'react-redux';
import { Scrollbars } from 'react-custom-scrollbars';
import axios from 'axios';
import { useHistory } from 'react-router-dom';
import CharacterCounter from 'react-character-counter';
import Button from '../../Components/Button/Button';
import QuestionForm from '../../Components/QuestionForm/QuestionForm';
import questionPreview from '../../Assets/Images/question-preview.png';
import cancelPreview from '../../Assets/Images/cancel-preview.png';
import { submitQuestion, uploadToS3 } from '../../Actions/Question';
import {
  answerField,
  dropdownCreateQuestion,
  fileUploadField,
  generateAnswerMultipleChoice,
  generateAnswerTrueOrFalse,
  generateQuestion,
  generateQuestionData,
  multipleAnswerField,
} from '../../Helpers/StaticField';
import Loading from '../../Components/Loading/Loading';
import correctIcon from '../../Assets/Images/correct-icon.png';
import iconMP4 from '../../Assets/Images/ico-mp4.png';
import iconFile from '../../Assets/Images/ico-file.png';
import iconZIP from '../../Assets/Images/ico-zip.png';
import ProposalSummary from '../../Components/ProposalSummary/ProposalSummary';
import {
  reset as creativeWorkActionRetrieveReset,
  retrieve as creativeWorkActionRetrieve
} from '../../Generated/actions/creativework/show';
import {
  reset as projectActionRetrieveReset,
  retrieve as projectActionRetrieve
} from '../../Generated/actions/project/show';
import {
  reset as organizationActionRetrieveReset,
  retrieve as organizationActionRetrieve
} from '../../Generated/actions/organization/show';
import {
  list as creativeWorkActionList,
  reset as creativeWorkActionListReset
} from '../../Generated/actions/creativework/list';
import {
  ANSWERS_URL,
  ARCHIVE_OBJECTS,
  CREATIVE_WORKS_URL,
  IMAGE_OBJECTS,
  QUESTIONS_URL,
  VIDEO_OBJECTS
} from '../../Constants/Constants';
import { list as userListAction, reset as userListActionReset } from '../../Generated/actions/user/list';
import { list as personActionList, reset as personActionReset } from '../../Generated/actions/person/list';
import ProposalOverview from '../../Components/ProposalOverview/ProposalOverview';

const QuestionAnswer = (props) => {
  const [previewContent, setPreviewContent] = useState(0);
  const [previewCorrectAnswer, setPreviewCorrectAnswer] = useState();
  const dispatch = useDispatch();
  const history = useHistory();
  const questions = useSelector((state) => state.questions);
  const creativeWorkDetail = useSelector((state) => state.creativework.show);
  const projectDetail = useSelector((state) => state.project.show);
  const organizationDetail = useSelector((state) => state.organization.show);
  const creativeWorkList = useSelector((state) => state.creativework.list);
  const personList = useSelector((state) => state.person.list);
  const userList = useSelector((state) => state.user.list);
  const [isLoading, setIsLoading] = useState(true);
  const [isShowSummary, setIsShowSummary] = useState(false);
  const [isShowModal, setIsShowModal] = useState(false);
  const [previewImage, setPreviewImage] = useState(null);
  const [mappedMembers, setMappedMembers] = useState(null);
  const [mappedImages, setMappedImages] = useState(null);
  const [proposalFile, setProposalFile] = useState(fileUploadField);
  const [voiceOverText, setVoiceOverText] = useState('');
  const mp4InputFile = useRef();
  const zipInputFile = useRef();

  useEffect(() => {
    dispatch(creativeWorkActionRetrieve(decodeURIComponent(`/creative_works/${props.match.params.page}`)));
    return () => {
      dispatch(creativeWorkActionRetrieveReset(creativeWorkDetail.eventSource));
    };
  }, []);

  useEffect(() => {
    const projectId = creativeWorkDetail.retrieved && creativeWorkDetail.retrieved.createdFor;
    dispatch(projectActionRetrieve(projectId));
    return () => {
      dispatch(projectActionRetrieveReset(projectDetail.eventSource));
    };
  }, [creativeWorkDetail.retrieved]);

  useEffect(() => {
    const organizationId = creativeWorkDetail.retrieved && creativeWorkDetail.retrieved.creator;
    dispatch(organizationActionRetrieve(organizationId));
    return () => {
      dispatch(organizationActionRetrieveReset(organizationDetail.eventSource));
    };
  }, [creativeWorkDetail.retrieved]);

  useEffect(() => {
    const organizationId = creativeWorkDetail.retrieved && creativeWorkDetail.retrieved.creator;
    dispatch(organizationActionRetrieve(organizationId));
    return () => {
      dispatch(organizationActionRetrieveReset(organizationDetail.eventSource));
    };
  }, [creativeWorkDetail.retrieved]);

  useEffect(() => {
    dispatch(userListAction());
    return () => {
      dispatch(userListActionReset(userList.eventSource));
    };
  }, []);

  useEffect(() => {
    if (organizationDetail.retrieved) {
      const membersArray = [];
      organizationDetail.retrieved.members.map((organization) => {
        membersArray.push(organization);
        return null;
      });
      const membersArrayString = membersArray.join('&user[]=');
      dispatch(personActionList(`/people?pagination=false&user[]=${membersArrayString}`));
    }
    return () => {
      dispatch(personActionReset(personList.eventSource));
    };
  }, [organizationDetail]);

  useEffect(() => {
    (async () => {
      if (personList.retrieved) {
        let imagesArray = [];
        const mappedMembersArr = [];
        personList.retrieved['hydra:member'].map((member) => {
          imagesArray = [...imagesArray, member.image];
          mappedMembersArr.push(member);
          return null;
        });
        setMappedMembers(mappedMembersArr);
        const mediaArrayString = imagesArray.filter(Boolean)
          .join('&id[]=');
        const { data: memberImages } = await axios.get(`${IMAGE_OBJECTS}?id[]=${mediaArrayString}`);
        setMappedImages(memberImages);
      }
    })();
  }, [personList.retrieved]);

  useEffect(() => {
    dispatch(creativeWorkActionList(`/creative_works/${props.match.params.page}/has_parts?order%5Bordering%5D=asc`));
    return () => {
      dispatch(creativeWorkActionListReset(creativeWorkList.eventSource));
    };
  }, []);

  useEffect(() => {
    if (creativeWorkList.retrieved) {
      const completeQuestionData = creativeWorkList.retrieved
        && creativeWorkList.retrieved['hydra:member']
          .map((filteredQuestion) => ({
            ...filteredQuestion,
            description: filteredQuestion.description || '',
            answerKey: filteredQuestion.answerKey || '',
            visualBrief: filteredQuestion.visualBrief || '',
            answers: [],
            answerPreview: multipleAnswerField,
            image: filteredQuestion.image || null
          }));
      dispatch(submitQuestion(completeQuestionData));
    }
  }, [creativeWorkList.retrieved]);

  useEffect(() => {
    (async () => {
      if (questions[previewContent] && questions[previewContent].image) {
        try {
          const getImageId = questions[previewContent].image.split('/');
          const { data } = await axios.get(`${IMAGE_OBJECTS}/${getImageId[2]}`);
          setPreviewImage(data.contentUrl);
        } catch (err) {
          throw new Error(err);
        }
      } else {
        setPreviewImage(null);
      }
    })();
  }, [previewContent, questions]);

  useEffect(() => {
    (async () => {
      if (creativeWorkDetail.retrieved && creativeWorkDetail.retrieved.archive) {
        try {
          const archiveURL = creativeWorkDetail.retrieved && creativeWorkDetail.retrieved.archive.split('/')[2];
          const { data: archiveData } = await axios.get(`${ARCHIVE_OBJECTS}/${archiveURL}`);
          setProposalFile((prevState) => ({
            ...prevState,
            zipFile: {
              name: (archiveData && archiveData.name) || '',
              contentUrl: (archiveData && archiveData.contentUrl) || ''
            }
          }));
        } catch (e) {
          throw new Error(e);
        }
      }
    })();
  }, [creativeWorkDetail.retrieved]);

  useEffect(() => {
    (async () => {
      if (creativeWorkDetail.retrieved && creativeWorkDetail.retrieved.video) {
        try {
          const videoURL = creativeWorkDetail.retrieved && creativeWorkDetail.retrieved.video.split('/')[2];
          const { data: mp4Data } = await axios.get(`${VIDEO_OBJECTS}/${videoURL}`);
          setProposalFile((prevState) => ({
            ...prevState,
            mp4File: {
              name: (mp4Data && mp4Data.name) || '',
              contentUrl: (mp4Data && mp4Data.contentUrl) || ''
            }
          }));
        } catch (e) {
          throw new Error(e);
        }
      }
    })();
  }, [creativeWorkDetail.retrieved]);

  useEffect(() => {
    (async () => {
      if (creativeWorkDetail.retrieved && creativeWorkDetail.retrieved.transcript) {
        setVoiceOverText(creativeWorkDetail.retrieved && creativeWorkDetail.retrieved.transcript);
      }
    })();
  }, [creativeWorkDetail.retrieved]);

  useEffect(() => {
    const previewSection = document.getElementById('fixed-wrapper');
    const stickyPreview = previewSection.offsetTop;
    const scrollCallBack = window.addEventListener('scroll', () => {
      if (window.pageYOffset >= (stickyPreview + 1750)) {
        previewSection.classList.add('fixed');
      }
      if (window.pageYOffset <= (stickyPreview + 1750)) {
        previewSection.classList.remove('fixed');
      }
    });
    setTimeout(() => {
      setIsLoading(false);
    }, 1500);
    return () => {
      window.removeEventListener('scroll', () => scrollCallBack);
    };
  }, []);

  const handleGenerateTrueOrFalseAnswer = async (creator, questionId) => {
    const generatedAnswer = generateAnswerTrueOrFalse(creator, questionId);
    try {
      const { data: firstAnswer } = await axios.post(ANSWERS_URL, generatedAnswer[0]);
      const { data: secondAnswer } = await axios.post(ANSWERS_URL, generatedAnswer[1]);
      const firstAnswerData = firstAnswer['@id'];
      const secondAnswerData = secondAnswer['@id'];
      return {
        firstAnswerData,
        secondAnswerData
      };
    } catch (error) {
      throw new Error(error);
    }
  };

  const handleGenerateMultipleChoiceAnswer = async (creator, questionId) => {
    const generatedAnswer = generateAnswerMultipleChoice(creator, questionId);
    try {
      const { data: firstAnswer } = await axios.post(`${ANSWERS_URL}`, generatedAnswer[0]);
      const { data: secondAnswer } = await axios.post(`${ANSWERS_URL}`, generatedAnswer[1]);
      const { data: thirdAnswer } = await axios.post(`${ANSWERS_URL}`, generatedAnswer[2]);
      const { data: fourthAnswer } = await axios.post(`${ANSWERS_URL}`, generatedAnswer[3]);
      const firstAnswerData = firstAnswer['@id'];
      const secondAnswerData = secondAnswer['@id'];
      const thirdAnswerData = thirdAnswer['@id'];
      const fourthAnswerData = fourthAnswer['@id'];
      return {
        firstAnswerData,
        secondAnswerData,
        thirdAnswerData,
        fourthAnswerData
      };
    } catch (error) {
      throw new Error(error);
    }
  };

  const generateTrueOrFalse = async (createdQuestion, creator, questionId) => {
    const {
      firstAnswerData, secondAnswerData
    } = await handleGenerateTrueOrFalseAnswer(creator, questionId);
    return {
      ...createdQuestion,
      connectedAnswers: [
        firstAnswerData,
        secondAnswerData
      ]
    };
  };

  const generateMultipleAnswer = async (createdQuestion, creator, questionId) => {
    const {
      firstAnswerData, secondAnswerData,
      thirdAnswerData, fourthAnswerData
    } = await handleGenerateMultipleChoiceAnswer(creator, questionId);
    return {
      ...createdQuestion,
      connectedAnswers: [
        firstAnswerData,
        secondAnswerData,
        thirdAnswerData,
        fourthAnswerData
      ]
    };
  };

  const generateAnswer = async (type, createdQuestion, generatedQuestionData) => {
    if (type === 'TYPE_TRUE_OR_FALSE') {
      return await generateTrueOrFalse(createdQuestion,
        generatedQuestionData.creator, generatedQuestionData.id);
    }
    if (type === 'TYPE_MULTIPLE_CHOICE') {
      return await generateMultipleAnswer(createdQuestion,
        generatedQuestionData.creator, generatedQuestionData.id);
    }
    if (type === 'TYPE_FILL_THE_BLANK') {
      return {
        ...createdQuestion
      };
    }
    return null;
  };

  const handleGenerateQuestion = async (type) => {
    let listOfQuestion = [...questions];
    const creatorId = creativeWorkDetail.retrieved && creativeWorkDetail.retrieved.creator;
    const isPartOfId = `/creative_works/${props.match.params.page}`;
    const createdQuestion = generateQuestion(creatorId, isPartOfId, type);
    const generatedQuestionData = generateQuestionData(createdQuestion, type, listOfQuestion.length + 1);
    try {
      await axios.post(QUESTIONS_URL, generatedQuestionData);
      const questionData = await generateAnswer(type, createdQuestion, generatedQuestionData);
      listOfQuestion = [...listOfQuestion, questionData];
      dispatch(submitQuestion(listOfQuestion));
    } catch (error) {
      throw new Error(error);
    }
  };

  const handleProposalSummary = () => {
    setIsShowSummary(true);
  };

  const renderHeaderProposal = () => {
    return (
      <header>
        <Container>
          <div className='question-answer-header'>
            <div className='question-answer-header-back' onClick={() => history.push('/my-projects')}>
              <ArrowLeft />
            </div>
            <div className='question-answer-header-title'>
              <h1>
                {projectDetail.retrieved && projectDetail.retrieved.title}
              </h1>
            </div>
            <div className='question-answer-header-submit'>
              <Button type='main' onClick={handleProposalSummary}>
                Kumpul Proposal
              </Button>
              <ProposalSummary
                isShowSummary={isShowSummary}
                setIsShowSummary={setIsShowSummary}
                answerField={answerField}
                proposalFile={proposalFile}
                creativeWorkId={props.match.params.page}
              />
            </div>
          </div>
        </Container>
      </header>
    );
  };

  const renderCorrectAnswerPreview = () => {
    return (questions.length !== 0 && previewCorrectAnswer !== undefined) && (
      <div
        className='question-answer-content-preview-mask'
        id='question-answer-content-preview-mask'
      >
        <div className='question-answer-content-preview-mask-text'>
          <h1>Yay</h1>
        </div>
        <div className='question-answer-content-preview-mask-icon'>
          <Image src={correctIcon} />
        </div>
        <div className='question-answer-content-preview-mask-description'>
          <h1>Correct</h1>
          <div className='question-answer-content-preview-mask-key'>
            <Scrollbars
              autoHeight
              autoHeightMin={0}
              autoHeightMax={70}
              autoHide={false}
            >
              <p>{questions[previewContent].answerKey}</p>
            </Scrollbars>
          </div>
          <div
            className='question-answer-content-preview-mask-button'
            onClick={() => setPreviewCorrectAnswer(undefined)}
          >
            <div className='question-answer-content-preview-mask-button-image'>
              Close
            </div>
          </div>
          <span>Report to Us</span>
        </div>
      </div>
    );
  };

  const renderQuestionPreview = () => {
    return (
      <div id='fixed-wrapper'>
        <div
          className='question-answer-content-preview'
          id='question-answer-content-preview'
          style={{ backgroundImage: `url(${previewImage && previewImage})` }}
        >
          <div className='question-answer-content-wrapper'>
            {questions.length !== 0 && <div className='question-answer-content-preview-header'>
              <div className='question-answer-content-preview-header-ask'>
                <Image src={questionPreview} />
              </div>
              <div className='question-answer-content-preview-header-progress'>
                <span style={{ color: '#FF9908', marginBottom: '8px' }}>Questions Answered</span>
                <span style={{ color: '#000', background: '#BDBDBD' }}>
                  {previewContent + 1}/{questions.length}
                </span>
              </div>
              <div className='question-answer-content-preview-header-cancel'>
                <Image src={cancelPreview} />
              </div>
            </div>}
            {questions.length !== 0 && <div className='question-answer-content-preview-content'>
              <h1>
                {questions[previewContent].description}
              </h1>
            </div>}
            <div className='question-answer-content-preview-footer'>
              {(questions.length !== 0 && questions[previewContent].type === 'TYPE_TRUE_OR_FALSE')
                && answerField(questions, previewContent).renderTrueOrFalse}
              {(questions.length !== 0 && questions[previewContent].type === 'TYPE_MULTIPLE_CHOICE')
                && answerField(questions, previewContent).renderMultipleChoice}
              {(questions.length !== 0 && questions[previewContent].type === 'TYPE_FILL_THE_BLANK')
                && answerField(questions, previewContent).renderFillTheBlank}
            </div>
          </div>
        </div>
        {renderCorrectAnswerPreview()}
      </div>
    );
  };

  const renderContentProposal = () => {
    return (
      <div
        className='question-answer-content'
        id='question-answer-content'
      >
        <Container>
          <Row>
            <Col xs={12}>
              <div className='question-answer-content-desc'>
                <h2>#2. Questions & Answer</h2>
                <p>
                  Create 15 questions to test student understanding about the lessons that you’ve done. <br />
                  Also please provide the key answer to help student learn about their mistake.
                </p>
              </div>
            </Col>
          </Row>
          <Row>
            <Col xs={8}>
              <div className='question-answer-content-box'>
                {!isEmpty(questions) && questions.map((question, index) => {
                  return (
                    <QuestionForm
                      key={question.id}
                      questionData={question}
                      questionIndex={index}
                      previewContent={previewContent}
                      setPreviewContent={setPreviewContent}
                      setPreviewCorrectAnswer={setPreviewCorrectAnswer}
                    />
                  );
                })}
              </div>
              <div className='question-answer-content-create'>
                <Button
                  type='dropdown'
                  icon={<PlusCircleFill />}
                  dropdownItem={dropdownCreateQuestion(handleGenerateQuestion)}
                >
                  Buat Soal Baru
                </Button>
              </div>
            </Col>
            <Col xs={4}>
              {renderQuestionPreview()}
            </Col>
          </Row>
        </Container>
      </div>
    );
  };

  const generateBlobURL = (data, fileName) => {
    const downloadFile = window.URL.createObjectURL(new Blob([data]));
    const fileURL = document.createElement('a');
    fileURL.href = downloadFile;
    fileURL.setAttribute('download', fileName);
    document.body.appendChild(fileURL);
    return fileURL.click();
  };

  const handleGetFile = async (fileURL, fileName, ref) => {
    try {
      if (fileURL !== '') {
        const { data } = await fetch(fileURL, {
          method: 'get'
        });
        return generateBlobURL(data, fileName);
      }
      return ref.current.click();
    } catch (error) {
      throw new Error(error);
    }
  };

  const handleUploadFile = async (type) => {
    try {
      if (type === 'video') {
        const { contentUrl, name } = proposalFile.mp4File;
        await handleGetFile(contentUrl, name, mp4InputFile);
      }
      if (type === 'archive') {
        const { contentUrl, name } = proposalFile.zipFile;
        await handleGetFile(contentUrl, name, zipInputFile);
      }
    } catch (error) {
      throw new Error(error);
    }
  };

  const handleUpdateUpload = async (typeUpload, uploadedData, files) => {
    const updatedState = {
      name: files[0].name,
      contentUrl: uploadedData.contentUrl || ''
    };
    try {
      if (typeUpload === 'document') {
        setProposalFile((prevState) => ({
          ...prevState,
          pdfFile: updatedState
        }));
      }
      if (typeUpload === 'archive') {
        setProposalFile((prevState) => ({
          ...prevState,
          zipFile: updatedState
        }));
        const updatedData = {
          archive: uploadedData['@id']
        };
        await axios.put(`${CREATIVE_WORKS_URL}/${props.match.params.page}`, updatedData);
      }
      if (typeUpload === 'video') {
        setProposalFile((prevState) => ({
          ...prevState,
          mp4File: updatedState
        }));
        const updatedData = {
          video: uploadedData['@id']
        };
        await axios.put(`${CREATIVE_WORKS_URL}/${props.match.params.page}`, updatedData);
      }
    } catch (error) {
      throw new Error(error);
    }
  };

  const handleOnChangeFile = async (event, typeUpload) => {
    event.preventDefault();
    event.persist();
    event.stopPropagation();
    const { target: { files } } = event;
    let fileType;
    if (!isEmpty(files)) {
      const getTypeFile = files[0].type.split('/');
      fileType = {
        fileExtension: getTypeFile[1]
      };
    }
    const uploadedData = await dispatch(uploadToS3(fileType, files[0]));
    if (uploadedData !== null) {
      await handleUpdateUpload(typeUpload, uploadedData, files);
    }
  };

  const handleDeleteUpload = async (typeUpload) => {
    const emptyState = {
      name: '',
      contentUrl: ''
    };
    try {
      if (typeUpload === 'document') {
        setProposalFile((prevState) => ({
          ...prevState,
          pdfFile: emptyState
        }));
      }
      if (typeUpload === 'archive') {
        await axios.put(`${CREATIVE_WORKS_URL}/${props.match.params.page}`, { archive: null });
        setProposalFile((prevState) => ({
          ...prevState,
          zipFile: emptyState
        }));
      }
      if (typeUpload === 'video') {
        await axios.put(`${CREATIVE_WORKS_URL}/${props.match.params.page}`, { video: null });
        setProposalFile((prevState) => ({
          ...prevState,
          mp4File: emptyState
        }));
      }
    } catch (error) {
      throw new Error(error);
    }
  };

  const renderUploadButton = (type, file) => {
    return (
      <Button block={true} onClick={() => handleUploadFile(type)}>
        {file.name !== ''
          ? file.name
          : (
            <>
              <CardImage /> Choose File
            </>
          )}
      </Button>
    );
  };

  const renderDeleteButton = (type, file) => {
    if (file.name !== '') {
      return (
        <div
          className='question-answer-lesson-content-wrapper-button-trash'
          onClick={() => handleDeleteUpload(type)}
        >
          <TrashFill />
        </div>
      );
    }
    return null;
  };

  const handleSubmitTranscript = async () => {
    try {
      const transcriptData = {
        transcript: voiceOverText
      };
      await axios.put(`${CREATIVE_WORKS_URL}/${props.match.params.page}`, transcriptData);
    } catch (error) {
      throw new Error(error);
    }
  };

  const renderLessonToLearnt = () => {
    return (
      <Container>
        <div className='question-answer-lesson'>
          <Row>
            <Col xs={8}>
              <div className='question-answer-lesson-title'>
                <Row>
                  <Col xs={2}>
                    <Image src={iconFile} fluid />
                  </Col>
                  <Col xs={10}>
                    <h1>Proposal <br /> Pelajaran</h1>
                  </Col>
                </Row>
              </div>
              <div className='question-answer-lesson-content'>
                <Row>
                  <Col xs={12}>
                    <div className='question-answer-lesson-content-desc'>
                      <div><h2 >#1. Lesson data</h2> <div className='question-answer-lesson-content-desc-line' /></div>
                      <p>
                        Create 1-2 minute animation lessons to help students learn more fun.
                        Please{' '}
                        <span className='bold-text'>Download Our Template</span>
                        , follow the <span className='bold-text'>Examples</span> and upload your
                        final animation proposals
                      </p>
                      <label className='question-answer-lesson-content-textarea-label' >Voice-over Text</label>
                      <div className='question-answer-lesson-content-textarea-container' >
                      <CharacterCounter
                        value={voiceOverText || ''}
                        maxLength={300}
                      >
                        <textarea
                          rows="10"
                          className='question-answer-lesson-content-textarea'
                          placeholder='Write out the exact words of the voiceover /
                         narration of the animation. Minimum 300 characters.'
                          onChange={(e) => setVoiceOverText(e.target.value)}
                          onBlur={handleSubmitTranscript}
                          value={voiceOverText}
                        />
                      </CharacterCounter>
                      </div>
                    </div>
                  </Col>
                </Row>
                <div className='question-answer-lesson-content-wrapper'>
                </div>
                <div className='question-answer-lesson-content-wrapper'>
                  <Row>
                    <input
                      type='file'
                      id='upload-file-zip'
                      ref={zipInputFile}
                      style={{ display: 'none' }}
                      onChange={(event) => handleOnChangeFile(event, 'archive')}
                    />
                    <Col xs={1}>
                      <Image src={iconZIP} width='40' />
                    </Col>
                    <Col xs={6}>
                      <h3>After Effect Folder</h3>
                      <p>Zip File</p>
                    </Col>
                    <Col xs={5}>
                      <div className={`question-answer-lesson-content-wrapper-button 
                      ${proposalFile.zipFile.name !== '' && 'filled-button '}`}>
                        {renderUploadButton('archive', proposalFile.zipFile)}
                        {renderDeleteButton('archive', proposalFile.zipFile)}
                      </div>
                    </Col>
                  </Row>
                </div>
                <div className='question-answer-lesson-content-wrapper'>
                  <Row>
                    <input
                      type='file'
                      id='upload-file-mp4'
                      ref={mp4InputFile}
                      style={{ display: 'none' }}
                      onChange={(event) => handleOnChangeFile(event, 'video')}
                    />
                    <Col xs={1}>
                      <Image src={iconMP4} width='40' />
                    </Col>
                    <Col xs={6}>
                      <h3>Final Movie</h3>
                      <p>MP4 or Mov</p>
                    </Col>
                    <Col xs={5}>
                      <div className={`question-answer-lesson-content-wrapper-button 
                      ${proposalFile.mp4File.name !== '' && 'filled-button '}`}>
                        {renderUploadButton('video', proposalFile.mp4File)}
                        {renderDeleteButton('video', proposalFile.mp4File)}
                      </div>
                    </Col>
                  </Row>
                </div>
              </div>
            </Col>
          </Row>
        </div>
      </Container>
    );
  };

  return (
    <div className='question-answer'>
      {isLoading && <Loading />}
      {renderHeaderProposal()}
      <ProposalOverview
        projectDetail={projectDetail.retrieved && projectDetail.retrieved}
        mappedImages={mappedImages && mappedImages}
        isShowModal={isShowModal}
        setIsShowModal={setIsShowModal}
        mappedMembers={mappedMembers && mappedMembers}
      />
      {renderLessonToLearnt()}
      {renderContentProposal()}
    </div>
  );
};

export default QuestionAnswer;
