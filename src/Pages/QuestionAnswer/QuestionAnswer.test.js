import React from 'react';
import { shallow } from 'enzyme';
import { useSelector } from 'react-redux';
import { convertToSnapshot } from '../../Utils/TestUtils/TestUtils';
import QuestionAnswer from './QuestionAnswer';

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useSelector: jest.fn((fn) => fn),
  useDispatch: jest.fn()
}));

describe('SignIn', () => {
  const mockMatchParams = {
    params: {
      page: 'test'
    }
  };
  const renderComponent = () => {
    return shallow(
      <QuestionAnswer match={mockMatchParams} />
    );
  };

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('#render', () => {
    it('should render component QuestionAnswer correctly when invoked', () => {
      const mockQuestionData = [
        {
          questionId: '1',
          type: 'true-or-false',
          question: '',
          answer: '',
          answerKey: '',
          options: [],
          trueOrFalseOptions: [],
          multipleOptions: [],
          image: null,
        },
      ];
      useSelector.mockImplementation(() => mockQuestionData);
      const renderedComponent = renderComponent();

      const actualComponent = convertToSnapshot(renderedComponent);

      expect(actualComponent)
        .toMatchSnapshot();
    });
  });
});
