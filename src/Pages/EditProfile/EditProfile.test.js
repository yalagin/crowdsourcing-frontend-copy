import React from 'react';
import { shallow } from 'enzyme';
import EditProfile from './EditProfile';
import { convertToSnapshot } from '../../Utils/TestUtils/TestUtils';

describe('EditProfile', () => {
  const setState = jest.fn();
  const useStateSpy = jest.spyOn(React, 'useState');
  useStateSpy.mockImplementation((init) => [init, setState]);

  const renderComponent = () => {
    return shallow(
      <EditProfile />
    );
  };

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render component ContestantProposal correctly when invoked', () => {
    const renderedComponent = renderComponent();

    const actualComponent = convertToSnapshot(renderedComponent);

    expect(actualComponent).toMatchSnapshot();
  });
});
