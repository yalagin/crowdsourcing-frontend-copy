import React, { useState, useRef } from 'react';
import { Container, Form, Modal } from 'react-bootstrap';
import Cropper from 'cropperjs';
import NavigationBar from '../../Components/NavigationBar/NavigationBar';
import ProfilePicture from '../../Assets/Images/profile-picture.png';
import Image from '../../Assets/Images/image.png';
import Button from '../../Components/Button/Button';
import Input from '../../Components/Input/Input';
import DropdownFormComponent from '../../Components/DropdownFormComponent/DropdownFormComponent';
import PhoneInput from '../../Components/PhoneInput/PhoneInput';
import './EditProfile.scss';

const GenderOptions = [
  { label: 'Male', value: 'male' },
  { label: 'Female', value: 'female' },
  { label: 'Other', value: 'other' },
];

const JobTitleOptions = [
  { label: 'Designer', value: 'designer' },
  { label: 'Founder', value: 'founder' },
  { label: 'Creator', value: 'creator' },
  { label: 'Maintainer', value: 'maintainer' },
];

const SpecialityOptions = [
  { label: 'ILLUSTRATION', value: 'illustration' },
  { label: 'ANIMATION', value: 'animation' },
  { label: 'DEVELOPMENT', value: 'development' },
  { label: 'ARCHITECTURE', value: 'architecture' },
];

const EditProfile = () => {
  const imgInput = useRef(null);

  const imgToCropRef = useRef(null);

  const [activeTab, setActiveTab] = useState('personalInfo');

  const [showCropPicModal, setShowCropPicModal] = useState(false);

  const [cropper, setCropper] = useState(null);

  const [croppedImgUrl, setShowCroppedImgUrl] = useState('');

  const initCropper = () => {
    const newCropper = new Cropper(imgToCropRef.current, {
      aspectRatio: 1 / 1,
      background: false,
    });
    return setCropper(newCropper);
  };

  const onCropClick = () => {
    const imgurl = cropper.getCroppedCanvas().toDataURL();
    setShowCroppedImgUrl(imgurl);
    setShowCropPicModal(false);
  };

  const renderHeader = () => {
    return (
      <div className='edit-profile-header' >
        <h1>Your Profile</h1>
      </div>
    );
  };

  const renderTab = () => {
    return (
      <div className='edit-profile-tabs' >
        <div
          className={`edit-profile-tabs-tab ${activeTab === 'personalInfo' ? 'active' : ''}`}
          onClick={() => setActiveTab('personalInfo')}
        >
          Personal Info
        </div>
        <div
          className={`edit-profile-tabs-tab ${activeTab === 'socialProfile' ? 'active' : ''}`}
          onClick={() => setActiveTab('socialProfile')} >Social Profile</div>
        <div
          className={`edit-profile-tabs-tab ${activeTab === 'accountSetting' ? 'active' : ''}`}
          onClick={() => setActiveTab('accountSetting')} >Account Setting</div>
      </div>
    );
  };

  const renderPersonalInfoForm = () => {
    return (
      <div>
        <Form>
          <h4 className='edit-profile-form-heading' >ABOUT</h4>
          <Input label='Name' placeholder='Enter Name' />
          <DropdownFormComponent label='Gender' placeholder='Choose Gender' options={GenderOptions} />
          <h4 className='edit-profile-form-heading' >CURRENT / MOST RECENT JOB</h4>
          <DropdownFormComponent label='JOB TITLE ' placeholder='Choose Job Title' options={JobTitleOptions} />
          <DropdownFormComponent
            label='SPECIALITY'
            placeholder='Choose Specialities'
            options={SpecialityOptions}
            isMulti
          />
          <Input label='INSTITUTE' placeholder='Enter Name of Institute' />
          <Input label='LOCATION' placeholder='Enter Location' />
          <div className='edit-profile-personal-info-btn-save' >
            <Button block >Save Changes</Button>
          </div>
        </Form>
      </div>
    );
  };

  const PersonalInfoTab = () => {
    return (
      <div className='edit-profile-tabs-content-container' >
        <div className='edit-profile-personal-info-img-container' >
          <img
            src={croppedImgUrl || ProfilePicture}
            alt=""
            className='edit-profile-personal-info-img-container-img'
          />
          <Button onClick={() => imgInput.current.click()} >
            <img
              src={Image}
              alt="image"
              className='edit-profile-personal-info-img-container-upload-btn-img'
            />
              Upload New Picture
          </Button>
        </div>
        {renderPersonalInfoForm()}
      </div>
    );
  };

  const SocialProfileTab = () => {
    return (
      <div className='edit-profile-tabs-content-container' >
        <h4 className='edit-profile-form-heading' >SOCIAL MEDIA</h4>
        <Input label='LINKEDIN' placeholder='Enter Linkedin Link' />
        <Button
          bsPrefix='custom-button btn edit-profile-btn-outline-primary'
        >
          <i className='fa fa-plus-circle edit-profile-btn-outline-primary-icon' />
          Add New Link
        </Button>
        <div className='edit-profile-social-profile-btn-update' >
          <Button block >Update Social Profile</Button>
        </div>
      </div>
    );
  };

  const AccountSettingTab = () => {
    return (
      <div className='edit-profile-tabs-content-container' >
        <Form>
          <h4 className='edit-profile-form-heading' >ACCOUNT</h4>
          <Input label='USERNAME' placeholder='Enter Username' disabled />
          <Input label='EMAIL' placeholder='Enter Email' />
          <PhoneInput label='PHONE NUMBER' placeholder='Enter Phone Number' />
          <h4 className='edit-profile-form-heading' >PASSWORD</h4>
          <div className='edit-profile-account-setting-flex-container' >
            <Input
              label='PASSWORD'
              type='password'
              style={{ width: 'calc(100% - 220px)' }}
              addInputClass='edit-profile-account-setting-input-password'
              disabled
            />
            <Button
              type='secondary'
              bsPrefix='btn btn-secondary edit-profile-account-setting-btn-password'
            >Change Password</Button>
          </div>
          <h4 className='edit-profile-form-heading' >CLOSE ACCOUNT</h4>
          <div className='edit-profile-account-setting-flex-container' >
            <p>Delete your account and account data</p>
            <Button type='secondary' >Delete Your Account</Button>
          </div>
          <div className='edit-profile-account-setting-btn-update' >
            <Button block >Update Social Profile</Button>
          </div>
        </Form>
      </div>
    );
  };

  const ImgFileInput = () => {
    const onImgInputChange = (file) => {
      if (file) {
        const reader = new FileReader();
        reader.onload = (e) => {
          imgToCropRef.current.setAttribute('src', e.target.result);
        };
        reader.readAsDataURL(file);
        setTimeout(initCropper, 1000);
      }
    };

    return (
      <input
        type='file'
        className='edit-profile-img-file-input'
        ref={imgInput}
        onChange={(e) => {
          onImgInputChange(e.target.files[0]);
          setShowCropPicModal(true);
        }}
      />
    );
  };

  const renderCropImageModal = () => {
    return (
      <Modal
        show={showCropPicModal}
        dialogClassName='edit-profile-modal'
        backdrop='static'
        centered
      >
        <div className='start-proposal-modal-header' >
          <i
            className='fa fa-times start-proposal-modal-header-close'
            onClick={() => setShowCropPicModal(false)}
          />
          <div className='start-proposal-modal-header-team-name' >Crop Image</div>
          <div className='edit-profile-modal-header-sub'>
            Please crop your Profile Image according to our requirements
          </div>
          <div
            className='start-proposal-modal-header-days'
          >
          </div>
        </div>

        <div className='edit-profile-modal-img-container' >
          <img src='' alt="" ref={imgToCropRef} />
        </div>
        <div className='edit-profile-modal-btn' >
          <Button block onClick={() => onCropClick()} >Crop Image</Button>
        </div>
      </Modal>
    );
  };

  return (
    <>
      <NavigationBar />
      <Container>
        <div className='edit-profile-width' >
          {renderHeader()}
          {renderTab()}
          {activeTab === 'personalInfo' && <PersonalInfoTab />}
          {activeTab === 'socialProfile' && <SocialProfileTab />}
          {activeTab === 'accountSetting' && <AccountSettingTab />}
        </div>
        {renderCropImageModal()}
      </Container>
      <ImgFileInput />
    </>
  );
};

export default EditProfile;
