import React from 'react';
import { shallow } from 'enzyme';
import AdminLessonBank from './AdminLessonBank';
import { convertToSnapshot } from '../../Utils/TestUtils/TestUtils';

describe('AdminLessonBank', () => {
  const setState = jest.fn();
  const useDispatch = jest.fn();
  const useStateSpy = jest.spyOn(React, 'useState');
  useStateSpy.mockImplementation((init) => [init, setState, useDispatch]);

  const renderComponent = () => {
    return shallow(
      <AdminLessonBank />
    );
  };

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render component AdminLessonBank correctly when invoked', () => {
    const renderedComponent = renderComponent();

    const actualComponent = convertToSnapshot(renderedComponent);

    expect(actualComponent).toMatchSnapshot();
  });
});
