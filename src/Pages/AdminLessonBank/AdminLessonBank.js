import React, { useState } from 'react';
import moment from 'moment';
import Calendar from 'react-calendar';
import AdminSidebar from '../../Components/AdminSidebar/AdminSidebar';
import Folder from '../../Assets/Images/Folder.png';
import ReviewLessonModal from '../../Components/ReviewLessonModal/ReviewLessonModal';
import './AdminLessonBank.scss';

const publishedLessons = [
  {
    title: 'Pahami keanekaragaman budaya di Indonesia',
    subject: 'Natural Science',
    grade: '4SD',
    published: 'just now',
  },
  {
    title: 'Pahami keanekaragaman budaya di Indonesia',
    subject: 'Natural Science',
    grade: '4SD',
    published: '1 min ago',
  },
  {
    title: 'Pahami keanekaragaman budaya di Indonesia',
    subject: 'Natural Science',
    grade: '4SD',
    published: '23 hours ago',
  },
  {
    title: 'Pahami keanekaragaman budaya di Indonesia',
    subject: 'Natural Science',
    grade: '4SD',
    published: '6 days ago',
  },
  {
    title: 'Pahami keanekaragaman budaya di Indonesia',
    subject: 'Natural Science',
    grade: '4SD',
    published: '1 week ago',
  },
  {
    title: 'Pahami keanekaragaman budaya di Indonesia',
    subject: 'Natural Science',
    grade: '4SD',
    published: '08 Oct 2020',
  },
  {
    title: 'Pahami keanekaragaman budaya di Indonesia',
    subject: 'Natural Science',
    grade: '4SD',
    published: '05 Oct 2020',
  },
  {
    title: 'Pahami keanekaragaman budaya di Indonesia',
    subject: 'Natural Science',
    grade: '4SD',
    published: '4 Oct 2020',
  },
];

const AdminLessonBank = () => {
  // States

  const [activeTab, setActiveTab] = useState('toPublish');

  const [activeFilter, setActiveFilter] = useState('');

  const [filteredClasses, setFilteredClasses] = useState(['SD Kelas 1']);

  const [filteredSubjects, setFilteredSubjects] = useState(['Semua Subbjek']);

  const [selectedPeriodFilter, setSelectedPeriodFilter] = useState([]);

  const [activePageNo, setActivePageNo] = useState(1);

  const [showReviewLessonModal, setShowReviewLessonModal] = useState(false);

  const classesFilterList = [
    'Semua Kelas',
    'SD Kelas 1',
    'SD Kelas 2',
    'SD Kelas 3',
    'SD Kelas 4',
    'SD Kelas 5',
    'SD Kelas 6',
  ];

  const subjectsFilterList = [
    'Semua Subjek',
    'Matematika',
    'IPA (Ilmu Pengetahuan Alam)',
  ];

  // Functions

  const toggleActiveFilter = (name) => {
    if (activeFilter === name) setActiveFilter();
    else setActiveFilter(name);
  };

  // Render Functions

  const renderHeading = () => {
    return (
      <div className='admin-lesson-requests-heading' >

        <h1 className='admin-lesson-requests-heading-text' >
          <img src={Folder} alt="" className='admin-lesson-requests-heading-img' />
          Lesson Bank
        </h1>

      </div>
    );
  };

  const renderTabs = () => {
    return (
      <div className='admin-lesson-requests-tabs' >
        <div
          className={`admin-lesson-requests-tabs-tab ${activeTab === 'toPublish' ? 'active' : ''}`}
          onClick={() => setActiveTab('toPublish')}
        >
          To Publish(4)
        </div>
        <div
          className={`admin-lesson-requests-tabs-tab ${activeTab === 'published' ? 'active' : ''}`}
          onClick={() => setActiveTab('published')}
        >
          Published(40)
        </div>
      </div>
    );
  };

  const CheckMark = ({ filterName, text, setFilter }) => {
    const toggleFilter = () => {
      const index = filterName.indexOf(text);

      const array = [...filterName];
      if (index !== -1) {
        array.splice(index, 1);
        setFilter(array);
      } else {
        array.push(text);
        setFilter(array);
      }
    };

    return (
      <div
        className={`admin-lesson-requests-check ${filterName.includes(text) ? 'active' : ''}`}
        onClick={() => toggleFilter()}
      >
        <input
          type='checkbox'
          className='admin-lesson-requests-check-input'
          checked={filterName.includes(text)}
        />
        <span className='admin-lesson-requests-check-checkmark' />
        {text}
      </div>
    );
  };

  const ClassesFilter = () => {
    return (
      <div className='admin-lesson-requests-filter-container' >
        <div
          onClick={() => toggleActiveFilter('classes')}
          className={`admin-lesson-requests-filter ${activeFilter === 'classes' ? 'active' : ''}`}
        >
          Semua Kelas
        <i className='fa fa-chevron-down admin-lesson-requests-filter-icon' />
        </div>

        {activeFilter === 'classes' && <div className='admin-lesson-requests-filter-expand classes' >
          {classesFilterList.map((singleClass) => (
            <CheckMark
              filterName={filteredClasses}
              text={singleClass}
              setFilter={setFilteredClasses}
            />
          ))}

          <div className='admin-lesson-requests-filter-expand-deadline' >
            <span
              className='admin-lesson-requests-filter-expand-deadline-hapus'
              onClick={() => setActiveFilter()}
            >
              Hapus
            </span>

            <span
              className='admin-lesson-requests-filter-expand-deadline-simpan'
              onClick={() => setActiveFilter()}
            >
              Simpan
            </span>
          </div>

        </div>}
      </div>
    );
  };

  const SubjectsFilter = () => {
    return (
      <div className='admin-lesson-requests-filter-container' >
        <div
          onClick={() => toggleActiveFilter('subjects')}
          className={`admin-lesson-requests-filter ${activeFilter === 'subjects' ? 'active' : ''}`}
        >
          Semua Subjek
        <i className='fa fa-chevron-down admin-lesson-requests-filter-icon' />
        </div>

        {activeFilter === 'subjects' && <div className='admin-lesson-requests-filter-expand subjects' >
          {subjectsFilterList.map((subject) => (
            <CheckMark
              filterName={filteredSubjects}
              text={subject}
              setFilter={setFilteredSubjects}
            />
          ))}

          <div className='admin-lesson-requests-filter-expand-deadline' >
            <span
              className='admin-lesson-requests-filter-expand-deadline-hapus'
              onClick={() => setActiveFilter()}
            >
              Hapus
            </span>

            <span
              className='admin-lesson-requests-filter-expand-deadline-simpan'
              onClick={() => setActiveFilter()}
            >
              Simpan
            </span>
          </div>

        </div>}

      </div>
    );
  };

  const PeriodFilter = () => {
    return (
      <div className='admin-lesson-requests-period-filter' >
        <span className='admin-lesson-requests-period-filter-text' >Periode</span>

        <div
          onClick={() => toggleActiveFilter('period')}
          className={`admin-lesson-requests-filter period ${activeFilter === 'period' ? 'active' : ''}`}
        >
          {selectedPeriodFilter.length > 0
            ? `${moment(selectedPeriodFilter[0]).format('DD MMM YYYY')} 
            - ${moment(selectedPeriodFilter[1]).format('DD MMM YYYY')}`
            : 'No Period Selected'}

          <i className='fa fa-chevron-down admin-lesson-requests-filter-icon' />
        </div>

        {activeFilter === 'period' && <div className='admin-lesson-requests-filter-expand period' >

          <span className='admin-lesson-requests-period-filter-date-text' >DATE</span>

          <div className='admin-lesson-requests-period-filter-inputs' >

            <input
              type="text"
              className='admin-lesson-requests-period-filter-input'
              value={selectedPeriodFilter.length > 0 ? moment(selectedPeriodFilter[0]).format('DD MMM YYYY') : ''}
            />

            <span>TO</span>

            <input
              type="text"
              className='admin-lesson-requests-period-filter-input'
              value={selectedPeriodFilter.length > 0 ? moment(selectedPeriodFilter[1]).format('DD MMM YYYY') : ''}
            />

          </div>

          <Calendar
            className='admin-lesson-requests-calendar'
            next2Label={null}
            prev2Label={null}
            selectRange={true}
            returnValue='start'
            onChange={(value) => {
              setSelectedPeriodFilter(value);
              setActiveFilter();
            }}
          />

        </div>}

      </div>
    );
  };

  const renderFilters = () => {
    return (
      <div className='admin-lesson-requests-filters' >
        <ClassesFilter />
        <SubjectsFilter />
        <PeriodFilter />
      </div>
    );
  };

  const PublishedTable = () => {
    return (
      <div className='admin-lesson-requests-table' >

      <div className='admin-lesson-requests-table-header' >

        <span className='admin-lesson-requests-table-header-text title' >TITLE</span>

        <span className='admin-lesson-requests-table-header-text subject' >SUBJECT</span>

        <span className='admin-lesson-bank-table-header-text grade' >
          GRADE
          <span className='admin-lesson-requests-table-header-text-arrow' >
            <i className='fa fa-caret-up' /> <i className='fa fa-caret-down' />
          </span>
        </span>

        <span className='admin-lesson-requests-table-header-text status' >PUBLISHED</span>

      </div>

      {publishedLessons.map((lesson) => (
        <div className='admin-lesson-requests-table-row' >

          <span
            className='admin-lesson-requests-table-row-text title'
            onClick={() => setShowReviewLessonModal(true)}
          >
            {lesson.title}
          </span>

          <span className='admin-lesson-bank-table-row-text subject' >{lesson.subject}</span>

          <span className='admin-lesson-bank-table-row-text grade' >{lesson.grade}</span>

          <span className='admin-lesson-requests-table-row-text status' >{lesson.published}         </span>

        </div>
      ))}

    </div>

    );
  };

  const renderPagination = () => {
    return (
      <div className='admin-lesson-requests-pagination' >

        <i className='fa fa-chevron-left admin-lesson-requests-pagination-arrow' />

        <span
          className={`admin-lesson-requests-pagination-num ${activePageNo === 1 ? 'active' : ''}`}
          onClick={() => setActivePageNo(1)}
        >
          1
        </span>

        <span
          className={`admin-lesson-requests-pagination-num ${activePageNo === 2 ? 'active' : ''}`}
          onClick={() => setActivePageNo(2)}
        >
          2
        </span>

        <i className='fa fa-chevron-right admin-lesson-requests-pagination-arrow' />

        <span>1 - 20 of 40</span>

      </div>
    );
  };

  return (
    <div>
      <AdminSidebar active='lessonBank' />
      <div className='admin-lesson-requests-content' >
        {renderHeading()}
        {renderTabs()}
        {renderFilters()}
        {activeTab === 'published' && <PublishedTable/>}
        {renderPagination()}
        <ReviewLessonModal
          showModal={showReviewLessonModal}
          setShowModal={setShowReviewLessonModal}
        />
      </div>
    </div>
  );
};

export default AdminLessonBank;
