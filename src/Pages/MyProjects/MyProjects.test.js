import React from 'react';
import { shallow } from 'enzyme';
import MyProjects from './MyProjects';
import { convertToSnapshot } from '../../Utils/TestUtils/TestUtils';

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useSelector: jest.fn((fn) => fn),
  useDispatch: jest.fn()
}));

describe('<MyProjects />', () => {
  const setState = jest.fn();
  const useStateSpy = jest.spyOn(React, 'useState');
  useStateSpy.mockImplementation((init) => [init, setState]);

  const renderComponent = () => {
    return shallow(
      <MyProjects />
    );
  };

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render component MyProjects correctly when invoked', () => {
    const renderedComponent = renderComponent();

    const actualComponent = convertToSnapshot(renderedComponent);

    expect(actualComponent).toMatchSnapshot();
  });
});
