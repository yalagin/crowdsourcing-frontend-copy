import React from 'react';
import { shallow } from 'enzyme';
import * as Formik from 'formik';
import { useSelector, useDispatch } from 'react-redux';
import SignIn from './SignIn';
import { convertToSnapshot } from '../../Utils/TestUtils/TestUtils';

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useSelector: jest.fn((fn) => fn),
  useDispatch: jest.fn()
}));

describe('SignIn', () => {
  const mockSetFieldValue = jest.fn();
  const mockHandleSubmit = jest.fn();
  const mockSetStatus = jest.fn();
  const mockForm = (errorMessage = {}) => ({
    setFieldValue: mockSetFieldValue,
    handleSubmit: mockHandleSubmit,
    setStatus: mockSetStatus,
    values: {
      email: 'admin@gmail.com',
      password: 'admin'
    },
    errors: {
      email: '',
      password: ''
    },
    validateForm: jest.fn(() => errorMessage)
  });

  const renderComponent = () => {
    return shallow(
      <SignIn />
    );
  };

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('#render', () => {
    it('should render component SignIn correctly when invoked', () => {
      const renderedComponent = renderComponent();

      const actualComponent = convertToSnapshot(renderedComponent);

      expect(actualComponent).toMatchSnapshot();
    });

    it('should render error message when isError is True', () => {
      useSelector
        .mockImplementation(() => ({ isError: true }));
      const renderedComponent = renderComponent();

      const errorMessage = renderedComponent.find('.sign-in-form-error');

      expect(errorMessage).toHaveLength(1);
    });
  });

  // describe('#handleOnChangeEmail', () => {
  //   it('should change email and call setFieldValue from formik when invoked', () => {
  //     const mockEmailValue = {
  //       target: {
  //         value: 'admin@gmail.com'
  //       }
  //     };
  //     jest
  //       .spyOn(Formik, 'useFormik')
  //       .mockImplementation(() => mockForm());
  //     const renderedComponent = renderComponent();

  //     const { props: { onChange } } = renderedComponent.find('Input').get(0);
  //     onChange(mockEmailValue);

  //     expect(mockSetFieldValue).toHaveBeenCalledWith('email', 'admin@gmail.com');
  //   });
  // });

  // describe('#handleOnChangePassword', () => {
  //   it('should change password and call setFieldValue from formik when invoked', () => {
  //     const mockPasswordValue = {
  //       target: {
  //         value: 'admin'
  //       }
  //     };
  //     jest
  //       .spyOn(Formik, 'useFormik')
  //       .mockImplementation(() => mockForm());
  //     const renderedComponent = renderComponent();

  //     const { props: { onChange } } = renderedComponent.find('Input').get(1);
  //     onChange(mockPasswordValue);

  //     expect(mockSetFieldValue).toHaveBeenCalledWith('password', 'admin');
  //   });
  // });

  describe('#handleSignIn', () => {
    it('should call dispatch function when handleSignIn is invoked', async () => {
      const dispatch = jest.fn();
      jest
        .spyOn(Formik, 'useFormik')
        .mockImplementation(() => mockForm());
      useDispatch.mockReturnValue(dispatch);
      const renderedComponent = renderComponent();

      const submitButton = renderedComponent.find('Button').at(0);
      submitButton.simulate('click', {
        preventDefault: () => {}
      });

      expect(useDispatch).toHaveBeenCalled();
    });

    it('should call setStatus function when handleSignIn is invoked', async () => {
      jest
        .spyOn(Formik, 'useFormik')
        .mockImplementation(() => mockForm({ email: 'test' }));
      const renderedComponent = renderComponent();

      const submitButton = renderedComponent.find('Button').at(0);
      submitButton.simulate('click', {
        preventDefault: () => {}
      });
      await Promise.resolve();

      expect(mockSetStatus).toHaveBeenCalled();
    });
  });
});
