import React from 'react';
import { shallow } from 'enzyme';
import AdminLessonRequests from './AdminLessonRequests';
import { convertToSnapshot } from '../../Utils/TestUtils/TestUtils';

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useSelector: jest.fn((fn) => fn),
  useDispatch: jest.fn()
}));

describe('AdminLessonRequests', () => {
  const setState = jest.fn();
  const useDispatch = jest.fn();
  const useStateSpy = jest.spyOn(React, 'useState');
  useStateSpy.mockImplementation((init) => [init, setState, useDispatch]);

  const renderComponent = () => {
    return shallow(
      <AdminLessonRequests />
    );
  };

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render component AdminLessonRequests correctly when invoked', () => {
    const renderedComponent = renderComponent();

    const actualComponent = convertToSnapshot(renderedComponent);

    expect(actualComponent).toMatchSnapshot();
  });
});
